# Implementation of the Algorithms from the Paper "Compressed Multiple Pattern Matching"

This repository contains implementations and experimental setups for the multiple pattern matching algorithms from the paper

Dmitry Kosolobov and Nikita Sivukhin
"Compressed multiple pattern matching".

To make the repository smaller, we present here only samples of each test file. References to full experimental datasets can be found below.

## Usage
You need to install [sdsl-lite](https://github.com/simongog/sdsl-lite) and [googletest](https://github.com/google/googletest) libraries to compile the source code.

```bash
# Firstly, generate a build configuration of the project via cmake:
mkdir _build
cd _build
cmake ..

# Then build the project:
cmake --build .

# Then you can run unit-tests via ctest with 'unit' token filter:
ctest -R unit
# Or run performance tests with 'perf' token filter:
ctest -R perf
```

After running the performance tests you can generate a plot from the `_build` directory:
```bash
# Run 'pip3 install -r ./scripts/requirements.txt' to install python dependencies
python3 ./scripts/generate_plot.py --source results.xml --dest plot.png
```
The plot looks as follows:
![Results plot](resources/plot.png)

The performance tests generate an XML-file with results in the following format:
```
<?xml version="1.0" encoding="UTF-8"?>
<testsuites tests="42" failures="0" disabled="0" errors="0" timestamp="2018-10-27T20:44:43" time="17948.6" name="AllTests">
  <testsuite name="MemoryTest/0" tests="7" failures="0" disabled="0" errors="0" time="2881.46">
    <testcase 
		name="UrlsDatabase[belazzougui_simple]" 
		type_param="belazzougui_simple" 
		status="run" 
		time="348.918" 
		classname="MemoryTest/0">
		<properties>
			<property name="test_description" 		value="~4,000,000 urls (1,000,000 popular urls, and many urls from domain zones .nu and .se)"/>
			<property name="dict_size_in_bytes"		value="67761031"/>
			<property name="effective_sigma"		value="40"/>
			<property name="trie_edges"				value="39385319"/>
			<property name="build_time_ms"			value="77214"/>
			<property name="algo_used_bytes"		value="62263084"/>
			<property name="text_size_in_bytes"		value="209715200"/>
			<property name="text_length" 			value="209040222"/>
			<property name="occurences" 			value="3461097"/>
			<property name="query_time_ms" 			value="267376"/>
		</properties>
    </testcase>
	...
  </testsuite>
</testsuites>
```

There is a small example source with basic top-level structures in the `sample.cpp` file. 

Also, you can find samples in the directory `tests/data/datasets.7z` (they are automatically unzipped when one builds the project).
And you can download the archive with full datasets from [yandex.disk](https://yadi.sk/d/FvaPt2-SWrxT5Q)

## Description of the Algorithms
The source code implements the following four algorithms (named in the plot, respectively, ``blz``, ``cblz``, ``cblz8``, and ``smp``): 

* The compressed multiple pattern matching algorithm of Belazzougui from the paper "Succinct dictionary matching with no slowdown", CPM 2010 (class `standard_matcher<>`)
* Our modification of the Belazzougui's algorithm that uses the fixed block compression boosting (class `compressed_matcher<>`)
* The same modification but with memory reduced using a 8-dense vertex subset to sparsify failure links (see our paper) (class `compressed_matcher<skip_layer_tree<8>>`)
* A simple O(mn)-time algorithm that uses only the transition table from the Belazzougui's algorithm and some additional simple structures (class `trie_matcher<>`)

Examples of usage of the algorithms can be found in the file `sample.cpp` or in the main performance testing file `tests/perf_tests.cpp`.

## Datasets (from the archive `datasets.7z`)

The file `tests/data/datasets.7z` of this repository contains truncated versions of the full datasets that can be found in https://yadi.sk/d/FvaPt2-SWrxT5Q. 

The full datasets contain the following files:

### urls/
* `1m_popular_urls.dict`(15 MB) - 1,000,000 popular urls from [Alexa database][alexa_urls]
* `1m_nu_urls.utf32`(24 MB) - 997,691 urls from .nu top-level domain extracted from [DNS-zone files][nu_zone]
* `2m_se_urls.utf32`(27 MB) - 1,831,144 urls from .se top-level domain extracted from [DNS-zone files][se_zone]
* `4m_all_urls.utf32`(65 MB) - 3,825,132 unique urls from `1m_popular_urls.txt`, `1m_nu_urls.txt`, and `2m_se_urls`

### wiki/
All titles in the dictionaries have length at least 3 symbols

* `5m_wiki_en_titles.dict`(165 MB) - 3,875,263 titles from English wikipedia extracted from [dumps.wikimedia][enwiki_titles]
* `4m_wiki_ru_titles.dict`(72 MB) - 4,145,276 titles from Russian wikipedia extracted from [dumps.wikimedia][ruwiki_titles]
* `2m_wiki_zh_titles.dict`(29 MB) - 1,649,209 titles from Chinese wikipedia extracted from [dumps.wikimedia][zhwiki_titles]
* `2m_wiki_ja_titles.dict`(38 MB) - 1,691,693 titles from Japanese wikipedia extracted from [dumps.wikimedia][jawiki_titles]
* `200MB_wiki_en_articles.utf32`(200 MB) - the first 200MB of the decompressed output for the .bz2 archive of English wikipedia articles extracted from [dumps.wikimedia][enwiki_articles]
* `200MB_wiki_ru_articles.utf32`(200 MB) - the first 200MB of the decompressed output for the .bz2 archive of Russian wikipedia articles extracted from [dumps.wikimedia][ruwiki_articles]
* `200MB_wiki_zh_articles.utf32`(200 MB) - the first 200MB of the decompressed output for the .bz2 archive of Chinese wikipedia articles extracted from [dumps.wikimedia][zhwiki_articles]
* `200MB_wiki_ja_articles.utf32`(200 MB) - the first 200MB of the decompressed output for the .bz2 archive of Japanese wikipedia articles extracted from [dumps.wikimedia][jawiki_articles]

### DNA/
* `human_chr1_dna.fa` - Human 1-st chromosome genome sequence in FASTA format taken from [NCBI][HumanChr1]
* `human_chr1_dna.ascii`(218 MB) - 228,503,292 nucleotides from `human_chr1_dna.fa` simply joined in one long string
* `human_chr1_dna_reads{1,2}.fq` (263 MB each) - reads generated by [wgsim][wgsim] paired reads simulator with default read length set to 100
* `human_chr1_dna_reads.dict`(193 MB) - all reads from `human_chr1_dna_reads{1,2}.fq`

### viruses/
* `4m_signatures.dict`(128 MB) - 4,059,198 virus signatures from the [ClamAV database (main.cvd/main.mdb)][viruses]
* `program_sample.bin`(100 MB) - a sample binary file generated with `viruses/gen_sample.cpp` algorithm from the `datasets.7z` archive

[alexa_urls]: http://s3.amazonaws.com/alexa-static/top-1m.csv.zip
[nu_zone]: https://zonedata.iis.se/nu.zone.gz
[se_zone]: https://zonedata.iis.se/se.zone.gz

[ruwiki_titles]: https://dumps.wikimedia.org/ruwiki/20180120/ruwiki-20180120-pages-articles-multistream-index.txt.bz2
[ruwiki_articles]: https://dumps.wikimedia.org/ruwiki/20180120/ruwiki-20180120-pages-articles-multistream.xml.bz2

[enwiki_titles]: https://dumps.wikimedia.org/enwiki/20180120/enwiki-20180120-pages-articles-multistream-index.txt.bz2
[enwiki_articles]: https://dumps.wikimedia.org/enwiki/20180120/enwiki-20180120-pages-articles-multistream.xml.bz2

[zhwiki_titles]: https://dumps.wikimedia.org/zhwiki/20180120/zhwiki-20180120-pages-articles-multistream-index.txt.bz2
[zhwiki_articles]: https://dumps.wikimedia.org/zhwiki/20180120/zhwiki-20180120-pages-articles-multistream.xml.bz2

[jawiki_titles]: https://dumps.wikimedia.org/jawiki/20180120/jawiki-20180120-pages-articles-multistream-index.txt.bz2
[jawiki_articles]: https://dumps.wikimedia.org/jawiki/20180120/jawiki-20180120-pages-articles-multistream.xml.bz2

[HumanChr1]: ftp://ftp.ncbi.nih.gov/genomes/H_sapiens/Assembled_chromosomes/seq/hs_alt_CHM1_1.1_chr1.fa.gz
[wgsim]: https://github.com/lh3/wgsim

[viruses]: http://database.clamav.net/main.cvd
