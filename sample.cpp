#include <compressed_matcher.hpp>
#include <standard_matcher.hpp>
#include <trie_matcher.hpp>

#include <iostream>

void print_occurences(std::string algorithm, std::vector<occurence> occurences)
{
    std::cout << "Try '" << algorithm << "' algorithm" << std::endl;
    for (const auto& occ : occurences)
        std::cout << "   " << occ << std::endl;
}
int main()
{
    // If you need to read the dictionary from file you can use `unordered_dictionary_file(std::string filename)` class
    auto d = unordered_dictionary<byte_alphabet>({ "multiple", "pattern", "matching" });

    // Use utf32_alphabet/utf16_alphabet types for unicode dictionaries/texts
    auto d_unicode = unordered_dictionary<utf16_alphabet>({ u"новый", u"мир" });
    print_occurences("unicode_dictionary",
        trie_matcher<>(d_unicode).enumerate_occurence_positions(string_seq(u"о дивный новый мир")));

    std::string text = "multiple alogirthms that solves multiple-pattern-matching problem are implemented here";
    // All algorithms work with string_seq that represents an abstract sequence of char codes
    auto text_seq = string_seq(text);

    // All matchers have many template parameters with some default values
    auto standard = standard_matcher<>(d);
    print_occurences("standard_matcher", standard.enumerate_occurence_positions(text_seq));

    auto compressed = compressed_matcher<>(d);
    print_occurences("compressed_matcher", compressed.enumerate_occurence_positions(text_seq));

    auto trie = trie_matcher<>(d);
    print_occurences("trie_matcher", trie.enumerate_occurence_positions(text_seq));

    // You can tune default parameters for less space consumption or faster query answer
    auto skipped = compressed_matcher<skip_layer_tree<10>>(d);
    print_occurences("skipped_compressed_matcher", skipped.enumerate_occurence_positions(text_seq));

    return 0;
}
