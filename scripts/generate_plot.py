#!/usr/bin/python3

import re
import argparse
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from xml.etree import ElementTree

parser = argparse.ArgumentParser()
parser.add_argument("--source", help="Source file with test results in JUnit XML format", default="results.xml")
parser.add_argument("--dest", help="Destination file for output plot image", default="plot.png")

colors = ['#e66101', '#fdb863', '#b2abd2', '#5e3c99', '#dbb1bc', '#8acdea']
colormap = sns.color_palette(colors)
sns.set_palette(colormap)
sns.set(style="whitegrid", color_codes=True)
matplotlib.rc('font', size=17)

DatasetColumn = 'Dataset (.dict)'
AlgorithmColumn = 'Algorithm'
MemoryColumn = 'Memory'
MemoryTrieColumn = 'Trie memory'
QueryTimeColumn = 'Query time'
BuildTimeColumn = 'Build time'
Sigma = 'Sigma'

database_name_alias = {
    'UrlsDatabase': 'urls',
    'VirusSignatureDatabase': 'virus',
    'DnaDatabase': 'dna',
    'WikiEnDatabase': 'ttl.en',
    'WikiRuDatabase': 'ttl.ru',
    'WikiChineseDatabase': 'ttl.zh',
    'WikiJapaneseDatabase': 'ttl.ja',
}

algorithm_name_alias = {
    'Belazzougui simple': 'blz',
    'Belazzougui compression boosting': 'cblz',
    'Belazzougui compression boosting partial failure tree(4)': 'cblz4',
    'Belazzougui compression boosting partial failure tree(8)': 'cblz8',
    'Belazzougui compression boosting partial failure tree(10)': 'cblz10',
    'Trie matcher simple': 'smp'
}

AlgoForPlot = ['blz', 'cblz', 'cblz8', 'smp']

def parse_type_param(s):
    return re.sub(r'\<(\d+).*?\>', r'(\1)', s.replace('_', ' ').capitalize())

def draw_barplot(data_frame, y_column, measure_units, ax, need_set_xtitle=False):
    sns.barplot(x=DatasetColumn, y=y_column, hue=AlgorithmColumn, data=data_frame, ax=ax, palette=colormap)
    ax.set_title(y_column)
    ax.set_xlabel('')
    ax.set_ylabel(measure_units)
    ax.legend_.remove()

def draw_sigma_baseline(data_frame, y_column, measure_units, ax):
    palette = sns.color_palette(['#555555'])
    sns.barplot(x=DatasetColumn, y=y_column, hue=AlgorithmColumn, data=data_frame, ax=ax, palette=palette, alpha=0.5, fill=False, hatch='//', edgecolor='black')
    ax.set_xlabel('')
    ax.set_ylabel(measure_units)
    ax.legend_.remove()

def gather_test_data_dict(test_node):
    result = {}
    for prop in test_node.iter('property'):
        result[prop.attrib['name']] = prop.attrib['value']
    if result: return result
    for attrib_name in ['effective_sigma', 'text_length', 'trie_edges', 'algo_used_bytes', 'query_time_ms', 'build_time_ms', 'dict_size_in_bytes']:
        result[attrib_name] = test_node.attrib[attrib_name]
    return result

def main():
    args = parser.parse_args()

    tree = ElementTree.parse(args.source)
    data, sigma_baseline = [], []
    fig, axes = plt.subplots(1, 3, figsize=(18, 8))
    for child in tree.getroot().iter('testcase'):
        dataset_name = re.search('^\w+', child.attrib['name']).group(0)
        dataset = database_name_alias[dataset_name]
        test_data = gather_test_data_dict(child)
        sigma = int(test_data['effective_sigma'])
        text_length = int(test_data['text_length'])
        trie_size = int(test_data['trie_edges'])
        algorithm = algorithm_name_alias[parse_type_param(child.attrib['type_param'])]
        if algorithm not in AlgoForPlot: continue
        data.append([dataset, algorithm,
            int(test_data['algo_used_bytes']) / text_length,
            int(test_data['algo_used_bytes']) / trie_size,
            int(test_data['query_time_ms']) / text_length,
            int(test_data['build_time_ms']) / int(test_data['dict_size_in_bytes'])])
        sigma_baseline.append([dataset, algorithm, np.log2(sigma) / 8])

    data_frame = pd.DataFrame(data, columns=[DatasetColumn, AlgorithmColumn, MemoryColumn, MemoryTrieColumn, QueryTimeColumn, BuildTimeColumn])
    sigma_baseline_frame = pd.DataFrame(sigma_baseline, columns=[DatasetColumn, AlgorithmColumn, Sigma])

    draw_barplot(data_frame, QueryTimeColumn, 'milliseconds per letter', axes[0])

    draw_barplot(data_frame, MemoryColumn, 'bytes per text letter', axes[1], need_set_xtitle=True)
    draw_sigma_baseline(sigma_baseline_frame, Sigma, 'bytes per text letter', axes[1])
    axes[1].set_xlabel(DatasetColumn)

    draw_barplot(data_frame, MemoryTrieColumn, 'bytes per trie edge', axes[2])
    draw_sigma_baseline(sigma_baseline_frame, Sigma, 'bytes per trie edge', axes[2])

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center', ncol=4)

    fig.savefig(args.dest)

if __name__ == '__main__':
    main()
