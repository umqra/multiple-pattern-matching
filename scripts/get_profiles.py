#!/usr/bin/python3

import os
import pathlib
import argparse
import subprocess

parser = argparse.ArgumentParser(description='Run gprof/perf/callgrind profile tools')
parser.add_argument('--gprof', action='store_true')

parser.add_argument('--perf', action='store_true')
parser.add_argument('--perf_rate', default=5000, type=int, help='Sample rate of the perf tool')
parser.add_argument('--flamegraph_path', help='Path to the FlameGraph directory with scripts')

parser.add_argument('--callgrind', action='store_true')
parser.add_argument('--sample_percent', default=0.1, type=float, help='Percent of original dataset that should be processed by callgrind profiler')

parser.add_argument('--perf_tests', default='./tests/perf_tests', help='Specify target binary that contains performance tests')
parser.add_argument('--gtest_filter')

def run_process(command):
    pipe = subprocess.PIPE
    process = subprocess.Popen(command, stdout=pipe, shell=True)
    line = "Run '{}'".format(command if type(command) is str else ' '.join(command)) + os.linesep
    while line != '':
        print(line, end='')
        line = process.stdout.readline()
        try:
            line = line.decode()
        except UnicodeDecodeError:
            line = '?' * 10 + os.linesep

def get_directory(directory):
    pathlib.Path(directory).mkdir(parents=True, exist_ok=True)
    return directory

def normalize_line(line):
    if line and line[0] == ' ':
        return line.strip()
    return ''

def get_tests_list(perf_tests, gtest_filter):
    command = '{} --gtest_list_tests'.format(perf_tests)
    if gtest_filter:
        command += ' --gtest_filter="{}"'.format(gtest_filter)
    print(command)
    tests_list = subprocess.check_output(command, shell=True)
    return list(filter(lambda s: s != '', map(normalize_line, tests_list.decode().split('\n'))))

class PerfRunner:
    def __init__(self, gtest_filter, perf_tests, perf_rate, flamegraph_path, sample_percent):
        self.gtest_filter = gtest_filter
        self.perf_tests = perf_tests
        self.perf_rate = perf_rate
        self.flamegraph_path = flamegraph_path
        self.sample_percent = sample_percent
        self.tests_list = get_tests_list(perf_tests, gtest_filter)
        self.build_targets()

    def build_targets(self):
        run_process('cmake .. -DCMAKE_BUILD_TYPE=Profile')
        run_process('make')

    def run_gprof(self):
        prefix = 'gprof: '
        target_dir = get_directory('./profile/gprof/')
        for test in self.tests_list:
            print(prefix + "'{}'".format(test))
            run_process('{} --gtest_filter="*{}*" --base_dir {}'.format(self.perf_tests, test, './tests/data'))
            analysis_file = pathlib.Path(target_dir, '{}.profile'.format(test))
            print(prefix + "store analysis of profiling in the '{}'".format(analysis_file))
            subprocess.call('gprof {} gmon.out > {}'.format(self.perf_tests, analysis_file), shell=True)

    def run_perf(self):
        if os.getuid() != 0:
            print("You must be a root to run 'perf' linux tools. Run 'sudo ./get_profile ...'")
            return
        prefix = 'perf (rate {}): '.format(self.perf_rate)
        target_dir = get_directory('./profile/perf/')
        for test in self.tests_list:
            print(prefix + "'{}'".format(self.perf_rate, test))
            run_process('perf record -F {} -a -g -- {} --gtest_filter="*{}*" --base_dir {} > /dev/null'.format(self.perf_rate, self.perf_tests, test, './tests/data'))
            analysis_file = pathlib.Path(target_dir, '{}.profile'.format(test))
            print(prefix + "store raw analysis of profiling in the '{}'".format(analysis_file))
            run_process('perf report > {} 2> /dev/null'.format(analysis_file))
            if self.flamegraph_path:
                flamegraph_file = pathlib.Path(target_dir, '{}.svg'.format(test))
                temp_file = pathlib.Path(target_dir, 'out.per-folded')
                print(prefix + "store flamegraph in the '{}'".format(self.perf_rate, flamegraph_file))
                run_process('perf script | {} > {}'.format(pathlib.Path(self.flamegraph_path, 'stackcollapse-perf.pl'), temp_file))
                run_process('{} {} > {}'.format(pathlib.Path(self.flamegraph_path, 'flamegraph.pl'), temp_file, flamegraph_file))
                os.remove(temp_file)

    def run_callgrind(self):
        prefix = 'callgrind: '
        target_dir = get_directory('./profile/callgrind/')
        for test in self.tests_list:
            print(prefix + "'{}'".format(test))
            callgrind_output = pathlib.Path(target_dir, '{}.callgrind.out'.format(test))

            print(prefix + "store raw callgrind output in the '{}'".format(callgrind_output))
            run_process('valgrind --tool=callgrind --callgrind-out-file="{}" {} --gtest_filter="*{}" --base_dir {}'.format(
                callgrind_output, self.perf_tests, test, './tests/data'))

def main():
    args = parser.parse_args()
    runner = PerfRunner(args.gtest_filter, args.perf_tests, args.perf_rate, args.flamegraph_path, args.sample_percent)
    if args.gprof:
        runner.run_gprof()
    if args.callgrind:
        runner.run_callgrind()
    if args.perf:
        runner.run_perf()

if __name__ == '__main__':
    main()

