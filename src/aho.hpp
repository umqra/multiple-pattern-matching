#ifndef AHO_HPP_INCLUDED
#define AHO_HPP_INCLUDED

#include "aho_build_tools.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "raw_trie.hpp"
#include "transitions.h"
#include <algorithm>
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

class aho_korasick {
private:
    // TODO: initialize all this fields?
    std::vector<bool> terminal_vertex;
    std::vector<size_t> word_length;

    transitions_map trans;
    std::vector<size_t> failure_link;
    std::vector<size_t> report_link;
    raw_trie trie;

public:
    using size_type = size_t; // required by SDSL for serialize/load

    aho_korasick(const dictionary& dict)
        : trie({ 0 }, { 0 })
    {
        trans = transitions_map(dict);
        trie = raw_trie(dict);

        failure_link = failure_link_calculator<transitions_map>(trie, trans).failure_link; // TODO: std::move instead of assignment?

        init_terminal_vertices_info(dict);

        report_link = report_link_calculator(trie, failure_link, terminal_vertex).report_link;
    }

    size_t size_in_bytes() const // estimated size in bytes
    {
        size_t vector_bodies_size = (terminal_vertex.size() + 7) / 8 + (word_length.size() + failure_link.size() + report_link.size()) * sizeof(size_t);
        size_t vector_data_size = 4 * sizeof(void*) + 4 * sizeof(size_t);
        size_t structures_size = trans.size_in_bytes() + trie.size_in_bytes();
        return vector_bodies_size + vector_data_size + structures_size;
    }

    size_t size() { return trie.size(); }

    size_t get_next_transition(size_t vertex, const string_seq& text,
        size_t position) const
    {
        size_t symbol = text[position];
        size_t next_vertex;
        while (vertex != root() && !trans.try_next(vertex, symbol, next_vertex))
            vertex = failure_link[vertex];
        return trans.try_next(vertex, symbol, next_vertex) ? next_vertex : root();
    }

    // TODO: aggresive inline?
    bool is_terminal_vertex(size_t vertex) const
    {
        return terminal_vertex[vertex];
    }
    size_t get_word_length(size_t word_id) const { return word_length[word_id]; }
    size_t get_report_link(size_t vertex) const { return report_link[vertex]; }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        throw std::logic_error("serialization is not supported for Aho-Korasick automaton");
    }
    void load(std::istream& in)
    {
        throw std::logic_error("serialization is not supported for Aho-Korasick automaton");
    }

private:
    size_t root() const { return 0; }

    void init_terminal_vertices_info(const dictionary& dict)
    {
        auto terminal_vertices_list = terminal_vertices_calculator(dict).terminal_vertices_list;
        terminal_vertex = std::vector<bool>(trie.size());
        word_length = std::vector<size_t>(trie.size());
        for (const auto& v_len : terminal_vertices_list) {
            terminal_vertex[v_len.first] = true;
            word_length[v_len.first] = v_len.second;
        }
    }
};

#endif // AHO_HPP_INCLUDED
