#ifndef AHO_BUILD_TOOLS_HPP
#define AHO_BUILD_TOOLS_HPP

#include "raw_trie.hpp"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <stddef.h>
#include <vector>

struct occurence {
    size_t left_pos, right_pos;
    occurence(size_t _left_pos, size_t _right_pos)
        : left_pos(_left_pos)
        , right_pos(_right_pos)
    {
    }
    bool operator==(const occurence& o) const
    {
        return left_pos == o.left_pos && right_pos == o.right_pos;
    }
    bool operator<(const occurence& o) const
    {
        return std::make_pair(left_pos, right_pos) < std::make_pair(o.left_pos, o.right_pos);
    }
};

std::ostream& operator<<(std::ostream& ostream, const occurence& occ)
{
    return ostream << "(" << occ.left_pos << ", " << occ.right_pos << ")";
}

class terminal_vertices_calculator {
private:
    const dictionary& dict;
    size_t trie_size;

public:
    std::vector<std::pair<size_t, size_t>> terminal_vertices_list;
    terminal_vertices_calculator(const dictionary& _dict)
        : dict(_dict)
    {
        calc_terminal_vertices_list();
    }

private:
    void calc_terminal_vertices_list()
    {
        trie_size = 1;
        trie_iterator iterator(dict);
        while (iterator.move()) {
            trie_size++;
            if (iterator.is_terminal()) {
                size_t word_id = terminal_vertices_list.size();
                terminal_vertices_list.emplace_back(iterator.to(), dict[word_id].length());
            }
        }
    }
};

class report_link_calculator {
private:
    const raw_trie& trie;
    const std::vector<size_t>& failure_link;
    const std::vector<bool>& terminal_vertex;

public:
    std::vector<size_t> report_link;
    report_link_calculator(const raw_trie& _trie,
        const std::vector<size_t>& _failure_link,
        const std::vector<bool>& _terminal_vertex)
        : trie(_trie)
        , failure_link(_failure_link)
        , terminal_vertex(_terminal_vertex)
    {
        calc_report_links();
    }

private:
    size_t root() const { return 0; }

    void calc_report_links()
    {
        report_link = std::vector<size_t>(trie.size(), 0);
        for (size_t vertex = 0; vertex < trie.size(); vertex++) {
            size_t link = failure_link[vertex];
            while (link != root() && !terminal_vertex[link])
                link = failure_link[link];
            report_link[vertex] = link;
        }
    }
};

template <typename transitions_type>
class failure_link_calculator {
private:
    const raw_trie& trie;
    const transitions_type& trans;

    std::vector<bool> calculated_failure;

public:
    std::vector<size_t> failure_link;

    failure_link_calculator(const raw_trie& _trie, const transitions_type& _trans)
        : trie(_trie)
        , trans(_trans)
    {
        calc_failure_links();
    }

private:
    void calc_failure_links()
    {
        failure_link = std::vector<size_t>(trie.size(), 0);
        calculated_failure = std::vector<bool>(trie.size(), false);

        for (size_t vertex = 1; vertex < trie.size(); vertex++) {
            if (!calculated_failure[vertex])
                calc_link(vertex);
        }
    }
    size_t root() const { return 0; }
    // TODO: somehow improve this ugly part?
    void calc_link(size_t vertex)
    {
        static std::vector<std::pair<size_t, std::pair<size_t, size_t>>>
            need_to_calc;
        need_to_calc.resize(trie.size());
        size_t count = 0;
        need_to_calc[count++] = std::make_pair(
            trie.parent[vertex], std::make_pair(vertex, trie.parent_char[vertex]));
        size_t current = trie.parent[vertex];
        size_t link;
        while (count > 0) {
            size_t v;
            if (need_to_calc[count - 1].first != current && trans.try_next(current, need_to_calc[count - 1].second.second, v)) {
                current = v;
                calculated_failure[need_to_calc[count - 1].second.first] = true;
                failure_link[need_to_calc[--count].second.first] = current;
            } else if (current == root()) {
                calculated_failure[need_to_calc[count - 1].second.first] = true;
                failure_link[need_to_calc[--count].second.first] = current;
            } else if (is_link_calculated(current, link)) {
                current = link;
            } else {
                need_to_calc[count++] = std::make_pair(trie.parent[current],
                    std::make_pair(current, trie.parent_char[current]));
                current = trie.parent[current];
            }
        }
    }
    bool is_link_calculated(size_t vertex, size_t& link)
    {
        link = 0;
        if (calculated_failure[vertex]) {
            link = failure_link[vertex];
            return true;
        }
        if (trie.parent[vertex] == root() || vertex == root())
            return true;
        return false;
    }
};

// TODO: move to another helper class?
std::vector<std::pair<size_t, size_t>> get_symbol_vertex_order(const raw_trie& trie)
{
    assert(trie.size() > 0);
    auto edge_order = std::vector<size_t>(trie.size() - 1);
    std::iota(edge_order.begin(), edge_order.end(), 1);
    std::sort(edge_order.begin(), edge_order.end(),
        [&trie](const int& v1, const int& v2) {
            return std::make_pair(trie.parent_char[v1], trie.parent[v1]) < std::make_pair(trie.parent_char[v2], trie.parent[v2]);
        });

    auto symbol_vertex = std::vector<std::pair<size_t, size_t>>(trie.size() - 1);
    for (size_t id = 0; id < trie.size() - 1; id++) {
        size_t v = edge_order[id];
        symbol_vertex[id] = std::make_pair(trie.parent_char[v], trie.parent[v]);
    }
    return symbol_vertex;
}

#endif // AHO_BUILD_TOOLS_HPP
