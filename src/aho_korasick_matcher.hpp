#ifndef AHO_KORASICK_MATCHER_HPP
#define AHO_KORASICK_MATCHER_HPP

#include "aho.hpp"
#include "aho_build_tools.hpp"
#include "dict_support_info.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "string_seq.hpp"
#include "transitions.h"

#include <cstdlib>
#include <sdsl/io.hpp>
#include <string>
#include <vector>

#include <iostream>

class aho_korasick_matcher : public matcher_base {
private:
    aho_korasick ak;

public:
    aho_korasick_matcher() = delete; // no default since no serialization
    aho_korasick_matcher(const aho_korasick_matcher&) = default;
    aho_korasick_matcher(aho_korasick_matcher&&) = default;
    aho_korasick_matcher& operator=(const aho_korasick_matcher&) = default;
    aho_korasick_matcher& operator=(aho_korasick_matcher&&) = default;

    explicit aho_korasick_matcher(const dictionary& dict)
        : ak(dict)
    {
    }

    std::vector<occurence> enumerate_occurence_positions(const string_seq& text) const override
    {
        std::vector<occurence> occurences = {};
        size_t current_vertex = 0;
        for (size_t position = 0; position < text.length(); position++) {
            current_vertex = ak.get_next_transition(current_vertex, text, position);
            add_occurence_positions(position, current_vertex, occurences);
        }
        return occurences;
    }

    size_t size_in_bytes() const // estimated size in bytes for diagnostics
    {
        return ak.size_in_bytes();
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const override
    {
        throw std::logic_error("serialization is not supported for Aho-Korasick automaton");
    }
    void load(std::istream& in) override
    {
        throw std::logic_error("serialization is not supported for Aho-Korasick automaton");
    }

private:
    size_t root() const { return 0; }

    void add_occurence_positions(size_t position, size_t vertex,
        std::vector<occurence>& occurences) const
    {
        if (ak.is_terminal_vertex(vertex))
            occurences.push_back(
                occurence(position - ak.get_word_length(vertex) + 1, position));
        vertex = ak.get_report_link(vertex);
        while (vertex != root()) {
            occurences.push_back(
                occurence(position - ak.get_word_length(vertex) + 1, position));
            vertex = ak.get_report_link(vertex);
        }
    }
};

#endif // AHO_KORASICK_MATCHER_HPP
