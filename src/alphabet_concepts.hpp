#ifndef ALPHABET_CONCEPTS_HPP
#define ALPHABET_CONCEPTS_HPP

#include <locale>
#include <string>

struct byte_alphabet {
    using string_type = std::string;
    static const uint8_t char_width = 1;
};

struct utf16_alphabet {
    using string_type = std::u16string;
    static const uint8_t char_width = 2;
};

struct utf32_alphabet {
    using string_type = std::u32string;
    static const uint8_t char_width = 4;
};

#endif // ALPHABET_CONCEPTS_HPP
