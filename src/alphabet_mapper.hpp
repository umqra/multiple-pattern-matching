#ifndef ALPHABET_MAPPER_HPP
#define ALPHABET_MAPPER_HPP

#include <algorithm>
#include <iterator>
#include <sdsl/io.hpp>
#include <stdint.h>
#include <vector>

class alphabet_mapper {
    std::vector<uint16_t> map;
    static constexpr uint16_t k_invalid_value = std::numeric_limits<uint16_t>::max();

public:
    using size_type = size_t; // required for serialize/load

    alphabet_mapper() = default;
    alphabet_mapper(const alphabet_mapper&) = default;
    alphabet_mapper(alphabet_mapper&&) = default;
    alphabet_mapper& operator=(const alphabet_mapper&) = default;
    alphabet_mapper& operator=(alphabet_mapper&&) = default;

    template <typename iter_type>
    alphabet_mapper(iter_type chars_begin, iter_type chars_end)
    {
        static_assert(std::is_convertible<typename std::iterator_traits<iter_type>::value_type, uint32_t>::value, "");
        if (chars_begin == chars_end) {
            map = std::vector<uint16_t>();
            return;
        }
        map = std::vector<uint16_t>(*std::max_element(chars_begin, chars_end) + 1);
        for (auto iter = chars_begin; iter != chars_end; ++iter)
            map[*iter] = 1;
        for (size_t ch = 0, rnk = 0; ch < map.size(); ++ch)
            if (map[ch] == 1)
                map[ch] = rnk++;
            else
                map[ch] = k_invalid_value;
    }

    size_t size() const { return map.size(); }

    size_t effective_alphabet_size() const
    {
        return std::count_if(map.begin(), map.end(), [](uint16_t x) { return x != k_invalid_value; });
    }

    inline size_t operator[](size_t symbol) const
    {
        if (symbol >= map.size())
            return k_invalid_value;
        return map[symbol];
    }

    static inline bool is_valid_mapped_symbol(size_t symbol)
    {
        return symbol != k_invalid_value;
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += sdsl::serialize(map, out, child, "map");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    void load(std::istream& in) { sdsl::load(map, in); }
};

#endif
