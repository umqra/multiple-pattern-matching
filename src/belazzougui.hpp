#ifndef BELAZZOUGUI_HPP
#define BELAZZOUGUI_HPP

#include "aho_build_tools.hpp"
#include "code_utils.hpp"
#include "dfuds_tree.hpp"
#include "dict_support_info.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "raw_trie.hpp"
#include "string_seq.hpp"
#include "transitions.h"
#include "trees.h"
#include "trie_path_array.hpp"
#include "type_trait_tools.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sdsl/bit_vectors.hpp>
#include <type_traits>
#include <vector>

#include "dfuds_tree.hpp"

template <typename transitions_type = transitions_succinct_map<>,
    typename failure_link_tree_type = dfuds_tree<>,
    typename report_link_tree_type = ultra_dfuds_tree<>,
    typename dict_support_info_type = dict_support_info<>>
class belazzougui {
private:
    transitions_type trans;
    failure_link_tree_type failure_tree;
    report_link_tree_type report_tree;
    dict_support_info_type support_info;

public:
    using size_type = size_t; // required by SDSL for serialize/load

    belazzougui() = default;
    belazzougui(const belazzougui&) = default;
    belazzougui(belazzougui&&) = default;
    belazzougui& operator=(const belazzougui&) = default;
    belazzougui& operator=(belazzougui&&) = default;

    belazzougui(raw_trie trie, const dictionary& dict)
    {
        dbg("Build trie_array\n");
        auto trie_array = trie_path_array(trie);

        dbg("Permute raw_trie\n");
        trie = trie.permute(trie_array.vertices_order, trie_array.i_vertices_order);

        dbg("Build trans\n");
        trans = transitions_type(trie);

        dbg("Calculate failure links\n");
        auto failure_link = failure_link_calculator<transitions_type>(trie, trans).failure_link;

        dbg("Build failure tree\n");
        failure_tree = failure_link_tree_type(calc_tree_degrees(failure_link));

        dbg("Build support info\n");
        support_info = dict_support_info_type(dict, trie_array);

        auto terminal_vertices_tmp_mask = std::vector<bool>(trie.size());
        for (const auto& id : support_info.get_vertices_list()) {
            terminal_vertices_tmp_mask[id] = 1;
        }
        dbg("Calculate report links\n");
        auto report_link = report_link_calculator(trie, failure_link, terminal_vertices_tmp_mask)
                               .report_link;
        dbg("Build report links\n");
        report_tree = report_link_tree_type(calc_tree_degrees(report_link));
    }

    template <typename T = failure_link_tree_type>
    typename std::enable_if<is_full_tree<T>::value, size_t>::type
    get_next_transition(size_t vertex, const string_seq& text, size_t position) const
    {
        size_t symbol = text[position];
        size_t next;
        bool has_next = false;
        while (!(has_next = trans.try_next(vertex, symbol, next)) && vertex != 0)
            vertex = failure_tree.parent(vertex);
        return has_next ? next : 0;
    }

    template <typename T = failure_link_tree_type>
    typename std::enable_if<!is_full_tree<T>::value, size_t>::type
    get_next_transition(size_t vertex, const string_seq& text,
        size_t position) const
    {
        size_t final_position = position;
        while (position <= final_position) {
            size_t next;
            while (position <= final_position && trans.try_next(vertex, text[position], next)) {
                vertex = next;
                position++;
            }
            if (position > final_position)
                break;
            size_t parent;
            while (vertex != 0 && !failure_tree.try_get_parent(vertex, parent))
                vertex = trans.parent(vertex, text[--position]);
            if (vertex == 0)
                position++;
            else
                vertex = parent;
        }
        return vertex;
    }

    bool is_terminal_vertex(size_t vertex) const
    {
        return support_info.is_terminal_vertex(vertex);
    }
    size_t get_word_length(size_t vertex) const
    {
        return support_info.get_word_length(vertex);
    }
    size_t get_report_link(size_t vertex) const
    {
        return report_tree.parent(vertex);
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += trans.serialize(out, child, "trans");
        written_bytes += failure_tree.serialize(out, child, "failure_tree");
        written_bytes += report_tree.serialize(out, child, "report_tree");
        written_bytes += support_info.serialize(out, child, "support_info");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        trans.load(in);
        failure_tree.load(in);
        report_tree.load(in);
        support_info.load(in);
    }

private:
    std::vector<size_t> calc_tree_degrees(const std::vector<size_t>& parent)
    {
        auto degrees = std::vector<size_t>(parent.size());
        for (size_t v = 1; v < parent.size(); v++) {
            degrees[parent[v]]++;
        }
        return degrees;
    }
};

#endif // BELAZZOUGUI_HPP
