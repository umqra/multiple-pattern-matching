#ifndef BP_SUPPORT_SADA_LIGHT_HPP_INCLUDED
#define BP_SUPPORT_SADA_LIGHT_HPP_INCLUDED


#include <cstdint>
#include <sdsl/rank_support_v5.hpp>
#include <sdsl/bp_support_algorithm.hpp>

template<uint32_t t_sml_blk = 256, uint32_t t_med_deg = 32, class t_rank = sdsl::rank_support_v5<>>
class bp_support_sada_light
{
public:
    typedef sdsl::bit_vector::size_type       size_type;
    typedef sdsl::bit_vector::difference_type difference_type;
    typedef sdsl::int_vector<>                sml_block_array_type;
    typedef sdsl::int_vector<>                med_block_array_type;
    typedef t_rank                      rank_type;
private:
    static_assert(0 < t_sml_blk, "bp_support_sada_light: t_sml_blk should be greater than 0!");
    const sdsl::bit_vector* m_bp = nullptr;
    rank_type  m_bp_rank;

    sml_block_array_type  m_sml_block_min_max;
    med_block_array_type  m_med_block_min_max;

    size_type m_size             = 0; // number of supported parentheses
    size_type m_sml_blocks       = 0; // number of small sized blocks
    size_type m_med_blocks       = 0; // number of medium sized blocks
    size_type m_med_inner_blocks = 0; // number of inner nodes in the min max tree of the medium sized blocks

    void copy(const bp_support_sada_light& bp_support)
    {
        m_bp        = bp_support.m_bp;
        m_bp_rank   = bp_support.m_bp_rank;
        m_bp_rank.set_vector(m_bp);

        m_sml_block_min_max = bp_support.m_sml_block_min_max;
        m_med_block_min_max = bp_support.m_med_block_min_max;

        m_size             = bp_support.m_size;
        m_sml_blocks       = bp_support.m_sml_blocks;
        m_med_blocks       = bp_support.m_med_blocks;
        m_med_inner_blocks = bp_support.m_med_inner_blocks;
    }

    inline static size_type sml_block_idx(size_type i)
    {
        return i/t_sml_blk;
    }

    inline static size_type med_block_idx(size_type i)
    {
        return i/(t_sml_blk*t_med_deg);
    }

    inline static bool is_root(size_type v)
    {
        return v==0;
    }

    inline static bool is_right_child(size_type v)
    {
        assert(!is_root(v));
        return !(v%2);
    }

    inline static size_type parent(size_type v)
    {
        assert(!is_root(v));
        return (v-1)/2;
    }

    inline static size_type right_child(size_type v)
    {
        return 2*v+2;
    }

    inline static size_type left_sibling(size_type v)
    {
        return --v;
    }

    inline bool is_leaf(size_type v)const
    {
        return v >= m_med_inner_blocks;
    }

    inline difference_type min_value(size_type v)const
    {
        return m_size-((difference_type)m_med_block_min_max[2*v]);
    }

    inline difference_type max_value(size_type v)const
    {
        return m_med_block_min_max[2*v+1]-m_size;
    }

    //! Calculate the maximal parenthesis \f$ j<i \f$ with \f$ excess(j) = excess(i)+rel \f$
    /*! \param i     The index of a parenthesis in the supported sequence.
     *  \param rel  The excess difference to the excess value of parenthesis \f$i\f$.
     *  \return     If there exists a parenthesis \f$i<j\f$ with \f$ excess(j) = excess(i)+rel\f$, \f$j\f$ is returned
     *                otherwise size().
     */
    size_type bwd_excess(size_type i, difference_type rel)const
    {
        size_type j;
        if (i == 0) {
            return rel == 0 ? -1 : size();
        }
        // (1) search the small block for the answer
        if ((j = sdsl::near_bwd_excess(*m_bp, i-1, rel, t_sml_blk)) < i or j == (size_type)-1) {
            return j;
        }
        difference_type desired_excess = excess(i)+rel;
        // (2) scan the small blocks of the current median block for an answer
        if ((j = bwd_excess_in_med_block(sml_block_idx(i)-1, desired_excess)) != size()) {
            return j;
        }
        // (3) search the min-max tree of the medium blocks for the right med block
        if (med_block_idx(i) == 0) { // if we are already in the first medium block => we are done
            if (desired_excess == 0)
                return -1;
            return size();
        }
        size_type v    = m_med_inner_blocks + med_block_idx(i);
        // (3 a) go up the tree
        while (!is_root(v)) {
            if (is_right_child(v)) { // if the node is a right child
                v = left_sibling(v); // choose left sibling
                if (min_value(v) <= desired_excess and desired_excess <= max_value(v))  // found solution
                    break;
            }
            v = parent(v); // choose parent
        }
        // (3 b) go down the tree
        if (!is_root(v)) { // found solution for the query
            while (!is_leaf(v)) {
                v = right_child(v); // choose  child
                if (!(min_value(v) <= desired_excess and desired_excess <= max_value(v))) {
                    v = left_sibling(v); // choose left child == left sibling of the right child
                    assert((min_value(v) <= desired_excess and desired_excess <= max_value(v)));
                }
            }
            return bwd_excess_in_med_block((v-m_med_inner_blocks)*t_med_deg+(t_med_deg-1), desired_excess);
        } else if (desired_excess == 0) {
            return -1;
        }
        // no solution found
        return size();
    }

    //! Calculate the maximal parentheses \f$ j \leq sml_block_idx\cdot t_sml_blk+(t_sml_blk-1) \f$ with \f$ excess(j)=desired\_excess \f$
    size_type bwd_excess_in_med_block(size_type sml_block_idx, difference_type desired_excess)const
    {
        // get the first small block in the medium block right to the current med block
        size_type first_sml_block_in_med_block = (med_block_idx(sml_block_idx*t_sml_blk))*t_med_deg;

        while ((sml_block_idx+1) and sml_block_idx >= first_sml_block_in_med_block) {
            difference_type ex         = (sml_block_idx == 0) ? 0 : excess(sml_block_idx*t_sml_blk-1);
            difference_type min_ex     = ex + (1 - ((difference_type)m_sml_block_min_max[2*sml_block_idx]));
            difference_type max_ex    = ex + (m_sml_block_min_max[2*sml_block_idx+1] - 1);

            if (min_ex <= desired_excess and desired_excess <= max_ex) {
                size_type j = near_bwd_excess(*m_bp, (sml_block_idx+1)*t_sml_blk-1, desired_excess-excess((sml_block_idx+1)*t_sml_blk), t_sml_blk);
                return j;
            }
            --sml_block_idx;
        }
        if (sml_block_idx == 0 and desired_excess == 0)
            return -1;
        return size();
    }

public:
    bp_support_sada_light() {}

    //! Constructor
    explicit bp_support_sada_light(const sdsl::bit_vector* bp): m_bp(bp),
                                                    m_size(bp==nullptr?0:bp->size()),
                                                    m_sml_blocks((m_size+t_sml_blk-1)/t_sml_blk),
                                                    m_med_blocks((m_size+t_sml_blk* t_med_deg-1)/(t_sml_blk* t_med_deg)),
                                                    m_med_inner_blocks(0)
    {
        if (bp == nullptr or bp->size()==0)
            return;
        // initialize rank and select
        sdsl::util::init_support(m_bp_rank, bp);

        m_med_inner_blocks = 1;
        // m_med_inner_blocks = (next power of 2 greater than or equal to m_med_blocks)-1
        while (m_med_inner_blocks < m_med_blocks) {
            m_med_inner_blocks <<= 1; assert(m_med_inner_blocks!=0);
        }
        --m_med_inner_blocks;
        assert((m_med_inner_blocks == 0) or(m_med_inner_blocks%2==1));

        m_sml_block_min_max = sdsl::int_vector<>(2*m_sml_blocks, 0, sdsl::bits::hi(t_sml_blk+2)+1);
        m_med_block_min_max = sdsl::int_vector<>(2*(m_med_blocks+m_med_inner_blocks), 0, sdsl::bits::hi(2*m_size+2)+1);

        // calculate min/max excess values of the small blocks and medium blocks
        difference_type min_ex = 1, max_ex = -1, curr_rel_ex = 0, curr_abs_ex = 0;
        for (size_type i=0; i < m_size; ++i) {
            if ((*bp)[i])
                ++curr_rel_ex;
            else
                --curr_rel_ex;
            if (curr_rel_ex > max_ex) max_ex = curr_rel_ex;
            if (curr_rel_ex < min_ex) min_ex = curr_rel_ex;
            if ((i+1)%t_sml_blk == 0 or i+1 == m_size) {
                size_type sidx = i/t_sml_blk;
                m_sml_block_min_max[2*sidx    ] = -(min_ex-1);
                m_sml_block_min_max[2*sidx + 1] = max_ex+1;

                size_type v = m_med_inner_blocks + sidx/t_med_deg;

                if ((difference_type)(-(curr_abs_ex + min_ex)+m_size) > ((difference_type)m_med_block_min_max[2*v])) {
                    assert(curr_abs_ex+min_ex <= min_value(v));
                    m_med_block_min_max[2*v] = -(curr_abs_ex + min_ex)+m_size;
                }

                if ((difference_type)(curr_abs_ex + max_ex + m_size) > (difference_type)m_med_block_min_max[2*v + 1])
                    m_med_block_min_max[2*v + 1] = curr_abs_ex + max_ex + m_size;

                curr_abs_ex += curr_rel_ex;
                min_ex = 1; max_ex = -1; curr_rel_ex = 0;
            }
        }

        for (size_type v = m_med_block_min_max.size()/2 - 1; !is_root(v); --v) {
            size_type p = parent(v);
            if (min_value(v) < min_value(p))  // update minimum
                m_med_block_min_max[2*p] = m_med_block_min_max[2*v];
            if (max_value(v) > max_value(p))  // update maximum
                m_med_block_min_max[2*p+1] = m_med_block_min_max[2*v+1];
        }
    }

    //! Copy constructor
    bp_support_sada_light(const bp_support_sada_light& bp_support)
    {
        copy(bp_support);
    }

    //! Move constructor
    bp_support_sada_light(bp_support_sada_light&& bp_support)
    {
        *this = std::move(bp_support);
    }

    //! Assignment operator
    bp_support_sada_light& operator=(bp_support_sada_light&& bp_support)
    {
        if (this != &bp_support) {
            m_bp        = std::move(bp_support.m_bp);
            m_bp_rank   = std::move(bp_support.m_bp_rank);
            m_bp_rank.set_vector(m_bp);

            m_sml_block_min_max = std::move(bp_support.m_sml_block_min_max);
            m_med_block_min_max = std::move(bp_support.m_med_block_min_max);

            m_size             = std::move(bp_support.m_size);
            m_sml_blocks       = std::move(bp_support.m_sml_blocks);
            m_med_blocks       = std::move(bp_support.m_med_blocks);
            m_med_inner_blocks = std::move(bp_support.m_med_inner_blocks);
        }
        return *this;
    }

    //! Swap method
    /*! Swaps the content of the two data structure.
     *  You have to use set_vector to adjust the supported bit_vector.
     *  \param bp_support Object which is swapped.
     */
    void swap(bp_support_sada_light& bp_support)
    {
        // m_bp.swap(bp_support.m_bp); use set_vector to set the supported bit_vector
        m_bp_rank.swap(bp_support.m_bp_rank);

        m_sml_block_min_max.swap(bp_support.m_sml_block_min_max);
        m_med_block_min_max.swap(bp_support.m_med_block_min_max);

        std::swap(m_size, bp_support.m_size);
        std::swap(m_sml_blocks, bp_support.m_sml_blocks);
        std::swap(m_med_blocks, bp_support.m_med_blocks);
        std::swap(m_med_inner_blocks, bp_support.m_med_inner_blocks);
    }

    //! Assignment operator
    bp_support_sada_light& operator=(const bp_support_sada_light& bp_support)
    {
        if (this != &bp_support) {
            copy(bp_support);
        }
        return *this;
    }

    void set_vector(const sdsl::bit_vector* bp)
    {
        m_bp = bp;
        m_bp_rank.set_vector(bp);
    }

    /*! Calculates the excess value at index i.
     * \param i The index of which the excess value should be calculated.
     */
    inline difference_type excess(size_type i)const
    {
        return (m_bp_rank(i+1)<<1)-i-1;
    }

    //! Calculate the matching opening parenthesis to the closing parenthesis at position i
    /*! \param i Index of a closing parenthesis.
      * \return * i, if the parenthesis at index i is closing,
      *         * the position j of the matching opening parenthesis, if a matching parenthesis exists,
      *         * size() if no matching closing parenthesis exists.
      */
    size_type find_open(size_type i)const
    {
        assert(0 <= i && i < m_size);
        assert((*m_bp)[i] == 0);

        size_type bwd_ex = bwd_excess(i, 0);
        if (bwd_ex == size())
            return size();
        else
            return bwd_ex+1;
    }

    /*! The size of the supported balanced parentheses sequence.
     * \return the size of the supported balanced parentheses sequence.
     */
    size_type size() const
    {
        return m_size;
    }

    //! Serializes the bp_support_sada to a stream.
    /*!
     * \param out The outstream to which the data structure is written.
     * \return The number of bytes written to out.
     */
    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v=nullptr, std::string name="")const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_type written_bytes = 0;
        written_bytes += sdsl::write_member(m_size, out, child, "size");
        written_bytes += sdsl::write_member(m_sml_blocks, out, child, "sml_block_cnt");
        written_bytes += sdsl::write_member(m_med_blocks, out, child, "med_block_cnt");
        written_bytes += sdsl::write_member(m_med_inner_blocks, out, child, "med_inner_blocks");

        written_bytes += m_bp_rank.serialize(out, child, "bp_rank");

        written_bytes += m_sml_block_min_max.serialize(out, child, "sml_blocks");
        written_bytes += m_med_block_min_max.serialize(out, child, "med_blocks");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    //! Load the bp_support_sada for a bit_vector v.
    /*!
     * \param in The instream from which the data structure is read.
     * \param bp Bit vector representing a balanced parentheses sequence that is supported by this data structure.
     */
    void load(std::istream& in, const sdsl::bit_vector* bp)
    {
        m_bp = bp;
        sdsl::read_member(m_size, in);
        assert(m_size == bp->size());
        sdsl::read_member(m_sml_blocks, in);
        sdsl::read_member(m_med_blocks, in);
        sdsl::read_member(m_med_inner_blocks, in);

        m_bp_rank.load(in, m_bp);

        m_sml_block_min_max.load(in);
        m_med_block_min_max.load(in);
    }
};

#endif //BP_SUPPORT_SADA_LIGHT_HPP_INCLUDED
