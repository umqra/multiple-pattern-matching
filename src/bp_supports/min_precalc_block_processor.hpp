#ifndef MIN_PRECALC_BLOCK_PROCESSOR_HPP_INCLUDED
#define MIN_PRECALC_BLOCK_PROCESSOR_HPP_INCLUDED

#include <sdsl/int_vector.hpp>
#include "paired_naive_block_processor.hpp"
#include <iostream>

class MinPrecalcBlockProcessor
{
private:
    static const size_t saved_block_length = 12;
    static int8_t forward_min_balance[1 << saved_block_length];
    static int8_t backward_min_balance[1 << saved_block_length];
    static bool array_initialized;
    PairedNaiveBlockProcessor naive_processor;
    static int bad_hits_count;
    static int total_hits_count;
public:
    explicit MinPrecalcBlockProcessor() : naive_processor()
    {
        if (!array_initialized) {
            array_initialized = true;
            for (size_t mask = 0; mask < (1 << saved_block_length); mask++) {
                int8_t forward_balance = 0;
                int8_t backward_balance = 0;
                forward_min_balance[mask] = 1;
                backward_min_balance[mask] = 1;
                for (size_t i = 0; i < saved_block_length; i++) {
                    if (mask & (1 << i))
                        forward_balance++;
                    else
                        forward_balance--;
                    if (mask & (1 << (saved_block_length - i - 1)))
                        backward_balance--;
                    else
                        backward_balance++;
                    forward_min_balance[mask] = std::min(forward_min_balance[mask], forward_balance);
                    backward_min_balance[mask] = std::min(backward_min_balance[mask], backward_balance);
                }
            }
        }
    }
    int64_t try_find_close(uint64_t block, size_t block_size, size_t &balance) const
    {
        size_t i;
        for (i = 0; i + saved_block_length <= block_size; i += saved_block_length)
        {
            auto current_block = block & ((1 << saved_block_length) - 1);
            auto current_min_balance = forward_min_balance[current_block];
            if (static_cast<int8_t>(balance) + current_min_balance <= 0)
                return i + naive_processor.try_find_close(current_block, saved_block_length, balance);
            balance += 2 * sdsl::bits::cnt(current_block) - saved_block_length;
            block >>= saved_block_length;
        }
        auto last_block_length = (block_size - i);
        auto last_block = block & ((1 << last_block_length) - 1);
        auto last_block_result = naive_processor.try_find_close(last_block, last_block_length, balance);
        return last_block_result == -1 ? -1 : static_cast<int64_t>(i + last_block_result);
    }
    int64_t try_find_open(uint64_t block, size_t block_size, size_t &balance) const
    {
        size_t i;
        for (i = 0; i + saved_block_length <= block_size; i += saved_block_length)
        {
            total_hits_count++;
            auto current_block = (block >> (block_size - i - saved_block_length)) & ((1 << saved_block_length) - 1);
            auto current_min_balance = backward_min_balance[current_block];
            if (static_cast<int8_t>(balance) + current_min_balance <= 0) {
                bad_hits_count++;
                return i + naive_processor.try_find_open(current_block, saved_block_length, balance);
            }
            balance -= 2 * sdsl::bits::cnt(current_block) - saved_block_length;
        }
        auto last_block_length = (block_size - i);
        auto last_block = (block & ((1 << last_block_length) - 1));
        auto last_block_result = naive_processor.try_find_open(last_block, last_block_length, balance);
        return last_block_result == -1 ? -1 : static_cast<int64_t>(i + last_block_result);
    }
    static void reset_rates()
    {
        total_hits_count = bad_hits_count = 0;
    }
    static double get_bad_rate()
    {
        return bad_hits_count * 100.0 / total_hits_count;
    }
};

bool MinPrecalcBlockProcessor::array_initialized = false;
int8_t MinPrecalcBlockProcessor::forward_min_balance[1 << saved_block_length];
int8_t MinPrecalcBlockProcessor::backward_min_balance[1 << saved_block_length];
int MinPrecalcBlockProcessor::bad_hits_count = 0;
int MinPrecalcBlockProcessor::total_hits_count = 0;

#endif //MIN_PRECALC_BLOCK_PROCESSOR_HPP_INCLUDED
