#ifndef NAIVE_BLOCK_PROCESSOR_HPP_INCLUDED
#define NAIVE_BLOCK_PROCESSOR_HPP_INCLUDED

#include <sdsl/int_vector.hpp>

class NaiveBlockProcessor
{
public:
    int64_t try_find_close(uint64_t block, size_t block_size, size_t &balance) const
    {
        for (size_t offset = 0; offset < block_size; offset++)
        {
            balance += 2 * (block & 1) - 1;
            if (balance == 0)
                return offset;
            block >>= 1;
        }
        return -1;
    }
    int64_t try_find_open(uint64_t block, size_t block_size, size_t &balance) const
    {
        for (size_t offset = 0; offset < block_size; offset++)
        {
            balance -= 2 * ((block >> (block_size - offset - 1)) & 1) - 1;
            if (balance == 0)
                return offset;
        }
        return -1;
    }
};

#endif //NAIVE_BLOCK_PROCESSOR_HPP_INCLUDED
