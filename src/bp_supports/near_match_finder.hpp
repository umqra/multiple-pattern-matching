#ifndef NEAR_MATCH_FINDER_HPP_INCLUDED
#define NEAR_MATCH_FINDER_HPP_INCLUDED

#include <sdsl/int_vector.hpp>

template<typename block_processor_type>
class NearMatchFinder
{
private:
    const size_t block_size = 64;
    const sdsl::bit_vector* sequence;
    block_processor_type block_processor;
public:
    explicit NearMatchFinder(const sdsl::bit_vector* _sequence) : sequence(_sequence), block_processor()
    {
    }
    int64_t try_find_close_within_block(size_t start_inclusive, size_t block_size, size_t balance)
    {
        return try_find_close(start_inclusive, (start_inclusive / block_size + 1) * block_size, balance);
    }
    int64_t try_find_open_within_block(size_t start_exclusive, size_t block_size, size_t balance) const
    {
        if (start_exclusive == 0)
            return -1;
        auto end_inclusive = ((start_exclusive - 2) / block_size) * block_size;
        auto match = try_find_open(start_exclusive, end_inclusive, balance);
        return match == -1 ? start_exclusive : match - 1;
    }
    int64_t try_find_close(size_t start_inclusive, size_t end_exclusive, size_t balance)
    {
        size_t length = end_exclusive - start_inclusive;
        size_t full_blocks = length / block_size;
        for (size_t i = 0; i < full_blocks; i++)
        {
            auto current_start = start_inclusive + i * block_size;
            auto block = sequence->get_int(current_start, block_size);
            auto match_in_block = block_processor.try_find_close(block, block_size, balance);
            if (match_in_block != -1)
                return current_start + match_in_block;
        }
        auto adjusted_length = full_blocks * block_size;
        auto last_block_start = start_inclusive + adjusted_length;
        auto last_block_size = static_cast<const uint8_t>(length - adjusted_length);
        if (last_block_size == 0)
            return -1;
        auto last_block = sequence->get_int(last_block_start, last_block_size);
        auto match_in_last_block = block_processor.try_find_close(last_block, last_block_size, balance);
        if (match_in_last_block == -1)
            return -1;
        return last_block_start + match_in_last_block;
    }
    int64_t try_find_open(size_t start_exclusive, size_t end_inclusive, size_t balance) const
    {
        size_t length = start_exclusive - end_inclusive;
        size_t full_blocks = length / block_size;
        for (size_t i = 1; i <= full_blocks; i++)
        {
            auto current_start = start_exclusive - i * block_size;
            auto block = sequence->get_int(current_start, block_size);
            auto match_in_block = block_processor.try_find_open(block, block_size, balance);
            if (match_in_block != -1)
                return current_start + block_size - match_in_block - 1;
        }
        auto adjusted_length = full_blocks * block_size;
        auto last_block_size = static_cast<const uint8_t>(length - adjusted_length);
        if (last_block_size == 0)
            return -1;
        auto last_block = sequence->get_int(end_inclusive, last_block_size);
        auto match_in_last_block = block_processor.try_find_open(last_block, last_block_size, balance);
        if (match_in_last_block == -1)
            return -1;
        return end_inclusive + last_block_size - match_in_last_block - 1;
    }
};
#endif //NEAR_MATCH_FINDER_HPP_INCLUDED
