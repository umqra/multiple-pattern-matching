#ifndef PAIRED_NAIVE_BLOCK_PROCESSOR_INCLUDED_HPP
#define PAIRED_NAIVE_BLOCK_PROCESSOR_INCLUDED_HPP

#include <sdsl/int_vector.hpp>

class PairedNaiveBlockProcessor
{
public:
    int64_t try_find_close(uint64_t block, size_t block_size, size_t &balance) const
    {
        if (block_size == 0)
            return -1;
        size_t offset = 0;
        if (balance & 1) // note (umqra, 09.09.18): We need an even balance to easily process pairs of bits
        {
            balance += 2 * (block & 1) - 1;
            if (balance == 0)
                return 0;
            block >>= 1;
            offset++;
        }
        size_t adjusted_block_size = (block_size - offset) % 2 == 0 ? block_size : block_size - 1;
        for (; offset < adjusted_block_size; offset += 2)
        {
            balance += (block & 3) + (block & 1) - 2;
            if (balance == 0)
                return offset + 1;
            block >>= 2;
        }
        for (; offset < block_size; offset++)
        {
            balance += 2 * (block & 1) - 1;
            if (balance == 0)
                return offset;
            block >>= 1;
        }
        return -1;
    }
    int64_t try_find_open(uint64_t block, size_t block_size, size_t &balance) const
    {
        if (block_size == 0)
            return -1;
        size_t offset = 0;
        if (balance & 1)
        {
            balance -= 2 * ((block >> (block_size - offset - 1)) & 1) - 1;
            if (balance == 0)
                return 0;
            offset++;
        }
        size_t adjusted_block_size = (block_size - offset) % 2 == 0 ? block_size : block_size - 1;
        for (; offset < adjusted_block_size; offset += 2)
        {
            balance -= ((block >> (block_size - offset - 2)) & 3) + ((block >> (block_size - offset - 2)) & 1) - 2;
            if (balance == 0)
                return offset + 1;
        }
        for (; offset < block_size; offset++)
        {
            balance -= 2 * ((block >> (block_size - offset - 1)) & 1) - 1;
            if (balance == 0)
                return offset;
        }
        return -1;
    }
};

#endif //PAIRED_NAIVE_BLOCK_PROCESSOR_INCLUDED_HPP
