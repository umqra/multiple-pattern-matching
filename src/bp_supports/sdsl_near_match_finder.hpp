#ifndef SDSL_NEAR_MATCH_FINDER_HPP_INCLUDED
#define SDSL_NEAR_MATCH_FINDER_HPP_INCLUDED

#include <sdsl/int_vector.hpp>
#include <sdsl/bp_support_algorithm.hpp>

class SdslNearMatchFinder
{
private:
    const sdsl::bit_vector* sequence;
public:
    explicit SdslNearMatchFinder(sdsl::bit_vector* _sequence) : sequence(_sequence)
    {
    }
    int64_t try_find_close(size_t start_inclusive, size_t end_exclusive, size_t balance)
    {
        auto close_position = sdsl::near_fwd_excess(*sequence, start_inclusive, -(int64_t)balance, end_exclusive);
        if (close_position == start_inclusive - 1)
            return -1;
        return close_position;
    }
    // note (umqra, 30.09.18): try_find_close doesn't implemented because it's relatively hard to do
};

#endif //SDSL_NEAR_MATCH_FINDER_HPP_INCLUDED
