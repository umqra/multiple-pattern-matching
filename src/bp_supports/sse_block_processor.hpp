#ifndef SSE_BLOCK_PROCESSOR_HPP_INCLUDED
#define SSE_BLOCK_PROCESSOR_HPP_INCLUDED

#include <sdsl/int_vector.hpp>
#include <x86intrin.h>

class SseBlockProcessor
{
private:
    static const size_t saved_block_length = 12;
    int8_t min_balance[1 << saved_block_length]; // todo: move to the static member

    int64_t process_block_naive(uint64_t block, size_t block_size, size_t &balance)
    {
        for (size_t i = 0; i < block_size; i++)
        {
            balance -= 2 * (block & 1) - 1;
            if (balance == 0)
                return i;
            block >>= 1;
        }
        return -1;
    }
public:
    explicit SseBlockProcessor()
    {
        for (size_t mask = 0; mask < (1 << saved_block_length); mask++)
        {
            int8_t balance = 0;
            min_balance[mask] = 1;
            for (size_t i = 0; i < saved_block_length; i++)
            {
                if (mask & (1 << i))
                    balance--;
                else
                    balance++;
                min_balance[mask] = std::min(min_balance[mask], balance);
            }
            min_balance[mask] *= -2;
        }
    }
    int64_t try_find_close(uint64_t block, size_t block_size, size_t &balance)
    {
        block = ~block;
        size_t start_balance = balance;
        const uint64_t even_bit_mask = 0xAAAAAAAAAAAAAAAA;

        uint64_t current_even_bit_mask = even_bit_mask & (((uint64_t)1 << block_size) - 1);
        auto type_change = block ^ (block << 1);
        auto unchanged_type = (~type_change) & current_even_bit_mask;
        auto compressed_block = _pext_u64(block, unchanged_type);
        auto compressed_block_size = sdsl::bits::cnt(unchanged_type);
        for (size_t i = 0; i < compressed_block_size; i += saved_block_length)
        {
            auto current_block_size = std::min(saved_block_length, compressed_block_size - i);
            auto current_block = compressed_block & ((1 << current_block_size) - 1);
            auto current_min_balance = min_balance[current_block];
            if ((int8_t)balance <= current_min_balance + 1)
            {
                balance = start_balance;
                return process_block_naive(block, block_size, balance);
            }
            balance -= 2 * (2 * sdsl::bits::cnt(current_block) - current_block_size);
            compressed_block >>= saved_block_length;
        }
        if (compressed_block_size == 0 && balance <= 1)
            return process_block_naive(block, block_size, balance);
        return -1;
    }
};

const size_t SseBlockProcessor::saved_block_length;

#endif //SSE_BLOCK_PROCESSOR_HPP_INCLUDED
