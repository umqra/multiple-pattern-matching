# FindSDSL
# --------
#
# Find sdsl-lite library
#
#   SDSL_INCLUDE_DIRS   - where to find sdsl-lite sources.
#   SDSL_LIBRARIES      - List of libraries when using sdsl-lite.
#   SDSL_FOUND          - True if sdsl-lite found.
#   SDSL_VERSION_STRING - the version of sdsl-lite library found.

cmake_minimum_required(VERSION 3.9)
find_package(PkgConfig QUIET)
if (PkgConfig_FOUND)
	pkg_search_module(SDSL QUIET sdsl-lite)
endif()

if (NOT SDSL_FOUND)
	find_path(SDSL_INCLUDE_DIRS NAMES sdsl/)
	mark_as_advanced(SDSL_INCLUDE_DIRS)

	find_library(SDSL_LIBRARIES NAMES sdsl)
	mark_as_advanced(SDSL_LIBRARIES)
endif()

FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDSL REQUIRED_VARS 
	SDSL_LIBRARIES SDSL_INCLUDE_DIRS)
