#ifndef CODE_UTILS_HPP
#define CODE_UTILS_HPP

#ifdef DEBUG
#define dbg(...) fprintf(stderr, __VA_ARGS__)
#else
#define dbg(...) (void)0
#endif

#endif // CODE_UTILS_HPP
