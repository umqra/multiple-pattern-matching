#ifndef COMPRESSED_MATCHER_HPP
#define COMPRESSED_MATCHER_HPP

#include <cstdlib>
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/io.hpp>
#include <string>
#include <vector>

#include "aho_build_tools.hpp"
#include "belazzougui.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "raw_trie.hpp"

template <typename failure_link_tree_type = dfuds_tree<>,
    typename report_link_tree_type = ultra_dfuds_tree<>,
    typename dict_support_info_type = dict_support_info<>>
class compressed_matcher : public matcher_base {
private:
    template <size_t block_size>
    struct belazzougui_fixed_block {
        using algo_type = belazzougui<
            transitions_succinct_map<compression_boosting_bitvector<block_size>>,
            failure_link_tree_type,
            report_link_tree_type,
            dict_support_info_type>;
    };

    typename belazzougui_fixed_block<1024>::algo_type alg1024;
    typename belazzougui_fixed_block<2048>::algo_type alg2048;
    typename belazzougui_fixed_block<4096>::algo_type alg4096;
    typename belazzougui_fixed_block<8192>::algo_type alg8192;
    typename belazzougui_fixed_block<16384>::algo_type alg16384;
    typename belazzougui_fixed_block<32768>::algo_type alg32768;
    typename belazzougui_fixed_block<65536>::algo_type alg65536;
    typename belazzougui_fixed_block<131072>::algo_type alg131072;
    typename belazzougui_fixed_block<262144>::algo_type alg262144;
    typename belazzougui_fixed_block<524288>::algo_type alg524288;
    typename belazzougui_fixed_block<1048576>::algo_type alg1048576;
    typename belazzougui_fixed_block<4194304>::algo_type alg4194304;
    typename belazzougui_fixed_block<16777216>::algo_type alg16777216;
    static const size_t internal_load_facor = 80;
    static const size_t threshold_alg1024 = 1024 / internal_load_facor;
    static const size_t threshold_alg2048 = 2048 / internal_load_facor;
    static const size_t threshold_alg4096 = 4096 / internal_load_facor;
    static const size_t threshold_alg8192 = 8192 / internal_load_facor;
    static const size_t threshold_alg16384 = 16384 / internal_load_facor;
    static const size_t threshold_alg32768 = 32768 / internal_load_facor;
    static const size_t threshold_alg65536 = 65536 / internal_load_facor;
    static const size_t threshold_alg131072 = 131072 / internal_load_facor;
    static const size_t threshold_alg262144 = 262144 / internal_load_facor;
    static const size_t threshold_alg524288 = 524288 / internal_load_facor;
    static const size_t threshold_alg1048576 = 1048576 / internal_load_facor;
    static const size_t threshold_alg4194304 = 4194304 / internal_load_facor;
    static const size_t threshold_alg16777216 = 16777216 / internal_load_facor;

    size_t effective_sigma;

public:
    compressed_matcher() = default;
    compressed_matcher(const compressed_matcher&) = default;
    compressed_matcher(compressed_matcher&&) = default;
    compressed_matcher& operator=(const compressed_matcher&) = default;
    compressed_matcher& operator=(compressed_matcher&&) = default;

    explicit compressed_matcher(const dictionary& dict)
    {
        auto trie = raw_trie(dict);
        effective_sigma = trie.effective_alphabet_size();
        size_t threshold_param = effective_sigma;
        if (threshold_param < threshold_alg1024)
            alg1024 = typename belazzougui_fixed_block<1024>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg2048)
            alg2048 = typename belazzougui_fixed_block<2048>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg4096)
            alg4096 = typename belazzougui_fixed_block<4096>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg8192)
            alg8192 = typename belazzougui_fixed_block<8192>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg16384)
            alg16384 = typename belazzougui_fixed_block<16384>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg32768)
            alg32768 = typename belazzougui_fixed_block<32768>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg65536)
            alg65536 = typename belazzougui_fixed_block<65536>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg131072)
            alg131072 =
                typename belazzougui_fixed_block<131072>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg262144)
            alg262144 =
                typename belazzougui_fixed_block<262144>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg524288)
            alg524288 =
                typename belazzougui_fixed_block<524288>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg1048576)
            alg1048576 =
                typename belazzougui_fixed_block<1048576>::algo_type(trie, dict);
        else if (threshold_param < threshold_alg4194304)
            alg4194304 =
                typename belazzougui_fixed_block<4194304>::algo_type(trie, dict);
        else
            alg16777216 =
                typename belazzougui_fixed_block<16777216>::algo_type(trie, dict);
    }

    // TODO: return iterator to consume less memory?
    std::vector<occurence> enumerate_occurence_positions(const string_seq& text) const override
    {
        size_t threshold_param = effective_sigma;
        if (threshold_param < threshold_alg1024)
            return enumerate_occurence_positions(alg1024, text);
        else if (threshold_param < threshold_alg2048)
            return enumerate_occurence_positions(alg2048, text);
        else if (threshold_param < threshold_alg4096)
            return enumerate_occurence_positions(alg4096, text);
        else if (threshold_param < threshold_alg8192)
            return enumerate_occurence_positions(alg8192, text);
        else if (threshold_param < threshold_alg16384)
            return enumerate_occurence_positions(alg16384, text);
        else if (threshold_param < threshold_alg32768)
            return enumerate_occurence_positions(alg32768, text);
        else if (threshold_param < threshold_alg65536)
            return enumerate_occurence_positions(alg65536, text);
        else if (threshold_param < threshold_alg131072)
            return enumerate_occurence_positions(alg131072, text);
        else if (threshold_param < threshold_alg262144)
            return enumerate_occurence_positions(alg262144, text);
        else if (threshold_param < threshold_alg524288)
            return enumerate_occurence_positions(alg524288, text);
        else if (threshold_param < threshold_alg1048576)
            return enumerate_occurence_positions(alg1048576, text);
        else if (threshold_param < threshold_alg4194304)
            return enumerate_occurence_positions(alg4194304, text);
        else
            return enumerate_occurence_positions(alg16777216, text);
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const override
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::write_member(effective_sigma, out, v, "effective_sigma");
        written_bytes += alg1024.serialize(out, child, "alg1024");
        written_bytes += alg2048.serialize(out, child, "alg2048");
        written_bytes += alg4096.serialize(out, child, "alg4096");
        written_bytes += alg8192.serialize(out, child, "alg8192");
        written_bytes += alg16384.serialize(out, child, "alg16384");
        written_bytes += alg32768.serialize(out, child, "alg32768");
        written_bytes += alg65536.serialize(out, child, "alg64536");
        written_bytes += alg131072.serialize(out, child, "alg131072");
        written_bytes += alg262144.serialize(out, child, "alg262144");
        written_bytes += alg524288.serialize(out, child, "alg524288");
        written_bytes += alg1048576.serialize(out, child, "alg1048576");
        written_bytes += alg4194304.serialize(out, child, "alg4194304");
        written_bytes += alg16777216.serialize(out, child, "alg16777216");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in) override
    {
        sdsl::read_member(effective_sigma, in);
        alg1024.load(in);
        alg2048.load(in);
        alg4096.load(in);
        alg8192.load(in);
        alg16384.load(in);
        alg32768.load(in);
        alg65536.load(in);
        alg131072.load(in);
        alg262144.load(in);
        alg524288.load(in);
        alg1048576.load(in);
        alg4194304.load(in);
        alg16777216.load(in);
    }

private:
    template <typename alg_type>
    std::vector<occurence> enumerate_occurence_positions(const alg_type& algorithm, const string_seq& text) const
    {
        std::vector<occurence> occurences = {};
        size_t current_vertex = 0;
        for (size_t position = 0; position < text.length(); position++) {
            current_vertex = algorithm.get_next_transition(current_vertex, text, position);
            add_occurence_positions(algorithm, position, current_vertex, occurences);
        }
        return occurences;
    }

    size_t root() const { return 0; }

    template <typename alg_type>
    void add_occurence_positions(const alg_type& algorithm, size_t position, size_t vertex, std::vector<occurence>& occurences) const
    {
        if (algorithm.is_terminal_vertex(vertex))
            occurences.push_back(occurence(
                position - algorithm.get_word_length(vertex) + 1, position));
        vertex = algorithm.get_report_link(vertex);
        while (vertex != root()) {
            occurences.push_back(occurence(
                position - algorithm.get_word_length(vertex) + 1, position));
            vertex = algorithm.get_report_link(vertex);
        }
    }
};

#endif // COMPRESSED_MATCHER_HPP
