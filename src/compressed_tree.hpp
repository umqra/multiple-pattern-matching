#ifndef COMPRESSED_TREE_HPP
#define COMPRESSED_TREE_HPP

#include "tree_utils.hpp"
#include "ultra_dfuds_tree.hpp"

#include <memory>
#include <sdsl/bit_vectors.hpp>
#include <vector>

template <typename bit_vector_type = class sdsl::sd_vector<>,
    typename tree_type = class ultra_dfuds_tree<>>
class compressed_tree {
private:
    bit_vector_type marked_vertices;
    tree_type tree;

public:
    using size_type = size_t;

    compressed_tree() = default;
    compressed_tree(const compressed_tree&) = default;
    compressed_tree(compressed_tree&&) = default;
    compressed_tree& operator=(const compressed_tree&) = default;
    compressed_tree& operator=(compressed_tree&&) = default;

    compressed_tree(const std::vector<size_t>& dfs_degrees,
        const bit_vector_type& _marked_vertices)
    {
        marked_vertices = _marked_vertices;

        auto marked_parents = get_marked_parents(dfs_degrees, marked_vertices);
        auto compressed_dfs = get_compressed_dfs(dfs_degrees, marked_parents);
        tree = tree_type(compressed_dfs);
    }

    bool is_marked(size_t vertex) const { return marked_vertices[vertex] == 1; }

    // TODO: public interface unification for tree classes?
    bool try_get_parent(size_t vertex, size_t& parent) const
    {
        if (!is_marked(vertex))
            return false;
        parent = tree.parent(vertex);
        return true;
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += marked_vertices.serialize(out, child, "marked_vertices");
        written_bytes += tree.serialize(out, child, "tree");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        marked_vertices.load(in);
        tree.load(in);
    }

private:
    std::vector<size_t> get_compressed_dfs(const std::vector<size_t>& dfs_degrees, const bit_vector_type& marked_parents)
    {
        auto compressed_dfs_degrees = std::vector<size_t>(dfs_degrees.size(), 0);
        tree_iterator iterator = tree_iterator(dfs_degrees);
        while (iterator.move()) {
            if (iterator.has_info()) {
                size_t parent_id = iterator.info();
                compressed_dfs_degrees[parent_id]++;
            }
            if (marked_parents[iterator.vertex()]) {
                iterator.push_info(iterator.vertex());
            }
        }
        return compressed_dfs_degrees;
    }
    bit_vector_type get_marked_parents(const std::vector<size_t>& dfs_degrees, const bit_vector_type& marked_vertices)
    {
        sdsl::bit_vector marked_parents = sdsl::bit_vector(dfs_degrees.size());
        marked_parents[0] = 1;

        tree_iterator iterator = tree_iterator(dfs_degrees);
        while (iterator.move()) {
            if (marked_vertices[iterator.vertex()] && iterator.has_parent()) {
                marked_parents[iterator.parent()] = 1;
            }
        }
        return bit_vector_type(marked_parents);
    }
};

#endif // COMPRESSED_TREE_HPP
