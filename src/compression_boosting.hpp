#ifndef COMPRESSION_BOOSTING_HPP
#define COMPRESSION_BOOSTING_HPP

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

#include <sdsl/io.hpp>
#include <sdsl/structure_tree.hpp>
#include <sdsl/util.hpp>

#include "elias-fano.hpp"

template <uint32_t block_size>
class compression_boosting_bitvector {
    static_assert((block_size & (block_size - 1)) == 0 && block_size >= 512 && block_size <= (1u << 30),
        "the block size must be a power of two that is at least 512 = 2**9 bits but at most 2**30");

    struct block_descriptor {
        uint32_t prefix_sum; // TODO: maybe uint40_t?
        uint32_t bitvector_offset; // uint32_t is enough because the offset is in 4 byte steps

        using size_type = size_t; // required for serialize

        block_descriptor()
        {
            prefix_sum = 0;
            bitvector_offset = 0;
        } // defined to make is_pod<>::value=false

        size_type serialize(std::ostream& out,
            sdsl::structure_tree_node* v = nullptr,
            std::string name = "") const
        {
            out.write(reinterpret_cast<const char*>(&prefix_sum), sizeof(prefix_sum));
            out.write(reinterpret_cast<const char*>(&bitvector_offset), sizeof(bitvector_offset));
            return sizeof(prefix_sum) + sizeof(bitvector_offset);
        }
        void load(std::istream& in)
        {
            in.read(reinterpret_cast<char*>(&prefix_sum), sizeof(prefix_sum));
            in.read(reinterpret_cast<char*>(&bitvector_offset), sizeof(bitvector_offset));
        }
    };

    typedef elias_fano_bitvector<block_size> block_bitvector_type;
    static const uint32_t k_block_size = block_size;

    std::vector<block_descriptor> blocks;
    std::vector<uint32_t> blocks_buffer;
    size_t total_size;

    static inline uint32_t get_in_block_id(size_t index)
    {
        return static_cast<uint32_t>(
            index % k_block_size); // must fit in uint32_t (see asserts for block_size)
    }
    static inline size_t get_block_id(size_t index)
    {
        return index / k_block_size;
    }

public:
    using size_type = size_t; // accessed by SDSL in the serialization of vectors
        // of decltype(*this)

    compression_boosting_bitvector() = default;
    compression_boosting_bitvector(const compression_boosting_bitvector&) = default;
    compression_boosting_bitvector(compression_boosting_bitvector&&) = default;
    compression_boosting_bitvector& operator=(const compression_boosting_bitvector&) = default;
    compression_boosting_bitvector& operator=(compression_boosting_bitvector&&) = default;

    compression_boosting_bitvector(const std::vector<size_t>& elements, size_t size)
        : compression_boosting_bitvector(elements.begin(), elements.end(), size)
    {
    }

    template <typename iter_type>
    compression_boosting_bitvector(iter_type begin, iter_type end, size_t size)
    {
        static_assert(std::is_convertible<typename std::iterator_traits<iter_type>::value_type, size_t>::value, "");
        static_assert(std::is_unsigned<typename std::iterator_traits<iter_type>::value_type>::value, "");
        assert((std::is_sorted(begin, end) && (begin == end || *std::prev(end) < size)));

        if (size == 0)
            throw std::logic_error("size of the compressiom boosting bitvector cannot be 0");
        total_size = size;
        blocks.resize((size + k_block_size - 1) / k_block_size, {});

        auto align4 = [](size_t x) { return (x + 3) / 4 * 4; };
        size_t buffer_size = 8; // we need access to 8 bytes at the end of the last bitvector in the buffer
        std::vector<uint32_t> blk_ones;
        for (auto curr = begin; curr != end; ++curr) {
            auto id = get_block_id(*curr);
            blocks[id].prefix_sum++; // the number of ones in block
            blk_ones.push_back(get_in_block_id(*curr));
            if (curr + 1 == end || get_block_id(*(curr + 1)) != id) {
                buffer_size += align4(block_bitvector_type::size_in_bytes(
                    blk_ones.begin(), blk_ones.end()));
                blk_ones.clear();
            }
        }

        blocks_buffer.resize(buffer_size / 4);
        uint32_t mem_index = 0; // reinterpret_cast<uint8_t*>(blocks_buffer.data());
        for (auto curr = begin; curr != end;) {
            size_t id = get_block_id(*curr);
            uint32_t ones_num = static_cast<uint32_t>(blocks[id].prefix_sum); // not prefix sum yet
            if (ones_num != 0 && ones_num != k_block_size) { // actually ones_num == 0 is impossible
                blk_ones.resize(ones_num);
                std::transform(curr, curr + ones_num, blk_ones.begin(),
                    [](size_t x) { return get_in_block_id(x); });
                size_t used_bytes;
                new (&blocks_buffer[mem_index])
                    block_bitvector_type(blk_ones.begin(), blk_ones.end(), used_bytes);
                blocks[id].bitvector_offset = mem_index;
                assert((uint64_t)mem_index + align4(used_bytes) / 4 < (1ull << 32));
                mem_index += align4(used_bytes) / 4;
            }
            curr += ones_num; // ones_num > 0
        }

        for (size_t i = 1; i < blocks.size(); ++i) {
            assert(static_cast<uint64_t>(blocks[i].prefix_sum) + blocks[i - 1].prefix_sum <= std::numeric_limits<decltype(blocks[i].prefix_sum)>::max());
            blocks[i].prefix_sum += blocks[i - 1].prefix_sum;
        }
    }

    size_t size() const { return total_size; }

    size_t operator[](size_t i) const
    {
        assert(i < total_size);
        size_t id = get_block_id(i);
        size_t pref_sum = id ? blocks[id - 1].prefix_sum : 0;
        uint32_t ones_num = static_cast<uint32_t>(blocks[id].prefix_sum - pref_sum);
        if (ones_num == 0)
            return 0;
        if (ones_num == k_block_size)
            return 1;
        uint8_t ibit;
        auto bv = reinterpret_cast<const block_bitvector_type*>(
            &blocks_buffer[blocks[id].bitvector_offset]);
        bv->rank1(get_in_block_id(i), ones_num, ibit);
        return ibit;
    }

    /// Return the number of ones in the positions 0,1,...,i-1
    size_t rank1(size_t i) const { return rank1(i, uint8_t()); }

    /// Return the number of ones in the positions 0,1,...,i-1 and the bit at
    /// position i
    size_t rank1(size_t i, uint8_t& ibit) const
    {
        assert(i < total_size);
        size_t id = get_block_id(i);
        size_t pref_sum = id ? blocks[id - 1].prefix_sum : 0;
        uint32_t ones_num = static_cast<uint32_t>(blocks[id].prefix_sum - pref_sum);
        if (ones_num == 0) {
            ibit = 0;
            return pref_sum;
        } else if (ones_num == k_block_size) {
            ibit = 1;
            return pref_sum + get_in_block_id(i);
        }
        auto bv = reinterpret_cast<const block_bitvector_type*>(
            &blocks_buffer[blocks[id].bitvector_offset]);
        return pref_sum + bv->rank1(get_in_block_id(i), ones_num, ibit);
    }

    /// Return the position (0,1,...,size()-1) of the i-th one (i=1,2,...,count())
    size_t select1(size_t i) const
    {
        assert(1 <= i);
        size_t lb = 0, rb = blocks.size();
        while (lb < rb) {
            size_t mid = lb + (rb - lb) / 2;
            if (blocks[mid].prefix_sum >= i)
                rb = mid;
            else
                lb = mid + 1;
        }
        size_t ones_num = blocks[lb].prefix_sum;
        if (lb) {
            size_t pref_sum = blocks[lb - 1].prefix_sum;
            i -= pref_sum;
            ones_num -= pref_sum;
        }
        if (ones_num == k_block_size) // note that ones_num cannot be equal to 0
            return lb * k_block_size + i - 1;
        auto bv = reinterpret_cast<const block_bitvector_type*>(
            &blocks_buffer[blocks[lb].bitvector_offset]);
        return lb * k_block_size + bv->select1(static_cast<uint32_t>(i), static_cast<uint32_t>(ones_num));
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr,
        std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::write_member(total_size, out, child, "total_size");
        written_bytes += sdsl::serialize(blocks, out, child, "blocks");
        written_bytes += sdsl::serialize(blocks_buffer, out, child, "blocks_buffer");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    void load(std::istream& in)
    {
        sdsl::read_member(total_size, in);
        sdsl::load(blocks, in);
        sdsl::load(blocks_buffer, in);
    }
};

#endif // COMPRESSION_BOOSTING_HPP
