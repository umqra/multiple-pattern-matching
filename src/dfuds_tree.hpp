#ifndef DFUDS_TREE_INCLUDED
#define DFUDS_TREE_INCLUDED

#include "tree_utils.hpp"
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/bp_support.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/util.hpp>
#include <vector>
#include <bp_supports/bp_support_sada_light.hpp>

template <class t_bit_vector = sdsl::bit_vector,
    class t_select_0_support = typename t_bit_vector::select_0_type,
    class t_rank_0_support = typename t_bit_vector::rank_0_type,
    class t_bp_support = bp_support_sada_light<>>
class dfuds_tree {
private:
    t_bit_vector dfuds_sequence;
    t_select_0_support closing_brackets_select;
    t_rank_0_support closing_brackets_rank;
    t_bp_support dfuds_bp_support;
    size_t tree_size;
    const size_t root = 0;

public:
    using size_type = size_t;

    const t_bp_support& dfuds_bp_support_ref = dfuds_bp_support; // TODO: fragile construction

    dfuds_tree() = default;
    dfuds_tree(const dfuds_tree& other) { copy(other); }
    dfuds_tree(dfuds_tree&& other) { swap(other); }
    dfuds_tree& operator=(const dfuds_tree& other)
    {
        if (this != &other)
            copy(other);
        return *this;
    }
    dfuds_tree& operator=(dfuds_tree&& other)
    {
        swap(other);
        return *this;
    }

    // TODO: use of sdsl::int_vector<> more appropriate here, but may be it must be implemented via iterators
    dfuds_tree(const std::vector<size_t>& dfs_order_vertex_degree)
    {
        dfuds_sequence = sdsl::bit_vector(2 * dfs_order_vertex_degree.size(), 0);
        tree_size = dfs_order_vertex_degree.size();

        auto iterator = tree_iterator(dfs_order_vertex_degree);
        dfuds_sequence[0] = 1;
        size_t dfuds_ptr = 1, full_block_size = 64;
        while (iterator.move()) {
            dfuds_ptr++;
            for (size_t child = 0; child < iterator.degree();
                 child += full_block_size) {
                size_t block_size = std::min(iterator.degree() - child, full_block_size);
                dfuds_sequence.set_int(dfuds_ptr + child,
                    sdsl::bits::lo_set[block_size], block_size);
            }
            dfuds_ptr += iterator.degree();
        }

        sdsl::util::init_support(closing_brackets_select, &dfuds_sequence);
        sdsl::util::init_support(closing_brackets_rank, &dfuds_sequence);
        sdsl::util::init_support(dfuds_bp_support, &dfuds_sequence);
    }

    size_t size() const { return tree_size; }

    size_t parent(size_t vertex) const
    {
        assert(0 <= vertex && vertex < size());

        if (vertex == root)
            return root;
        size_t vertex_position = closing_brackets_select(vertex + 1);
        size_t open_position = dfuds_bp_support.find_open(vertex_position);
        return closing_brackets_rank(open_position) - 1;
    }

    void swap(dfuds_tree& other)
    {
        if (this != &other) {
            dfuds_sequence.swap(other.dfuds_sequence);
            closing_brackets_select.swap(other.closing_brackets_select);
            closing_brackets_select.set_vector(&dfuds_sequence);
            closing_brackets_rank.swap(other.closing_brackets_rank);
            closing_brackets_rank.set_vector(&dfuds_sequence);
            dfuds_bp_support.swap(other.dfuds_bp_support);
            dfuds_bp_support.set_vector(&dfuds_sequence);
            std::swap(tree_size, other.tree_size);
        }
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::write_member(tree_size, out, child, "tree_size");
        written_bytes += dfuds_sequence.serialize(out, child, "dfuds_sequence");
        written_bytes += closing_brackets_select.serialize(
            out, child, "closing_brackets_select");
        written_bytes += closing_brackets_rank.serialize(out, child, "closing_brackets_rank");
        written_bytes += dfuds_bp_support.serialize(out, child, "dfuds_bp_support");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        sdsl::read_member(tree_size, in);
        dfuds_sequence.load(in);
        closing_brackets_select.load(in, &dfuds_sequence);
        closing_brackets_rank.load(in, &dfuds_sequence);
        dfuds_bp_support.load(in, &dfuds_sequence);
    }

private:
    void copy(const dfuds_tree& other)
    {
        dfuds_sequence = other.dfuds_sequence;
        closing_brackets_select = other.closing_brackets_select;
        closing_brackets_select.set_vector(&dfuds_sequence);
        closing_brackets_rank = other.closing_brackets_rank;
        closing_brackets_rank.set_vector(&dfuds_sequence);
        dfuds_bp_support = other.dfuds_bp_support;
        dfuds_bp_support.set_vector(&dfuds_sequence);
        tree_size = other.tree_size;
    }
};

#endif // DFUDS_TREE_INCLUDED
