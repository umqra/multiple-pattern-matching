#ifndef DICT_SUPPORT_INFO_HPP
#define DICT_SUPPORT_INFO_HPP

#include "aho_build_tools.hpp"
#include "dictionary.hpp"
#include "trie_path_array.hpp"
#include <sdsl/bit_vectors.hpp>

template <typename terminal_mask_type = sdsl::sd_vector<>,
    typename lengths_dictionary_type = sdsl::sd_vector<>>
class dict_support_info {
private:
    using terminal_mask_rank_1_type = typename terminal_mask_type::rank_1_type;
    using terminal_mask_select_1_type = typename terminal_mask_type::select_1_type;
    using lengths_dictionary_select_1_type = typename lengths_dictionary_type::select_1_type;

    size_t count_terminal_vertices;
    terminal_mask_type terminal_vertex;
    terminal_mask_rank_1_type terminal_vertex_rank;
    terminal_mask_select_1_type terminal_vertex_select;

    lengths_dictionary_type terminal_lengths;
    lengths_dictionary_select_1_type terminal_lengths_select;

public:
    using size_type = size_t;

    dict_support_info() = default;
    dict_support_info(const dict_support_info& other) { copy(other); }
    dict_support_info(dict_support_info&& other) { swap(other); }
    dict_support_info& operator=(dict_support_info other)
    {
        swap(other);
        return *this;
    }

    // TODO: think about dependencies for this constructor
    dict_support_info(const dictionary& dict, const trie_path_array& trie_array)
    {
        auto terminal_vertices_list = terminal_vertices_calculator(dict).terminal_vertices_list; // TODO: better name
        count_terminal_vertices = terminal_vertices_list.size();

        for (size_t id = 0; id < terminal_vertices_list.size(); id++)
            terminal_vertices_list[id].first = trie_array.vertex_order(terminal_vertices_list[id].first);
        std::sort(terminal_vertices_list.begin(), terminal_vertices_list.end());

        auto terminal_vertices = std::vector<size_t>(terminal_vertices_list.size());
        auto terminal_lengths_sum = std::vector<size_t>(terminal_vertices_list.size());
        for (size_t id = 0; id < terminal_vertices_list.size(); id++) {
            terminal_vertices[id] = terminal_vertices_list[id].first;
            terminal_lengths_sum[id] = terminal_vertices_list[id].second + (id == 0 ? 0 : terminal_lengths_sum[id - 1]);
        }
        terminal_vertex = terminal_mask_type(terminal_vertices.begin(), terminal_vertices.end());
        sdsl::util::init_support(terminal_vertex_rank, &terminal_vertex);
        sdsl::util::init_support(terminal_vertex_select, &terminal_vertex);

        terminal_lengths = lengths_dictionary_type(terminal_lengths_sum.begin(), terminal_lengths_sum.end());
        sdsl::util::init_support(terminal_lengths_select, &terminal_lengths);
    }

    bool is_terminal_vertex(size_t vertex) const
    {
        if (vertex >= terminal_vertex.size())
            return false;
        return terminal_vertex[vertex];
    }

    size_t get_word_length(size_t vertex) const
    {
        size_t word_id = terminal_vertex_rank(vertex) + 1;
        return terminal_lengths_select(word_id) - (word_id == 1 ? 0 : terminal_lengths_select(word_id - 1));
    }

    std::vector<size_t> get_vertices_list() const
    {
        auto vertices_list = std::vector<size_t>(count_terminal_vertices);
        for (size_t i = 1; i <= count_terminal_vertices; i++) {
            vertices_list[i - 1] = terminal_vertex_select(i);
        }
        return vertices_list;
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::write_member(count_terminal_vertices, out, child, "count_terminal_vertices");
        written_bytes += terminal_vertex.serialize(out, child, "terminal_vertex");
        written_bytes += terminal_vertex_rank.serialize(out, child, "terminal_vertex_rank");
        written_bytes += terminal_vertex_select.serialize(out, child, "terminal_vertex_select");
        written_bytes += terminal_lengths.serialize(out, child, "terminal_lengths");
        written_bytes += terminal_lengths_select.serialize(out, child, "terminal_lengths_select");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        sdsl::read_member(count_terminal_vertices, in);
        terminal_vertex.load(in);
        terminal_vertex_rank.load(in, &terminal_vertex);
        terminal_vertex_select.load(in, &terminal_vertex);
        terminal_lengths.load(in);
        terminal_lengths_select.load(in, &terminal_lengths);
    }

private:
    void copy(const dict_support_info& m)
    {
        count_terminal_vertices = m.count_terminal_vertices;
        terminal_vertex = m.terminal_vertex;
        terminal_vertex_rank = m.terminal_vertex_rank;
        terminal_vertex_rank.set_vector(&terminal_vertex);
        terminal_vertex_select = m.terminal_vertex_select;
        terminal_vertex_select.set_vector(&terminal_vertex);
        terminal_lengths = m.terminal_lengths;
        terminal_lengths_select = m.terminal_lengths_select;
        terminal_lengths_select.set_vector(&terminal_lengths);
    }
    // TODO: Make swap method public?
    void swap(dict_support_info& m)
    {
        if (this != &m) {
            std::swap(count_terminal_vertices, m.count_terminal_vertices);
            terminal_vertex.swap(m.terminal_vertex);
            terminal_vertex_rank.swap(m.terminal_vertex_rank);
            terminal_vertex_rank.set_vector(&terminal_vertex);
            terminal_vertex_select.swap(m.terminal_vertex_select);
            terminal_vertex_select.set_vector(&terminal_vertex);
            terminal_lengths.swap(m.terminal_lengths);
            terminal_lengths_select.swap(m.terminal_lengths_select);
            terminal_lengths_select.set_vector(&terminal_lengths);
        }
    }
};

#endif // DICT_SUPPORT_INFO_HPP
