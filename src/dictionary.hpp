#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "alphabet_concepts.hpp"
#include "file_utils.hpp"
#include "string_seq.hpp"

class dictionary {
protected:
    std::vector<string_seq> words;

public:
    size_t size() const { return words.size(); }
    const string_seq& operator[](size_t index) const { return words[index]; }
    bool empty() const { return size() == 0; }
    // TODO: calculate entropy?
    size_t size_in_bytes() const
    {
        size_t bytes = 0;
        for (const auto& word : words)
            bytes += word.size_in_bytes();
        return bytes;
    }
};

template <typename alphabet = byte_alphabet>
class unordered_dictionary : public dictionary {
private:
    using string_type = typename alphabet::string_type;

public:
    unordered_dictionary(const std::vector<string_type>& _words)
    {
        words = std::vector<string_seq>(_words.size());
        for (size_t i = 0; i < _words.size(); i++)
            words[i] = string_seq(_words[i]);

        if (!std::is_sorted(words.begin(), words.end()))
            sort(words.begin(), words.end());
        words.resize(std::unique(words.begin(), words.end()) - words.begin());
    }
};

template <typename alphabet = byte_alphabet>
class ordered_dictionary : public dictionary {
private:
    using string_type = typename alphabet::string_type;

public:
    ordered_dictionary(const std::vector<string_type>& _words)
    {
        if (!std::is_sorted(_words.begin(), _words.end()))
            throw std::logic_error("Words must be ordered");

        words = std::vector<string_seq>(_words.size());
        for (size_t i = 0; i < _words.size(); i++)
            words[i] = string_seq(_words[i]);
        words.resize(std::unique(words.begin(), words.end()) - words.begin());
    }
};

template <typename alphabet = byte_alphabet>
class unordered_dictionary_file : public dictionary {
public:
    unordered_dictionary_file(const std::string& filename)
    {
        words = read_seq_lines<alphabet>(filename);
        std::sort(words.begin(), words.end());
        words.resize(std::unique(words.begin(), words.end()) - words.begin());
    }
};

template <typename alphabet = byte_alphabet>
class ordered_dictionary_file : public dictionary {
public:
    ordered_dictionary_file(const std::string& filename)
    {
        words = read_seq_lines<alphabet>(filename);
        if (!std::is_sorted(words.begin(), words.end()))
            throw std::logic_error("Words in file '" + filename + "' must be ordered");
        words.resize(std::unique(words.begin(), words.end()) - words.begin());
    }
};

template <typename alphabet = byte_alphabet>
class hex_dictionary : public dictionary {
public:
    hex_dictionary(const dictionary& dictionary)
    {
        words = std::vector<string_seq>(dictionary.size());
        for (size_t i = 0; i < dictionary.size(); i++) {
            words[i] = convert_from_hex(dictionary[i]);
        }
    }

private:
    size_t get_hex_code(size_t c)
    {
        if ('0' <= c && c <= '9')
            return c - '0';
        return 10 + c - 'a';
    }
    string_seq convert_from_hex(const string_seq& seq)
    {
        assert(seq.length() % 2 == 0);
        auto char_codes = std::vector<size_t>(seq.length() / 2);
        for (size_t i = 0; i < seq.length(); i += 2)
            char_codes[i / 2] = get_hex_code(seq[i]) * 16 + get_hex_code(seq[i + 1]);
        return string_seq(char_codes, alphabet::char_width);
    }
};

class trie_iterator {
private:
    const dictionary& dict;
    std::vector<size_t> v_stack;
    size_t word_pointer, offset_pointer, vertex_id;
    bool first_move;

public:
    trie_iterator(const dictionary& _dict)
        : dict(_dict)
        , word_pointer()
        , offset_pointer()
        , vertex_id()
        , first_move(true)
    {
        v_stack = { vertex_id++ };

        while (word_pointer < dict.size() && dict[word_pointer].length() == 0) {
            word_pointer++;
        }
    }
    size_t from() const { return v_stack.back(); }
    size_t to() const { return vertex_id; }
    size_t character() const { return dict[word_pointer][offset_pointer]; }
    size_t depth() { return v_stack.size(); }
    bool is_terminal() const
    {
        return offset_pointer + 1 == dict[word_pointer].length();
    }
    bool move()
    {
        if (first_move) {
            first_move = false;
            return !dict.empty();
        }
        if (word_pointer + 1 == dict.size() && offset_pointer + 1 == dict[word_pointer].length())
            return false;
        v_stack.push_back(vertex_id++);
        if (offset_pointer + 1 < dict[word_pointer].length()) {
            offset_pointer++;
        } else {
            word_pointer++;
            offset_pointer = 0;
            while (offset_pointer < dict[word_pointer].length() && offset_pointer < dict[word_pointer - 1].length()
                && dict[word_pointer][offset_pointer] == dict[word_pointer - 1][offset_pointer])
                offset_pointer++;

            while (v_stack.size() > offset_pointer + 1)
                v_stack.pop_back();
        }
        return true;
    }
};

#endif // DICTIONARY_HPP
