#ifndef ELIAS_FANO_HPP
#define ELIAS_FANO_HPP

#include "helpers.hpp"
#include <algorithm>
#include <cassert>
#include <iterator>
#include <limits>
#include <sdsl/bits.hpp>
#include <sdsl/util.hpp>
#include <stdint.h>
#include <type_traits>

/// This is a highly specialized Elias-Fano bit vector encoding designed for
/// compression boosting blocks. The size of the vector is constant:
/// vector_size. The data structure does not store the number of ones in the
/// vector; instead, callers give this number and they must guarantee its
/// correcness. The code is based on the SDSL sd_vector and select_support_mcl
/// implementations.

template <uint32_t vector_size>
class elias_fano_bitvector {
    static_assert((vector_size & (vector_size - 1)) == 0 && vector_size <= (1u << 30),
        "vector_size must be a power of two not larger than 2**30 bits");

    uint32_t data[1]; // the only member - "flexible" array (UB in C++ but it's a popular hack)

    static const uint32_t k_vector_size = vector_size;
    static constexpr uint8_t k_vector_size_pow = static_cast<uint8_t>(int_log2(vector_size));
#ifdef _DEBUG // to actually test large samples we lower their size and the size of "small words" (with k_sw)
    static const uint32_t k_linear_search_threshold = 64; // rank1 uses bin_search on low vector ranges longer than this
    static const uint32_t k_raw_data_threshold = 2; // hi bitvector of <= this length is stored in the raw form
    static const uint32_t k_large_samples_step = 32;
    static const uint32_t k_small_samples_step = 8;
    static const int k_sw = 2; // regulates word size to address large sampled blocks
#else
    static const uint32_t k_linear_search_threshold = 64; // rank1 uses bin_search on low vector ranges longer than this
    static const uint32_t k_raw_data_threshold = 256; // hi bitvector of <= this length is stored in the raw form
    static const uint32_t k_large_samples_step = 4096;
    static const uint32_t k_small_samples_step = 64;
    static const int k_sw = 4; // regulates word size to address large sampled blocks
#endif

    static_assert(k_large_samples_step >= k_small_samples_step && k_large_samples_step % k_small_samples_step == 0, "");
    static_assert(k_large_samples_step <= (1ull << 4 * (int_log2(int_log2(k_large_samples_step - 1)) + 1)),
        "hi_bitvector with 2m < k_large_samples_step bits must be addressible with 4*ceil(log2(log2(2m-1)))-bit words");
    struct data_offsets_and_info {
        // offsets in the increasing order (some might be equal if the corresponding field is empty):
        uint32_t large_samples_0, large_samples_1;
        uint32_t small_samples_0, small_samples_1;
        uint32_t raw_bits; // offfset to the raw 2*ones_num bits of the high bitvector
        uint32_t low_vector; // offset to the low vector (ones_num elements, each occupying low_bit bits)
        uint32_t extra_data; // offset to the data located after the low vector data:
        uint32_t large_samples_num, small_samples_num;
        uint32_t ones_num;
        uint32_t last_pos; // = 2*ones_num - 1; the last position of the high bitvector
        uint8_t low_bits, word_size, small_word_size;
    };

public:
    template <typename iter_type>
    elias_fano_bitvector(iter_type begin, iter_type end)
    {
        size_t tmp;
        initialize(begin, end, tmp);
    }

    template <typename iter_type>
    elias_fano_bitvector(iter_type begin, iter_type end, size_t& used_bytes)
    {
        initialize(begin, end, used_bytes);
    }

    /// Total size in bytes occupied by the whole Elias-Fano encoding with [begin..end) set bits.
    template <typename iter_type>
    static size_t size_in_bytes(iter_type begin, iter_type end)
    {
        static_assert(std::is_convertible<typename std::iterator_traits<iter_type>::value_type, uint32_t>::value, "");
        static_assert(std::is_unsigned<typename std::iterator_traits<iter_type>::value_type>::value, "");
        assert((std::is_sorted(begin, end) && std::distance(begin, end) <= k_vector_size));
        assert(begin == end || *std::prev(end) < k_vector_size);

        uint32_t ones_num = std::distance(begin, end);
        if (ones_num == 0 || ones_num == k_vector_size) // special cases
            return 0;

        data_offsets_and_info inf = compute_offsets_and_info(ones_num);
        uint32_t extra_size = 0;
        uint32_t last_smp_zero = 0, last_smp_one = 0;
        uint32_t bit_idx = 0, ones_count = 0, zeros_count = 0;
        for (auto curr = begin; true; ++curr) {
            uint32_t hi_idx = (curr != end) ? (*curr >> inf.low_bits) : (inf.ones_num - 1);
            while (hi_idx >= zeros_count) {
                if (zeros_count % k_large_samples_step == 0 && inf.large_samples_num > 0)
                    last_smp_zero = bit_idx;
                if (bit_idx - last_smp_zero >= (1u << inf.small_word_size))
                    if ((zeros_count + 1) % k_large_samples_step == 0 || zeros_count == inf.ones_num - 1)
                        extra_size += (zeros_count % k_large_samples_step + 1) * inf.word_size;
                zeros_count++;
                bit_idx++;
            }
            if (curr == end)
                break;
            if (ones_count % k_large_samples_step == 0 && inf.large_samples_num > 0)
                last_smp_one = bit_idx;
            if (bit_idx - last_smp_one >= (1u << inf.small_word_size))
                if ((ones_count + 1) % k_large_samples_step == 0 || ones_count == inf.ones_num - 1)
                    extra_size += (ones_count % k_large_samples_step + 1) * inf.word_size;
            ones_count++;
            bit_idx++;
        }
        return static_cast<size_t>((inf.extra_data + extra_size + 7) / 8);
    }

    /// Return the number of ones in the positions 0,1,...,i-1 and the bit at
    /// position i (ibit). The caller must guarantee that ones_num is equal to the
    /// number of ones in the bit vector encoded in data[] and 0 < ones_num <
    /// k_vector_size. This lack of universality is due to optimizations.
    uint32_t rank1(uint32_t i, uint32_t ones_num, uint8_t& ibit) const
    {
        assert(i < k_vector_size);
        // the cases ones_num == 0 and ones_num == k_vector_size must be processed outside
        assert(ones_num > 0 && ones_num < k_vector_size);

        uint8_t low_bits = k_vector_size_pow - sdsl::bits::hi(ones_num); // 0 < low_bits <= 30 (see asserts)
        uint32_t hi_idx = (i >> low_bits) + 1;
        uint32_t last_pos = 2 * ones_num - 1;
        uint32_t lb, rb;
        if (hi_idx == ones_num) {
            lb = (last_pos > 1) ? select0_high(hi_idx, last_pos) - hi_idx + 1 : 0;
            rb = ones_num;
        } else {
            auto range = select0_high_pair(hi_idx, last_pos);
            lb = range.first - hi_idx + 1;
            rb = range.second - hi_idx;
        }
        uint32_t lo_idx = i & ((1u << low_bits) - 1);
        uint32_t lv_offset = low_vector_offset(last_pos);
        uint8_t ibit_result = 0;
        if (rb - lb <= k_linear_search_threshold) { // the threshold is adjusted experimentally
            uint32_t end = lv_offset + rb * low_bits;
            for (lv_offset += lb * low_bits; lv_offset < end;
                 lv_offset += low_bits, ++lb) {
                uint32_t read = read_data_int32(lv_offset, low_bits);
                if (read >= lo_idx) {
                    ibit_result = (read == lo_idx);
                    break;
                }
            }
        } else {
            while (lb < rb) { // binary search in [lb..rb) in std::lower_bound fasion
                uint32_t mid = (lb + rb) / 2u; // no overflow in lb+rb since lb < rb <= ones_num < 2**30
                uint32_t read = read_data_int32(lv_offset + mid * low_bits, low_bits);
                if (read >= lo_idx) {
                    ibit_result = (read == lo_idx);
                    rb = mid;
                } else {
                    lb = mid + 1;
                }
            } // in the end low_vector[lb] is the range smallest value that is not less than lo_idx
        }
        ibit = ibit_result;
        return lb;
    }

    /// Returns the position (from 0 to k_vector_size - 1) of the i-th one in the
    /// bit vector (i=1,2...). It must be guaranteed by the caller that ones_num
    /// equals the number of ones in the bit vector and 0 < ones_num <
    /// k_vector_size. This lack of universality is due to optimizations.
    inline uint32_t select1(uint32_t i, uint32_t ones_num) const
    {
        assert(i > 0 && i <= ones_num);
        // the cases ones_num == 0 and ones_num == k_vector_size must be processed outside
        assert(ones_num > 0 && ones_num < k_vector_size);

        uint8_t low_bits = k_vector_size_pow - sdsl::bits::hi(ones_num); // 0 < low_bits <= 30 (see asserts)
        uint32_t high_bv_last_pos = 2 * ones_num - 1;
        uint32_t hi = high_bv_last_pos > 1 ? select1_high(i, high_bv_last_pos) - i : 0; // note special case
        return (hi << low_bits) + read_data_int32(low_vector_offset(high_bv_last_pos) + (i - 1) * low_bits, low_bits);
    }

private:
    template <typename iter_type>
    void initialize(iter_type begin, iter_type end, size_t& used_bytes)
    {
        static_assert(std::is_convertible<typename std::iterator_traits<iter_type>::value_type, uint32_t>::value, "");
        static_assert(std::is_unsigned<typename std::iterator_traits<iter_type>::value_type>::value, "");
        assert((std::is_sorted(begin, end) && std::distance(begin, end) <= k_vector_size));
        assert(begin == end || *std::prev(end) < k_vector_size);

        used_bytes = 0;
        uint32_t ones_num = static_cast<uint32_t>(std::distance(begin, end));
        if (ones_num == 0 || ones_num == k_vector_size) // special cases: zero bits used
            return;
        size_t total_bits;
        data_offsets_and_info inf = compute_offsets_and_info(ones_num);
        memset(data, 0, (inf.extra_data - 1) / 8 + 1);
        construct_hi_and_low_vectors(begin, end, inf);
        construct_samples(inf, total_bits);
        used_bytes = (total_bits + 7) / 8;
        assert((used_bytes == size_in_bytes(begin, end)));
    }

    static data_offsets_and_info compute_offsets_and_info(uint32_t ones_num)
    {
        data_offsets_and_info inf;
        inf.ones_num = ones_num;
        inf.last_pos = inf.ones_num * 2 - 1;
        inf.low_bits = k_vector_size_pow - sdsl::bits::hi(inf.ones_num); // 0 < low_bits <= 30 (see asserts)
        inf.word_size = sdsl::bits::hi(inf.last_pos) + 1; //=ceil(log2(last_pos)) (NB: last_pos is odd)
        if (inf.last_pos == 1) // special case
            inf.small_word_size = 0xff; // shouldn't be used
        else
            inf.small_word_size = k_sw * (sdsl::bits::hi(static_cast<uint32_t>(inf.word_size) - 1) + 1); //= 4*ceil(log2(word_size))
        inf.large_samples_num = 0;
        if (inf.last_pos >= k_raw_data_threshold && inf.last_pos >= k_large_samples_step)
            inf.large_samples_num = inf.last_pos / k_large_samples_step + 1;
        inf.small_samples_num = 0;
        if (inf.last_pos >= k_raw_data_threshold && inf.last_pos > 1) // last_pos == 1 is a special case
            inf.small_samples_num = inf.last_pos / k_small_samples_step + 1;
        inf.large_samples_0 = 0;
        inf.large_samples_1 = inf.large_samples_num * 64;
        inf.small_samples_0 = inf.large_samples_num * 64 * 2;
        inf.small_samples_1 = inf.large_samples_num * 64 * 2 + inf.small_samples_num * inf.small_word_size;
        inf.raw_bits = inf.large_samples_num * 64 * 2 + inf.small_samples_num * inf.small_word_size * 2;
        inf.low_vector = inf.last_pos > 1 ? inf.raw_bits + 2 * inf.ones_num : 0; // no raw bits in case last_pos == 1
        inf.extra_data = inf.low_vector + inf.ones_num * inf.low_bits;
        return inf;
    }

    template <typename iter_type>
    void construct_hi_and_low_vectors(iter_type begin, iter_type end, const data_offsets_and_info& inf)
    {
        if (inf.ones_num == 0 || inf.ones_num == k_vector_size) // special cases
            return;
        uint32_t bit_idx = 0, lv_idx = 0, next_bucket = 0;
        for (auto curr = begin; curr != end; ++curr) {
            uint32_t hi_ibit = *curr >> inf.low_bits;
            uint32_t lo_ibit = *curr & ((1u << inf.low_bits) - 1);
            while (hi_ibit >= next_bucket)
                bit_idx++,
                    next_bucket++; // memory is zero initialized so we skip zero bits
            if (inf.ones_num > 1) // in the case ones_num == 1 the high bitvector is not stored
                write_data_bit(inf.raw_bits + bit_idx);
            write_data_int32(inf.low_vector + lv_idx * inf.low_bits, lo_ibit, inf.low_bits);
            bit_idx++;
            lv_idx++;
        }
    }

    void construct_samples(const data_offsets_and_info& inf, size_t& total_bits)
    {
        total_bits = inf.extra_data;
        if (inf.last_pos < k_raw_data_threshold || inf.last_pos <= 1) // bits are in the raw form or are not stored
            return;
        uint32_t ones_count = 0, zeros_count = 0;
        uint32_t last_large_one_smp = 0, last_large_zero_smp = 0;
        std::vector<uint32_t> last_ones(k_large_samples_step), last_zeros(k_large_samples_step);
        uint32_t extra_idx = 0;
        for (uint32_t bit_idx = 0; bit_idx <= inf.last_pos; ++bit_idx)
            if (read_data_bit(inf.raw_bits + bit_idx)) {
                last_ones[ones_count % k_large_samples_step] = bit_idx;
                if (inf.large_samples_num > 0 && ones_count % k_large_samples_step == 0) {
                    last_large_one_smp = bit_idx; // the second 32 bit value is zero initialized by default
                    write_data_int32(inf.large_samples_1 + ones_count / k_large_samples_step * 64, bit_idx, 32);
                }
                if (bit_idx - last_large_one_smp < (1u << inf.small_word_size)) {
                    auto smp_off = inf.small_samples_1 + (ones_count / k_small_samples_step) * inf.small_word_size;
                    if (ones_count % k_small_samples_step == 0)
                        write_data_int32(smp_off, bit_idx - last_large_one_smp, inf.small_word_size);
                } else if (ones_count == inf.ones_num - 1 || (ones_count + 1) % k_large_samples_step == 0) {
                    assert(inf.large_samples_num > 0);
                    auto ptr_off = inf.large_samples_1 + ones_count / k_large_samples_step * 64 + 32;
                    write_data_int32(ptr_off, inf.extra_data + extra_idx * inf.word_size, 32);
                    for (uint32_t k = 0; k <= ones_count % k_large_samples_step; ++k, ++extra_idx)
                        write_data_int32(inf.extra_data + extra_idx * inf.word_size, last_ones[k], inf.word_size);
                }
                ones_count++;
            } else {
                last_zeros[zeros_count % k_large_samples_step] = bit_idx;
                if (inf.large_samples_num > 0 && zeros_count % k_large_samples_step == 0) {
                    last_large_zero_smp = bit_idx; // the second 32 bit value is zero initialized by default
                    write_data_int32(inf.large_samples_0 + zeros_count / k_large_samples_step * 64, bit_idx, 32);
                }
                if (bit_idx - last_large_zero_smp < (1u << inf.small_word_size)) {
                    auto smp_off = inf.small_samples_0 + (zeros_count / k_small_samples_step) * inf.small_word_size;
                    if (zeros_count % k_small_samples_step == 0)
                        write_data_int32(smp_off, bit_idx - last_large_zero_smp, inf.small_word_size);
                } else if (zeros_count == inf.ones_num - 1 || (zeros_count + 1) % k_large_samples_step == 0) {
                    assert(inf.large_samples_num > 0);
                    auto ptr_off = inf.large_samples_0 + zeros_count / k_large_samples_step * 64 + 32;
                    write_data_int32(ptr_off, inf.extra_data + extra_idx * inf.word_size, 32);
                    for (uint32_t k = 0; k <= zeros_count % k_large_samples_step; ++k, ++extra_idx)
                        write_data_int32(inf.extra_data + extra_idx * inf.word_size, last_zeros[k], inf.word_size);
                }
                zeros_count++;
            }
        total_bits += extra_idx * inf.word_size;
    }

    inline void write_data_bit(uint32_t offset)
    {
        auto byte_ptr = reinterpret_cast<uint8_t*>(data) + offset / 8;
        *byte_ptr |= (1u << offset % 8);
    }

    inline uint8_t read_data_bit(uint32_t offset)
    {
        auto byte_ptr = reinterpret_cast<uint8_t*>(data) + offset / 8;
        return (*byte_ptr >> offset % 8) & 1;
    }

    static inline uint32_t bits_offset(uint32_t high_bv_last_pos)
    {
        if (high_bv_last_pos < k_raw_data_threshold || (k_raw_data_threshold == 0 && high_bv_last_pos == 1))
            return 0;
        uint32_t off = 0;
        if (high_bv_last_pos >= k_large_samples_step)
            off = 64 * 2 * (high_bv_last_pos / k_large_samples_step + 1);
        uint8_t small_word_size = k_sw * (sdsl::bits::hi(sdsl::bits::hi(high_bv_last_pos)) + 1);
        return off + 2 * (high_bv_last_pos / k_small_samples_step + 1) * small_word_size;
    }

    static inline uint32_t low_vector_offset(uint32_t high_bv_last_pos)
    {
        if (high_bv_last_pos == 1) // we do not store the high bitvector in this special trivial case
            return 0;
        return bits_offset(high_bv_last_pos) + high_bv_last_pos + 1;
    }

    inline uint32_t read_data_int32(uint32_t offset, uint8_t word_size) const
    {
        assert(word_size <= 32);
        auto ptr64 = reinterpret_cast<const uint64_t*>(data + offset / 32);
        return (*ptr64 >> offset % 32) & ((1u << word_size) - 1);
    }

    inline void write_data_int32(uint32_t offset, uint32_t value, uint8_t word_size)
    {
        assert(word_size <= 32 && value < (1ull << word_size));
        auto ptr64 = reinterpret_cast<uint64_t*>(data + offset / 32);
        uint64_t mask = ((1ull << word_size) - 1) << offset % 32;
        *ptr64 = (*ptr64 & ~mask) | ((uint64_t)value << offset % 32);
    }

    inline uint32_t get_small_1_sample(uint32_t i, uint32_t large_samples_num, uint8_t word_size, uint32_t last_pos) const
    {
        uint32_t small_samples_num = last_pos / k_small_samples_step + 1;
        uint32_t smp_index = i / k_small_samples_step;
        uint8_t small_word_size = k_sw * (sdsl::bits::hi(static_cast<uint32_t>(word_size) - 1) + 1); //=4*ceil(log2(word_size))
        assert(small_word_size <= 32 - 7); // to be able to read the small word with one uint32_t access
        auto small_samples = reinterpret_cast<const uint8_t*>(data + 2 * 2 * large_samples_num);
        uint32_t offset = (small_samples_num + smp_index) * small_word_size;
        auto smp_ptr32 = reinterpret_cast<const uint32_t*>(small_samples + offset / 8);
        return (*smp_ptr32 >> offset % 8) & ((1u << small_word_size) - 1);
    }

    inline uint32_t select1_high(uint32_t i, uint32_t last_pos) const
    {
        // the special cases ones_num == 0 and ones_num == k_vector_size must be
        // processed outside
        assert(last_pos > 0 && last_pos < 2 * (uint64_t)k_vector_size - 1 && (last_pos & 1));
        assert(i >= 1 && i <= (last_pos + 1) / 2 && last_pos > 1); // trivial case last_pos==1 is processed outside

        if (last_pos < k_raw_data_threshold)
            return naive_select1(data, i);

        i--;
        uint8_t word_size = sdsl::bits::hi(last_pos) + 1; // = ceil(log2(last_pos)) (NB: last_pos is odd)
        uint32_t large_samples_num = 0, position = 0;
        if (last_pos >= k_large_samples_step) {
            large_samples_num = last_pos / k_large_samples_step + 1;
            uint32_t smp_index = i % k_large_samples_step;
            auto data_ptr64 = reinterpret_cast<const uint64_t*>(data);
            uint64_t u64 = data_ptr64[large_samples_num + i / k_large_samples_step]; // NB: skip samples of zeros
            position = (uint32_t)u64; // position of the sampled one is in the lower 32 bits
            if (smp_index == 0)
                return position;
            uint32_t sampled_group_offset = static_cast<uint32_t>(u64 >> 32);
            if (sampled_group_offset != 0)
                return read_data_int32(sampled_group_offset + smp_index * word_size,
                    word_size);
        }

        position += get_small_1_sample(i, large_samples_num, word_size, last_pos);
        i %= k_small_samples_step;
        if (i == 0)
            return position;

        uint32_t pos_off = bits_offset(last_pos) + position;
        return position - pos_off % 32 + naive_select1(data + pos_off / 32, i, ~((1ull << (pos_off % 32 + 1)) - 1));
    }

    static inline uint32_t naive_select1(const void* start_ptr, uint32_t i, uint64_t init_and_mask = ~0ull)
    {
        auto block_ptr64 = reinterpret_cast<const uint64_t*>(start_ptr);
        uint32_t res = 0;
        uint64_t bits64 = *block_ptr64 & init_and_mask;
        for (uint32_t ones = sdsl::bits::cnt(bits64); ones < i; ones = sdsl::bits::cnt(bits64)) {
            bits64 = *(++block_ptr64);
            res += 64;
            i -= ones;
        }
        return res + sdsl::bits::sel(bits64, i);
    }

    static inline uint32_t naive_select0(const void* start_ptr, uint32_t i, uint64_t init_or_mask = 0)
    {
        auto block_ptr64 = reinterpret_cast<const uint64_t*>(start_ptr);
        uint32_t res = 0;
        uint64_t bits64 = *block_ptr64 | init_or_mask;
        for (uint32_t zeros = sdsl::bits::cnt(~bits64); zeros < i; zeros = sdsl::bits::cnt(~bits64)) {
            bits64 = *(++block_ptr64);
            res += 64;
            i -= zeros;
        }
        return res + sdsl::bits::sel(~bits64, i);
    }

    static inline std::pair<uint32_t, uint32_t> naive_select0_pair(const void* start_ptr, uint32_t i, uint64_t init_or_mask = 0)
    {
        auto block_ptr64 = reinterpret_cast<const uint64_t*>(start_ptr);
        uint32_t res = 0;
        uint64_t bits64 = *block_ptr64 | init_or_mask;
        for (uint32_t zeros = sdsl::bits::cnt(~bits64); zeros < i; zeros = sdsl::bits::cnt(~bits64)) {
            bits64 = *(++block_ptr64);
            res += 64;
            i -= zeros;
        }
        uint32_t word_zero = sdsl::bits::sel(~bits64, i);
        uint32_t ith_zero = res + word_zero;
        uint64_t x = (uint64_t)1 << word_zero;
        for (bits64 |= x | (x - 1u); bits64 == std::numeric_limits<uint64_t>::max(); res += 64)
            bits64 = *(++block_ptr64);
        return std::make_pair(ith_zero, res + sdsl::bits::lo(~bits64));
    }

    static inline uint32_t naive_select_first0(const void* start_ptr, uint64_t init_or_mask)
    {
        auto block_ptr64 = reinterpret_cast<const uint64_t*>(start_ptr);
        uint32_t res = 0;
        uint64_t bits64 = *block_ptr64++ | init_or_mask;
        for (res = 0; bits64 == std::numeric_limits<uint64_t>::max(); res += 64)
            bits64 = *block_ptr64++;
        return res + sdsl::bits::lo(~bits64);
    }

    inline uint32_t get_small_0_sample(uint32_t i, uint32_t large_samples_num, uint8_t word_size) const
    {
        uint32_t smp_index = i / k_small_samples_step;
        uint8_t small_word_size = k_sw * (sdsl::bits::hi(static_cast<uint32_t>(word_size) - 1) + 1); //=4*ceil(log2(word_size))
        assert(small_word_size <= 32 - 7); // to be able to read the small word with one uint32_t access
        auto small_samples = reinterpret_cast<const uint8_t*>(data + 2 * 2 * large_samples_num);
        uint32_t offset = smp_index * small_word_size;
        auto smp_ptr32 = reinterpret_cast<const uint32_t*>(small_samples + offset / 8);
        return (*smp_ptr32 >> offset % 8) & ((1u << small_word_size) - 1);
    }

    uint32_t select0_high(uint32_t i, uint32_t last_pos) const
    {
        // the special cases ones_num == 0 and ones_num == k_vector_size must be
        // processed outside
        assert(last_pos > 0 && last_pos < 2 * (uint64_t)k_vector_size - 1 && (last_pos & 1));
        assert(i >= 1 && i <= (last_pos + 1) / 2 && last_pos > 1); // trivial case last_pos==1 is processed outside

        if (last_pos < k_raw_data_threshold)
            return naive_select0(data, i);

        i--;
        uint8_t word_size = sdsl::bits::hi(last_pos) + 1; // = ceil(log2(last_pos)) (NB: last_pos is odd)
        uint32_t large_samples_num = 0, position = 0;
        if (last_pos >= k_large_samples_step) {
            large_samples_num = last_pos / k_large_samples_step + 1;
            uint32_t smp_index = i % k_large_samples_step;
            auto data_ptr64 = reinterpret_cast<const uint64_t*>(data);
            uint64_t u64 = data_ptr64[i / k_large_samples_step];
            position = (uint32_t)u64; // position of the sampled zero is in the lower 32 bits
            if (smp_index == 0)
                return position;
            uint32_t sampled_group_offset = static_cast<uint32_t>(u64 >> 32);
            if (sampled_group_offset != 0)
                return read_data_int32(sampled_group_offset + smp_index * word_size, word_size);
        }

        position += get_small_0_sample(i, large_samples_num, word_size);
        i %= k_small_samples_step;
        if (i == 0)
            return position;
        uint32_t pos_off = bits_offset(last_pos) + position;
        return position - pos_off % 32 + naive_select0(data + pos_off / 32, i, (1ull << (pos_off % 32 + 1)) - 1);
    }

    std::pair<uint32_t, uint32_t> select0_high_pair(uint32_t i, uint32_t last_pos) const
    {
        // the special cases ones_num == 0 and ones_num == k_vector_size must be
        // processed outside
        assert(last_pos > 0 && last_pos < 2 * (uint64_t)k_vector_size - 1 && (last_pos & 1));
        assert(i >= 1 && i < (last_pos + 1) / 2); // the i-th and (i+1)-st zeros
            // must exist ((last_pos+1)/2 is
            // the num of zeros)

        if (last_pos < k_raw_data_threshold)
            return naive_select0_pair(data, i);

        i--;
        uint8_t word_size = sdsl::bits::hi(last_pos) + 1; // = ceil(log2(last_pos)) (NB: last_pos is odd)
        uint32_t large_samples_num = 0, position = 0;
        if (last_pos >= k_large_samples_step) {
            large_samples_num = last_pos / k_large_samples_step + 1;
            auto data_ptr64 = reinterpret_cast<const uint64_t*>(data);
            uint64_t u64 = data_ptr64[i / k_large_samples_step];
            position = (uint32_t)u64; // position of the sampled zero is in the lower 32 bits
            uint32_t sampled_group_offset = static_cast<uint32_t>(u64 >> 32);
            uint32_t smp_index = i % k_large_samples_step;
            if ((i + 1) % k_large_samples_step == 0) {
                uint32_t position_second = static_cast<uint32_t>(data_ptr64[(i + 1) / k_large_samples_step]);
                if (sampled_group_offset != 0) {
                    position = read_data_int32(sampled_group_offset + smp_index * word_size, word_size);
                    return std::make_pair(position, position_second);
                }
                position += get_small_0_sample(i, large_samples_num, word_size);
                uint32_t pos_off = bits_offset(last_pos) + position;
                uint32_t res = naive_select0(data + pos_off / 32, i % k_small_samples_step, (1ull << (pos_off % 32 + 1)) - 1);
                return std::make_pair(position - pos_off % 32 + res, position_second);
            } else if (sampled_group_offset != 0) {
                uint32_t off = sampled_group_offset + smp_index * word_size;
                return std::make_pair(read_data_int32(off, word_size), read_data_int32(off + word_size, word_size));
            } else if (smp_index == 0) {
                uint32_t pos_off = bits_offset(last_pos) + position;
                uint32_t res = naive_select_first0(data + pos_off / 32, (1ull << (pos_off % 32 + 1)) - 1);
                return std::make_pair(position, position + res - pos_off % 32);
            }
        }

        position += get_small_0_sample(i, large_samples_num, word_size);
        i %= k_small_samples_step;
        if (i == 0) {
            uint32_t pos_off = bits_offset(last_pos) + position;
            uint32_t res = naive_select_first0(data + pos_off / 32, (1ull << (pos_off % 32 + 1)) - 1);
            return std::make_pair(position, position + res - pos_off % 32);
        }

        uint32_t pos_off = bits_offset(last_pos) + position;
        auto res = naive_select0_pair(data + pos_off / 32, i, (1ull << (pos_off % 32 + 1)) - 1);
        uint32_t shift = position - pos_off % 32;
        return std::make_pair(shift + res.first, shift + res.second);
    }
};

#endif // ELIAS_FANO_HPP
