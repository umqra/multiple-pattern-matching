#ifndef FILE_UTILS_HPP
#define FILE_UTILS_HPP

#include <codecvt>
#include <fstream>
#include <iostream>
#include <locale>
#include <stdint.h>
#include <string>
#include <vector>

#include "alphabet_concepts.hpp"
#include "string_seq.hpp"

void remove_file(const std::string& filename)
{
    if (remove(filename.c_str()) != 0)
        throw std::logic_error("Unable to remove file '" + filename + "'");
}

std::string read_all(const std::string& filename)
{
    std::ifstream in(filename, std::ios::binary);
    return std::string(std::istreambuf_iterator<char>(in),
        std::istreambuf_iterator<char>());
}

std::vector<std::string> read_lines(const std::string& filename)
{
    std::vector<std::string> lines = {};
    std::ifstream file(filename, std::ios::binary);
    std::string token;
    while (getline(file, token)) {
        lines.push_back(token);
    }
    return lines;
}

template <typename alphabet>
string_seq read_seq_all(const std::string& filename);

template <typename alphabet>
std::vector<string_seq> read_seq_lines(const std::string& filename);

template <>
string_seq read_seq_all<byte_alphabet>(const std::string& filename)
{
    return string_seq(read_all(filename));
}

template <>
std::vector<string_seq>
read_seq_lines<byte_alphabet>(const std::string& filename)
{
    auto lines = read_lines(filename);
    auto seq = std::vector<string_seq>(lines.size());
    for (size_t i = 0; i < lines.size(); i++) {
        seq[i] = string_seq(lines[i]);
    }
    return seq;
}

#if defined(_MSC_VER) && _MSC_VER >= 1900
std::u16string convert_to_utf16(std::string s) // this implementation is due to a bug in MSVC
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    auto res = converter.from_bytes(s);
    return std::u16string(res.begin(), res.end());
}
std::u32string convert_to_utf32(std::string s) // this implementation is due to a bug in MSVC
{
    std::wstring_convert<std::codecvt_utf8<__int32>, __int32> converter;
    auto res = converter.from_bytes(s);
    return std::u32string(res.begin(), res.end());
}
#else
std::u16string convert_to_utf16(std::string s)
{
    std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
    return converter.from_bytes(s);
}
std::u32string convert_to_utf32(std::string s)
{
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
    return converter.from_bytes(s);
}
#endif

template <>
string_seq read_seq_all<utf16_alphabet>(const std::string& filename)
{
    auto data = read_all(filename);
    return string_seq(convert_to_utf16(data));
}

template <>
std::vector<string_seq> read_seq_lines<utf16_alphabet>(const std::string& filename)
{
    auto lines = read_lines(filename);
    auto converted_lines = std::vector<string_seq>(lines.size());
    for (size_t i = 0; i < lines.size(); i++) {
        converted_lines[i] = string_seq(convert_to_utf16(lines[i]));
    }
    return converted_lines;
}

template <>
string_seq read_seq_all<utf32_alphabet>(const std::string& filename)
{
    auto data = read_all(filename);
    return string_seq(convert_to_utf32(data));
}

template <>
std::vector<string_seq> read_seq_lines<utf32_alphabet>(const std::string& filename)
{
    auto lines = read_lines(filename);
    auto converted_lines = std::vector<string_seq>(lines.size());
    for (size_t i = 0; i < lines.size(); i++) {
        converted_lines[i] = string_seq(convert_to_utf32(lines[i]));
    }
    return converted_lines;
}

bool file_exists(const std::string& filename)
{
    std::ifstream in(filename);
    return in.good();
}

#endif // FILE_UTILS_HPP
