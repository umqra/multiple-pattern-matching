#ifndef ELIAS_FANO_HELPERS_HPP
#define ELIAS_FANO_HELPERS_HPP

#include <cassert>
#include <limits>
#include <mmintrin.h>
#include <stdint.h>

#ifdef _MSC_VER
#include <intrin.h>
#pragma intrinsic(_BitScanForward64)
#pragma intrinsic(_BitScanReverse64)
inline uint32_t trailing_one(uint64_t value)
{
    assert(value);
    unsigned long res;
    _BitScanForward64(&res, value);
    return res;
}
inline uint32_t leading_one(uint64_t value)
{
    assert(value);
    unsigned long res;
    _BitScanReverse64(&res, value);
    return res;
}
inline uint32_t trailing_one(uint32_t value)
{
    assert(value);
    unsigned long res;
    _BitScanForward(&res, value);
    return res;
}
inline uint32_t leading_one(uint32_t value)
{
    assert(value);
    unsigned long res;
    _BitScanReverse(&res, value);
    return res;
}
#else
inline uint32_t trailing_one(uint64_t value)
{
    assert(value);
    return __builtin_ctzll(value);
}
inline uint32_t leading_one(uint64_t value)
{
    assert(value);
    return 63 - __builtin_clzll(value);
}
inline uint32_t trailing_one(uint32_t value)
{
    assert(value);
    return __builtin_ctz(value);
}
inline uint32_t leading_one(uint32_t value)
{
    assert(value);
    return 63 - __builtin_clz(value);
}
#endif

constexpr uint32_t int_log2(uint32_t val)
{
    return val ? 1u + int_log2(val >> 1) : std::numeric_limits<uint32_t>::max();
}

#endif // ELIAS_FANO_HELPERS_HPP
