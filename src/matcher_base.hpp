
#ifndef MATCHER_BASE_HPP
#define MATCHER_BASE_HPP

#include <vector>

#include "aho_build_tools.hpp"
#include "dictionary.hpp"
#include <sdsl/structure_tree.hpp>

class matcher_base {
public:
    using size_type = size_t;
    virtual std::vector<occurence>
    enumerate_occurence_positions(const string_seq& text) const = 0;
    virtual size_t serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const = 0;
    virtual void load(std::istream& in) = 0;
};

#endif // MATCHER_BASE_HPP
