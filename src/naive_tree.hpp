#ifndef NAIVE_TREE_INCLUDED
#define NAIVE_TREE_INCLUDED

#include "tree_utils.hpp"
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/bp_support.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/util.hpp>
#include <vector>

template <class t_bit_vector = sdsl::bit_vector>
class naive_tree {
private:
    t_bit_vector dfuds_sequence;
    size_t tree_size;

public:
    using size_type = size_t;

    naive_tree() = default;
    naive_tree(const naive_tree& other) { copy(other); }
    naive_tree(naive_tree&& other) { swap(other); }
    naive_tree& operator=(const naive_tree& other)
    {
        if (this != &other)
            copy(other);
        return *this;
    }
    naive_tree& operator=(naive_tree&& other)
    {
        swap(other);
        return *this;
    }

    explicit naive_tree(const std::vector<size_t>& dfs_order_vertex_degree)
    {
        tree_size = dfs_order_vertex_degree.size();
        assert(tree_size > 0);

        dfuds_sequence = t_bit_vector(2 * tree_size);
        size_t sequence_pointer = 1;
        for (auto degree : dfs_order_vertex_degree)
        {
            dfuds_sequence[sequence_pointer] = 1;
            sequence_pointer += degree + 1;
        }
    }

    size_t size() const { return tree_size; }

    size_t parent(size_t vertex) const
    {
        assert(0 <= vertex && vertex < size());
        if (vertex == 0)
            return 0;

        size_t closing_bracket_id = 0;
        size_t closing_bracket_position = 0;
        for (closing_bracket_position = 0; closing_bracket_position < dfuds_sequence.size(); closing_bracket_position++)
        {
            if (dfuds_sequence[closing_bracket_position] != 1)
                continue;
            if (closing_bracket_id == vertex)
                break;
            closing_bracket_id++;
        }
        size_t balance = 1;
        do
        {
            closing_bracket_position--;
            if (dfuds_sequence[closing_bracket_position] == 1)
            {
                closing_bracket_id--;
                balance++;
            }
            else
                balance--;
        } while (balance != 0);
        return closing_bracket_id - 1;
    }

    void swap(naive_tree& other)
    {
        if (this != &other) {
            dfuds_sequence.swap(other.dfuds_sequence);
            std::swap(tree_size, other.tree_size);
        }
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::write_member(tree_size, out, child, "tree_size");
        written_bytes += dfuds_sequence.serialize(out, child, "dfuds_sequence");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        sdsl::read_member(tree_size, in);
        dfuds_sequence.load(in);
    }

private:
    void copy(const naive_tree& other)
    {
        dfuds_sequence = other.dfuds_sequence;
        tree_size = other.tree_size;
    }
};

#endif // NAIVE_TREE_INCLUDED
