#ifndef RAW_TRIE_HPP
#define RAW_TRIE_HPP

#include "alphabet_mapper.hpp"
#include "dictionary.hpp"
#include <cstdlib>
#include <unordered_set>
#include <vector>

// TODO: rename to the trie
class raw_trie {
    size_t effective_sigma;

    void add_vertex(size_t v_parent, size_t v_char)
    {
        parent.push_back(v_parent);
        parent_char.push_back(v_char);
    }
    size_t count_effective_alphabet() const
    {
        if (size() <= 1)
            return 0; // first parent_char[0] is a dummy character (since root doesn't have a parent)
        return std::unordered_set<size_t>(parent_char.begin() + 1, parent_char.end()).size();
    }

public:
    std::vector<size_t> parent;
    std::vector<size_t> parent_char;

    raw_trie() = default;

    raw_trie(const std::vector<size_t>& _parent,
        const std::vector<size_t>& _parent_char)
    {
        parent = _parent;
        parent_char = _parent_char;
        effective_sigma = count_effective_alphabet();
    }
    raw_trie(const dictionary& dict)
        : raw_trie({ 0 }, { 0 })
    {
        trie_iterator iterator(dict);
        while (iterator.move()) {
            add_vertex(iterator.from(), iterator.character());
        }
        effective_sigma = count_effective_alphabet();
    }
    size_t effective_alphabet_size() const { return effective_sigma; }
    alphabet_mapper compute_alphabet_mapper() const
    {
        if (size() <= 1) // first parent_char[0] is a dummy character (since root doesn't have a parent)
            return alphabet_mapper();
        return alphabet_mapper(parent_char.begin() + 1, parent_char.end());
    }
    size_t size() const { return parent.size(); }
    size_t size_in_bytes() const // estimated memory size
    {
        return sizeof(effective_sigma) + (parent.size() + parent_char.size() + 2) * sizeof(size_t) + 2 * sizeof(void*);
    }
    raw_trie permute(const std::vector<size_t> permutation, const std::vector<size_t> i_permutation) const
    {
        auto new_parent = std::vector<size_t>(size());
        auto new_parent_char = std::vector<size_t>(size());
        for (size_t v = 0; v < size(); v++) {
            new_parent[v] = i_permutation[parent[permutation[v]]];
            new_parent_char[v] = parent_char[permutation[v]];
        }
        return raw_trie(new_parent, new_parent_char);
    }
};

std::ostream& operator<<(std::ostream& ostream, const raw_trie& trie)
{
    ostream << "raw_trie (";
    for (size_t v = 0; v < trie.size(); v++)
        ostream << "[" << trie.parent[v] << "," << trie.parent_char[v] << "], ";
    return ostream << ")";
}

#endif // RAW_TRIE_HPP
