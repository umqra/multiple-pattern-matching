#ifndef SKIP_LAYER_TREE_HPP
#define SKIP_LAYER_TREE_HPP

#include "tree_utils.hpp"
#include "trees.h"
#include <algorithm>
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <vector>

// TODO: change semantic of skip_count argument?
template <size_t skip_count, typename tree_type = compressed_tree<>>
class skip_layer_tree {
private:
    tree_type tree;

public:
    using size_type = size_t; // required by SDSL for serialize/load

    skip_layer_tree() = default;
    skip_layer_tree(const skip_layer_tree&) = default;
    skip_layer_tree(skip_layer_tree&&) = default;
    skip_layer_tree& operator=(const skip_layer_tree&) = default;
    skip_layer_tree& operator=(skip_layer_tree&&) = default;

    skip_layer_tree(const std::vector<size_t>& degrees)
    {
        // TODO: align skip_count + 1 to the nearest power of two?
        auto iterator = tree_iterator(degrees);
        auto layer_sizes = std::vector<size_t>(skip_count + 1, 0);
        while (iterator.move()) {
            layer_sizes[iterator.height() % (skip_count + 1)]++;
        }
        size_t smallest_layer = std::min_element(layer_sizes.begin(), layer_sizes.end()) - layer_sizes.begin();
        iterator = tree_iterator(degrees);
        auto vertex_mask = sdsl::bit_vector(degrees.size());
        vertex_mask[0] = 1;

        while (iterator.move()) {
            if (iterator.height() % (skip_count + 1) == smallest_layer)
                vertex_mask[iterator.vertex()] = 1;
        }
        tree = tree_type(degrees, vertex_mask);
    }
    bool is_marked(size_t vertex) const { return tree.is_marked(vertex); }
    bool try_get_parent(size_t vertex, size_t& parent) const
    {
        return tree.try_get_parent(vertex, parent);
    }
    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += tree.serialize(out, child, "tree");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in) { tree.load(in); }
};

#endif // SKIP_LAYER_TREE_HPP
