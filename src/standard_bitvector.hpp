#ifndef STANDARD_BITVECTOR_HPP
#define STANDARD_BITVECTOR_HPP

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

#include <sdsl/bit_vectors.hpp>
#include <sdsl/io.hpp>
#include <sdsl/structure_tree.hpp>
#include <sdsl/util.hpp>

#include "elias-fano.hpp"

template <typename bitvector_type = sdsl::sd_vector<>>
class standard_bitvector {
    using bitvector_rank_type = typename bitvector_type::rank_1_type;
    using bitvector_select_type = typename bitvector_type::select_1_type;

    bitvector_type bitvector;
    bitvector_rank_type rank_support;
    bitvector_select_type select_support;
    size_t max_one, total_size, elements_num;

    void swap(standard_bitvector& m)
    {
        if (this != &m) {
            std::swap(max_one, m.max_one);
            std::swap(total_size, m.total_size);
            std::swap(elements_num, m.elements_num);
            bitvector.swap(m.bitvector);
            rank_support.swap(m.rank_support);
            rank_support.set_vector(&bitvector);
            select_support.swap(m.select_support);
            select_support.set_vector(&bitvector);
        }
    }
    void copy(const standard_bitvector& m)
    {
        max_one = m.max_one;
        total_size = m.total_size;
        elements_num = m.elements_num;
        bitvector = m.bitvector;
        rank_support = m.rank_support;
        rank_support.set_vector(&bitvector);
        select_support = m.select_support;
        select_support.set_vector(&bitvector);
    }

public:
    using size_type = size_t; // accessed by SDSL in the serialization of vectors of decltype(*this)

    standard_bitvector() = default;

    standard_bitvector(const standard_bitvector& m) { copy(m); }
    standard_bitvector(standard_bitvector&& m) { swap(m); }
    standard_bitvector& operator=(standard_bitvector m)
    {
        if (this != &m)
            swap(m);
        return *this;
    }

    standard_bitvector(const std::vector<size_t>& elements, size_t size)
        : standard_bitvector(elements.begin(), elements.end(), size)
    {
    }

    template <typename iter_type>
    standard_bitvector(iter_type begin, iter_type end, size_t size)
    {
        assert((std::is_sorted(begin, end) && std::distance(begin, end) > 0));
        total_size = size;
        elements_num = std::distance(begin, end);
        max_one = *std::max_element(begin, end);
        bitvector = bitvector_type(begin, end);
        sdsl::util::init_support(rank_support, &bitvector);
        sdsl::util::init_support(select_support, &bitvector);
    }

    size_t operator[](size_t i) const
    {
        assert(i < total_size);
        if (i > max_one)
            return 0;
        return bitvector[i];
    }

    size_t size() const { return total_size; }

    /// Return the number of ones in the positions 0,1,...,i-1
    size_t rank1(size_t i) const
    {
        assert(i < total_size);
        if (i > max_one)
            return elements_num;
        return rank_support(i);
    }

    /// Return the number of ones in the positions 0,1,...,i-1 and the bit at
    /// position i
    size_t rank1(size_t i, uint8_t& ibit) const
    {
        assert(i < total_size);
        ibit = static_cast<uint8_t>((*this)[i]);
        return rank1(i);
    }

    /// Return the position (0,1,...,size()-1) of the i-th one (i=1,2,...,count())
    size_t select1(size_t i) const
    {
        assert(1 <= i && i <= elements_num);
        return select_support(i);
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += sdsl::write_member(max_one, out, child, "max_one");
        written_bytes += sdsl::write_member(total_size, out, child, "total_size");
        written_bytes += sdsl::serialize(bitvector, out, child, "bitvector");
        written_bytes += sdsl::serialize(rank_support, out, child, "rank_support");
        written_bytes += sdsl::serialize(select_support, out, child, "select_support");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    void load(std::istream& in)
    {
        sdsl::read_member(max_one, in);
        sdsl::read_member(total_size, in);
        bitvector.load(in);
        rank_support.load(in, &bitvector);
        select_support.load(in, &bitvector);
    }
};

#endif // STANDARD_BITVECTOR_HPP
