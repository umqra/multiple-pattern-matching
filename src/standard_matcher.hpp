#ifndef STANDARD_MATCHER_HPP
#define STANDARD_MATCHER_HPP

#include <cstdlib>
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/io.hpp>
#include <sdsl/structure_tree.hpp>
#include <string>
#include <vector>

#include "aho_build_tools.hpp"
#include "belazzougui.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "raw_trie.hpp"

template <typename bitvector_type = sdsl::sd_vector<>,
    typename failure_link_tree_type = dfuds_tree<>,
    typename report_link_tree_type = ultra_dfuds_tree<>,
    typename dict_support_info_type = dict_support_info<>>
class standard_matcher : public matcher_base {
private:
    typedef belazzougui<
        transitions_succinct_map<standard_bitvector<bitvector_type>>,
        failure_link_tree_type, report_link_tree_type, dict_support_info_type>
        algo_type;
    algo_type algorithm;

public:
    standard_matcher() = default;
    standard_matcher(const standard_matcher&) = default;
    standard_matcher(standard_matcher&&) = default;
    standard_matcher& operator=(const standard_matcher&) = default;
    standard_matcher& operator=(standard_matcher&&) = default;

    explicit standard_matcher(const dictionary& dict)
        : algorithm(raw_trie(dict), dict)
    {
    }

    // TODO: return iterator to consume less memory?
    std::vector<occurence> enumerate_occurence_positions(const string_seq& text) const override
    {
        std::vector<occurence> occurences = {};
        size_t current_vertex = 0;
        for (size_t position = 0; position < text.length(); position++) {
            current_vertex = algorithm.get_next_transition(current_vertex, text, position);
            add_occurence_positions(position, current_vertex, occurences);
        }
        return occurences;
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const override
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += algorithm.serialize(out, child, "algorithm");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in) override { algorithm.load(in); }

private:
    size_t root() const { return 0; }

    void add_occurence_positions(size_t position, size_t vertex, std::vector<occurence>& occurences) const
    {
        if (algorithm.is_terminal_vertex(vertex))
            occurences.push_back(occurence(position - algorithm.get_word_length(vertex) + 1, position));
        vertex = algorithm.get_report_link(vertex);
        while (vertex != root()) {
            occurences.push_back(occurence(position - algorithm.get_word_length(vertex) + 1, position));
            vertex = algorithm.get_report_link(vertex);
        }
    }
};

#endif // STANDARD_MATCHER_HPP
