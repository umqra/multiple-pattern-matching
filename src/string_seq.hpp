#ifndef STRING_SEQ_HPP
#define STRING_SEQ_HPP

#include <string>
#include <vector>

class string_seq {
private:
    uint8_t width_in_bytes;

public:
    std::vector<size_t> char_codes;
    explicit string_seq()
        : width_in_bytes(1)
        , char_codes()
    {
    }
    explicit string_seq(std::string s)
    {
        char_codes.resize(s.length());
        for (size_t i = 0; i < s.length(); i++) {
            char_codes[i] = (unsigned char)s[i];
        }
        width_in_bytes = 1;
    }
    explicit string_seq(std::u16string s)
    {
        char_codes = std::vector<size_t>(s.begin(), s.end());
        width_in_bytes = 2;
    }
    explicit string_seq(std::u32string s)
    {
        char_codes = std::vector<size_t>(s.begin(), s.end());
        width_in_bytes = 4; // TODO: this constants repeated in alphabet_concepts.hpp
    }
    explicit string_seq(std::vector<size_t> _char_codes, uint8_t _width_in_bytes)
    {
        char_codes = _char_codes;
        width_in_bytes = _width_in_bytes;
    }

    size_t operator[](const int index) const { return char_codes[index]; }

    size_t length() const { return char_codes.size(); }

    size_t size_in_bytes() const { return length() * width_in_bytes; }

    // TODO: rel_ops?
    bool operator<(const string_seq& other) const
    {
        return char_codes < other.char_codes;
    }

    bool operator==(const string_seq& other) const
    {
        return char_codes == other.char_codes;
    }
};

std::ostream& operator<<(std::ostream& ostream, const string_seq& seq)
{
    ostream << "string_seq (";
    for (size_t i = 0; i < seq.length(); i++)
        ostream << seq[i] << ", ";
    return ostream << ")";
}

#endif // STRING_SEQ_HPP
