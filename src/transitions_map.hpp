#ifndef TRANSITIONS_MAP_HPP
#define TRANSITIONS_MAP_HPP

#include "dictionary.hpp"
#include <cstdlib>
#include <map>
#include <unordered_map>

class transitions_map {
private:
    std::unordered_map<size_t, std::unordered_map<size_t, size_t>> trans;

public:
    transitions_map()
        : trans()
    {
    }
    transitions_map(const dictionary& dictionary)
        : trans()
    {
        auto iterator = trie_iterator(dictionary);
        while (iterator.move()) {
            trans[iterator.from()][iterator.character()] = iterator.to();
        }
    }

    bool try_next(size_t from, size_t symbol, size_t& next_vertex) const
    {
        auto it_vertex = trans.find(from);
        if (it_vertex == trans.end())
            return false;
        auto it_vertex_symbol = it_vertex->second.find(symbol);
        if (it_vertex_symbol == it_vertex->second.end())
            return false;
        next_vertex = it_vertex_symbol->second;
        return true;
    }

    size_t size_in_bytes() const // estimated size in bytes for a typical unordered_map
    {
        size_t res = 4 * sizeof(size_t);
        size_t sz = trans.size();
        res += (sz + sz / 2) * sizeof(size_t) + sz * sizeof(size_t) * 5;
        for (const auto& elem : trans) {
            sz = elem.second.size();
            res += (sz + sz / 2) * sizeof(size_t) + sz * sizeof(size_t) * 2;
        }
        return res;
    }
};

#endif // TRANSITIONS_MAP_HPP
