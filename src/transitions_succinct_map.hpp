#ifndef TRANSITIONS_SUCCINCT_MAP_HPP
#define TRANSITIONS_SUCCINCT_MAP_HPP

#include "aho_build_tools.hpp"
#include "compression_boosting.hpp"
#include "raw_trie.hpp"
#include "standard_bitvector.hpp"
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sdsl/sd_vector.hpp>

template <typename bit_vector_type = standard_bitvector<>>
class transitions_succinct_map {
private:
    alphabet_mapper alphabet_map;
    std::vector<bit_vector_type> trans_by_symbol;
    std::vector<size_t> prefix_sum;

public:
    using size_type = size_t; // required for serialize/load

    transitions_succinct_map() = default;
    transitions_succinct_map(const transitions_succinct_map& m) = default;
    transitions_succinct_map(transitions_succinct_map&& m) = default;
    transitions_succinct_map& operator=(const transitions_succinct_map& m) = default;
    transitions_succinct_map& operator=(transitions_succinct_map&& m) = default;

    explicit transitions_succinct_map(const raw_trie& trie)
    {
        assert(trie.size() > 0);

        alphabet_map = trie.compute_alphabet_mapper();
        size_t sigma = alphabet_map.effective_alphabet_size();
        trans_by_symbol = std::vector<bit_vector_type>(sigma);
        prefix_sum = std::vector<size_t>(sigma);

        // TODO: we already have trie with good numeration and don't need this method
        auto symbol_vertex_order = get_symbol_vertex_order(trie);
        size_t order_ptr = 0;
        for (size_t symbol = 0; symbol < alphabet_map.size(); symbol++) {
            size_t mapped_sym = alphabet_map[symbol];
            if (!alphabet_mapper::is_valid_mapped_symbol(mapped_sym))
                continue;
            prefix_sum[mapped_sym] = order_ptr;
            size_t next_group_symbol = 0;
            if (order_ptr < symbol_vertex_order.size())
                next_group_symbol = alphabet_map[symbol_vertex_order[order_ptr].first];
            if (order_ptr == symbol_vertex_order.size() || next_group_symbol != mapped_sym)
                throw std::logic_error("We expect that all symbols from the alphabet_map are used in the trie");

            auto symbol_bits = std::vector<size_t>();
            while (order_ptr < symbol_vertex_order.size() && next_group_symbol == mapped_sym) {
                symbol_bits.push_back(symbol_vertex_order[order_ptr].second);
                order_ptr++;
                if (order_ptr < symbol_vertex_order.size())
                    next_group_symbol = alphabet_map[symbol_vertex_order[order_ptr].first];
            }
            trans_by_symbol[mapped_sym] = bit_vector_type(symbol_bits.begin(), symbol_bits.end(), trie.size());
        }
    }

    bool try_next(size_t from, size_t symbol, size_t& next) const
    {
        size_t mapped_sym = alphabet_map[symbol];
        if (!alphabet_mapper::is_valid_mapped_symbol(mapped_sym))
            return false;
        uint8_t bit;
        size_t res = trans_by_symbol[mapped_sym].rank1(from, bit);
        if (bit == 0)
            return false;
        next = res + 1 + prefix_sum[mapped_sym];
        return true;
    }

    size_t parent(size_t vertex, size_t parent_symbol) const
    {
        size_t mapped_sym = alphabet_map[parent_symbol];
        return trans_by_symbol[mapped_sym].select1(vertex - prefix_sum[mapped_sym]);
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += sdsl::serialize(alphabet_map, out, child, "alphabet_map");
        written_bytes += sdsl::serialize(trans_by_symbol, out, child, "trans_by_symbol");
        written_bytes += sdsl::serialize(prefix_sum, out, child, "prefix_sum");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    void load(std::istream& in)
    {
        sdsl::load(alphabet_map, in);
        sdsl::load(trans_by_symbol, in);
        sdsl::load(prefix_sum, in);
    }
};

#endif // TRANSITIONS_SUCCINCT_MAP_HPP
