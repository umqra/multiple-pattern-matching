#ifndef TREE_UTILS_HPP
#define TREE_UTILS_HPP

#include <stdexcept>
#include <vector>

class tree_iterator {
protected:
    // TODO: unnecessary copy. We can simply store const ref
    std::vector<size_t> vertex_degrees;
    std::vector<std::pair<size_t, size_t>> ids_and_degree_stack;
    std::vector<std::pair<size_t, size_t>> info_stack;
    size_t vertex_id;
    bool first_move;

public:
    tree_iterator(const std::vector<size_t>& _vertex_degrees)
        : vertex_degrees(_vertex_degrees)
        , ids_and_degree_stack({})
        , info_stack({})
        , vertex_id(0)
        , first_move(true)
    {
    }

    bool has_parent() const { return ids_and_degree_stack.size() > 0; }

    size_t id_in_parent() const
    {
        size_t p = parent();
        return vertex_degrees[p] - ids_and_degree_stack.back().second - 1;
    }

    size_t parent_degree() const { return vertex_degrees[parent()]; }

    size_t parent() const
    {
        if (ids_and_degree_stack.size() > 0)
            return ids_and_degree_stack.back().first;
        throw std::logic_error("Try get parent for root vertex");
    }

    size_t vertex() const { return vertex_id; }

    size_t degree() const { return vertex_degrees[vertex_id]; }

    bool has_info() const { return info_stack.size() > 0; }

    size_t info() const
    {
        if (info_stack.size() > 0)
            return info_stack.back().second;
        throw std::logic_error("Try get info from empty stack");
    }

    size_t height() const { return ids_and_degree_stack.size(); }

    void push_info(size_t info)
    {
        info_stack.push_back(std::make_pair(vertex(), info));
    }

    bool move()
    {
        if (first_move) {
            first_move = false;
            return !vertex_degrees.empty();
        }
        ids_and_degree_stack.push_back(std::make_pair(vertex(), degree()));
        while (ids_and_degree_stack.size() > 0 && ids_and_degree_stack.back().second == 0) {
            if (info_stack.size() > 0 && ids_and_degree_stack.back().first == info_stack.back().first)
                info_stack.pop_back();
            ids_and_degree_stack.pop_back();
        }
        if (ids_and_degree_stack.size() == 0)
            return false;
        ids_and_degree_stack.back().second--;
        vertex_id++;
        return true;
    }
};

#endif // TREE_UTILS_HPP
