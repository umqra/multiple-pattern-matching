#ifndef TREES_H
#define TREES_H

#include "compressed_tree.hpp"
#include "dfuds_tree.hpp"
#include "skip_layer_tree.hpp"
#include "ultra_dfuds_tree.hpp"

#endif // TREES_H
