#ifndef TRIE_MATCHER_HPP
#define TRIE_MATCHER_HPP

#include "aho_build_tools.hpp"
#include "dict_support_info.hpp"
#include "dictionary.hpp"
#include "matcher_base.hpp"
#include "string_seq.hpp"
#include "transitions.h"

#include <cstdlib>
#include <sdsl/io.hpp>
#include <string>
#include <vector>

#include <iostream>

template <typename transitions_type = transitions_succinct_map<>,
    typename dict_support_info_type = dict_support_info<>>
class trie_matcher : public matcher_base {
private:
    transitions_type trans;
    dict_support_info_type support_info;

public:
    trie_matcher() = default;
    trie_matcher(const trie_matcher&) = default;
    trie_matcher(trie_matcher&&) = default;
    trie_matcher& operator=(const trie_matcher&) = default;
    trie_matcher& operator=(trie_matcher&&) = default;

    trie_matcher(const dictionary& dict)
    {
        auto trie = raw_trie(dict);
        auto trie_array = trie_path_array(trie);
        trie = trie.permute(trie_array.vertices_order, trie_array.i_vertices_order);
        trans = transitions_type(trie);
        support_info = dict_support_info_type(dict, trie_array);
    }

    std::vector<occurence> enumerate_occurence_positions(const string_seq& text) const override
    {
        auto occurences = std::vector<occurence>();
        for (size_t p = 0; p < text.length(); p++) {
            size_t v = 0, next_v;
            size_t len = 0;
            while (p + len < text.length() && trans.try_next(v, text[p + len], next_v)) {
                v = next_v;
                if (support_info.is_terminal_vertex(v)) {
                    occurences.push_back(occurence(p, p + len));
                }
                len++;
            }
        }
        return occurences;
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const override
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += trans.serialize(out, child, "trans");
        written_bytes += support_info.serialize(out, child, "support_info");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in) override
    {
        trans.load(in);
        support_info.load(in);
    }
};

#endif // TRIE_MATCHER_HPP
