#ifndef TRIE_PATH_ARRAY_HPP
#define TRIE_PATH_ARRAY_HPP

#include "raw_trie.hpp"

#include <algorithm>
#include <cassert>
#include <ctime>
#include <iostream>
#include <numeric>
#include <tuple>
#include <vector>

class trie_path_array {
public:
    std::vector<size_t> vertices_order;
    std::vector<size_t> i_vertices_order;

    trie_path_array(const raw_trie& trie)
    {
        size_t size = trie.size();
        assert(0 < size);

        vertices_order = std::vector<size_t>(size);
        std::iota(vertices_order.begin(), vertices_order.end(), 0);

        std::vector<size_t> parent = trie.parent, classes = trie.parent_char;
        // Ensure that root has minimal class value among all other vertices
        for (size_t v = 1; v < size; v++) {
            classes[v] += 1;
        }
        // TODO: refactor this vector?
        auto classes_tuple = std::vector<std::tuple<size_t, size_t, size_t>>(size);

        for (size_t len = 1; len < size; len <<= 1) {
            for (size_t v = 0; v < size; v++)
                classes_tuple[v] = std::make_tuple(classes[v], classes[parent[v]], v);
            std::sort(classes_tuple.begin(), classes_tuple.end());

            classes[std::get<2>(classes_tuple[0])] = 1;
            for (size_t id = 1; id < size; id++) {
                classes[std::get<2>(classes_tuple[id])] = classes[std::get<2>(classes_tuple[id - 1])];
                if (std::get<0>(classes_tuple[id - 1]) != std::get<0>(classes_tuple[id])
                    || std::get<1>(classes_tuple[id - 1]) != std::get<1>(classes_tuple[id]))
                    classes[std::get<2>(classes_tuple[id])]++;
            }
            calc_far_parents(size, parent);
            if (classes[std::get<2>(classes_tuple.back())] == size || len * 2 >= size) {
                for (size_t id = 0; id < size; id++)
                    vertices_order[id] = std::get<2>(classes_tuple[id]);
                break;
            }
        }

        i_vertices_order = get_inverse_permutation(vertices_order);
    }
    size_t at_position(size_t index) const { return vertices_order[index]; }

    size_t vertex_order(size_t index) const { return i_vertices_order[index]; }

private:
    // TODO: move to utils header?
    std::vector<size_t> get_inverse_permutation(std::vector<size_t> permutation)
    {
        std::vector<size_t> i_permutation = std::vector<size_t>(permutation.size());
        for (size_t id = 0; id < permutation.size(); id++) {
            i_permutation[permutation[id]] = id;
        }
        return i_permutation;
    }

    void calc_far_parents(size_t size, std::vector<size_t>& parent)
    {
        // TODO: avoid static data
        static std::vector<size_t> tmp_parent;
        tmp_parent.resize(size);

        for (size_t v = 0; v < size; v++) {
            tmp_parent[v] = parent[parent[v]];
        }
        std::swap(parent, tmp_parent);
    }
};

#endif // TRIE_PATH_ARRAY_HPP
