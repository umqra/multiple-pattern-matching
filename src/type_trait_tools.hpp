#ifndef TYPE_TRAIT_TOOLS_HPP
#define TYPE_TRAIT_TOOLS_HPP

#include <cstdlib>
#include <type_traits>

template <typename T>
struct is_full_tree {
private:
    static void detect(...);
    template <typename U>
    static decltype(std::declval<U>().parent(0)) detect(const U&);

public:
    static constexpr bool value = std::is_same<size_t, decltype(detect(std::declval<T>()))>::value;
};

template <typename T>
struct is_partial_tree {
private:
    size_t parent;
    static void detect(...);
    template <typename U>
    static decltype(std::declval<U>().try_get_parent(static_cast<size_t>(0), parent)) detect(const U&);

public:
    static constexpr bool value = std::is_same<bool, decltype(detect(std::declval<T>()))>::value;
};

#endif // TYPE_TRAIT_TOOLS_HPP
