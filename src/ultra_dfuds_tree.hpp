#ifndef ULTRA_DFUDS_TREE
#define ULTRA_DFUDS_TREE

#include "dfuds_tree.hpp"
#include <sdsl/bit_vectors.hpp>
#include <vector>

template <class dfuds_tree_type = dfuds_tree<>,
    class rrr_bit_vector = sdsl::rrr_vector<>,
    class rrr_bit_vector_rank1 = typename rrr_bit_vector::rank_1_type,
    class rrr_bit_vector_select1 = typename rrr_bit_vector::select_1_type>
class ultra_dfuds_tree {
private:
    using bit_vector = sdsl::bit_vector;

    dfuds_tree_type internal_vertices_tree;

    rrr_bit_vector internal_vertices_mask;
    rrr_bit_vector_rank1 internal_vertices_mask_rank;
    rrr_bit_vector_select1 internal_vertices_mask_select;

public:
    using size_type = size_t;

    ultra_dfuds_tree() = default;

    ultra_dfuds_tree(const ultra_dfuds_tree& other) { copy(other); }

    ultra_dfuds_tree(ultra_dfuds_tree&& other) { swap(other); }

    ultra_dfuds_tree& operator=(ultra_dfuds_tree other)
    {
        swap(other);
        return *this;
    }

    explicit ultra_dfuds_tree(const std::vector<size_t>& dfs_order_vertex_degree)
    {
        build_internal_vertices_subtree(dfs_order_vertex_degree);

        sdsl::util::init_support(internal_vertices_mask_rank, &internal_vertices_mask);
        sdsl::util::init_support(internal_vertices_mask_select, &internal_vertices_mask);
    }

    size_t parent(size_t vertex) const
    {
        auto internal_id = internal_vertices_mask_rank(vertex);
        auto internal_parent_id = internal_vertices_tree.parent(internal_id);
        return internal_vertices_mask_select(internal_parent_id + 1);
    }

    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v = nullptr, std::string name = "") const
    {
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;

        written_bytes += internal_vertices_tree.serialize(out, child, "internal_vertices_tree");
        written_bytes += internal_vertices_mask.serialize(out, child, "internal_vertices_mask");
        written_bytes += internal_vertices_mask_rank.serialize(out, child, "internal_vertices_mask_rank");
        written_bytes += internal_vertices_mask_select.serialize(out, child, "internal_vertices_mask_select");
        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }
    void load(std::istream& in)
    {
        internal_vertices_tree.load(in);
        internal_vertices_mask.load(in);
        internal_vertices_mask_rank.load(in, &internal_vertices_mask);
        internal_vertices_mask_select.load(in, &internal_vertices_mask);
    }

private:
    void build_internal_vertices_subtree(const std::vector<size_t>& dfs_order_vertex_degree)
    {
        auto base_dfs = get_base_dfs_order(dfs_order_vertex_degree);
        internal_vertices_tree = dfuds_tree_type(base_dfs);
        internal_vertices_mask = build_internal_vertices_mask(dfs_order_vertex_degree);
    }

    rrr_bit_vector build_internal_vertices_mask(const std::vector<size_t>& degrees) const
    {
        auto mask = bit_vector(degrees.size(), 0);
        tree_iterator iterator = tree_iterator(degrees);
        while (iterator.move()) {
            if (is_base_vertex(iterator)) {
                mask[iterator.vertex()] = 1;
            }
        }
        return rrr_bit_vector(mask);
    }
    std::vector<size_t> get_base_dfs_order(const std::vector<size_t>& dfs_order_vertex_degree) const
    {
        std::vector<size_t> dfs_base_degrees;
        tree_iterator iterator = tree_iterator(dfs_order_vertex_degree);
        while (iterator.move()) {
            if (is_base_vertex(iterator)) {
                iterator.push_info(dfs_base_degrees.size());
                dfs_base_degrees.push_back(iterator.degree());
            } else {
                auto parent_id = iterator.info();
                dfs_base_degrees[parent_id]--;
            }
        }
        return dfs_base_degrees;
    }
    bool is_base_vertex(const tree_iterator& iterator) const
    {
        return !iterator.has_parent() || iterator.degree() > 0 || iterator.id_in_parent() + 1 == iterator.parent_degree();
    }

    // TODO: make swap method public
    void swap(ultra_dfuds_tree& other)
    {
        if (this != &other) {
            internal_vertices_tree.swap(other.internal_vertices_tree);
            internal_vertices_mask.swap(other.internal_vertices_mask);
            internal_vertices_mask_rank.swap(other.internal_vertices_mask_rank);
            internal_vertices_mask_rank.set_vector(&internal_vertices_mask);
            internal_vertices_mask_select.swap(other.internal_vertices_mask_select);
            internal_vertices_mask_select.set_vector(&internal_vertices_mask);
        }
    }
    void copy(const ultra_dfuds_tree& other)
    {
        internal_vertices_tree = other.internal_vertices_tree;
        internal_vertices_mask = other.internal_vertices_mask;
        internal_vertices_mask_rank = other.internal_vertices_mask_rank;
        internal_vertices_mask_rank.set_vector(&internal_vertices_mask);
        internal_vertices_mask_select = other.internal_vertices_mask_select;
        internal_vertices_mask_select.set_vector(&internal_vertices_mask);
    }
};

#endif // ULTRA_DFUDS_TREE
