
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <sstream>
#include <stdint.h>
#include <vector>

#include "compression_boosting.hpp"

template <uint32_t block_size>
static bool test_vector_rank(const compression_boosting_bitvector<block_size>& bv, uint32_t pos, uint32_t ans, uint8_t ans_bit)
{
    uint8_t ibit = static_cast<uint8_t>(bv[pos]);
    if (ibit != ans_bit)
        std::cerr << "read error: pos = " << pos << " ans/got = " << (int)ans_bit
                  << "/" << (int)ibit << std::endl;
    ibit = 0xff;
    uint32_t got = bv.rank1(pos, ibit);
    if (got != ans)
        std::cerr << "rank error: pos = " << pos << " ans/got = " << ans << "/"
                  << got << std::endl;
    else if (ibit != ans_bit)
        std::cerr << "ibit error: pos = " << pos << " ans/got = " << (int)ans_bit
                  << "/" << (int)ibit << std::endl;
    return got == ans && ibit == ans_bit;
}

template <uint32_t block_size>
static bool test_vector_select(const compression_boosting_bitvector<block_size>& bv, uint32_t pos, uint32_t ans)
{
    uint32_t got = bv.select1(pos);
    if (got != ans)
        std::cerr << "select error: index = " << pos << " ans/got = " << ans << "/"
                  << got << std::endl;
    return got == ans;
}

template <uint32_t block_size>
static compression_boosting_bitvector<block_size>
test_serialization(const compression_boosting_bitvector<block_size>& bv)
{
    std::stringstream buf;
    bv.serialize(buf);
    compression_boosting_bitvector<block_size> bv2;
    bv2.load(buf);
    return bv2;
}

template <uint32_t block_size>
static bool test_vector(size_t length, const std::vector<uint32_t>& positions_of_ones)
{
    auto positions = positions_of_ones; // copy in local
    assert((std::is_sorted(positions.begin(), positions.end())));

    compression_boosting_bitvector<block_size> bv(positions.begin(),
        positions.end(), length);
    compression_boosting_bitvector<block_size> tmp_bv = test_serialization(bv); // test move constructor
    bv = tmp_bv; // test assignment operator and copy constuctor

    typedef struct {
        uint32_t ans;
        uint32_t pos;
        uint8_t bit;
    } test_desc;
    std::vector<test_desc> tests;
    positions.push_back(length); // sentinel element
    for (uint32_t ans = 0, pos = 0, idx = 0; pos < length; ++pos) {
        if (positions[idx] < pos)
            ans++, idx++;
        tests.push_back({ ans, pos, positions[idx] == pos });
    }
    positions.pop_back(); // remove the sentinel element

    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_index(0, length - 1);
    std::random_shuffle(tests.begin(), tests.end(),
        [&](int i) { return rand_index(gen) % i; });

    bool succ = true;
    for (auto test : tests)
        if (!test_vector_rank(bv, test.pos, test.ans, test.bit)) {
            succ = false;
            break;
        }

    std::vector<uint32_t> sel_test(positions.size());
    std::iota(sel_test.begin(), sel_test.end(), 1);
    std::random_shuffle(sel_test.begin(), sel_test.end(),
        [&](int i) { return rand_index(gen) % i; });
    for (auto i : sel_test)
        if (!test_vector_select(bv, i, positions[i - 1])) {
            succ = false;
            break;
        }
    return succ;
}

template <uint32_t block_size>
static bool test_random(size_t length, double probability_of_one)
{
    int x = 100000;
    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_elem(0, x - 1);

    std::vector<uint32_t> positions;
    for (uint32_t i = 0; i < length; ++i)
        if ((double)rand_elem(gen) / x <= probability_of_one)
            positions.push_back(i);
    return test_vector<block_size>(length, positions);
}

template <uint32_t block_size>
static bool test_random_runs(size_t length, double run_prob, double run_1_interrupt_prob)
{
    int x = 100000;
    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_elem(0, x - 1);

    std::vector<uint32_t> positions;
    bool in_run = (rand_elem(gen) % 2 == 0);
    for (uint32_t i = 0; i < length; ++i) {
        double rand_num = (double)rand_elem(gen) / x;
        if (in_run)
            in_run = !(rand_num <= run_1_interrupt_prob);
        else
            in_run = (rand_num <= run_prob);
        if (in_run)
            positions.push_back(i);
    }
    return test_vector<block_size>(length, positions);
}

template <uint32_t block_size>
static void test_random_1000(size_t length, double prob, const int test_num = 1000)
{
    std::cout << "test: block_size = " << block_size << ", length = " << length
              << ", one's density = " << std::setprecision(5) << prob << "; ";
    bool succ = true;
    for (int i = 0; succ && i < test_num; ++i)
        succ = test_random<block_size>(length, prob);
    if (succ)
        std::cout << "OK" << std::endl;
}

template <uint32_t block_size>
static void test_random_run_1000(size_t length, double prob, double run_1_interrupt_prob, const int test_num = 1000)
{
    if (run_1_interrupt_prob == 0)
        run_1_interrupt_prob = prob;
    std::cout << "test runs: block_size = " << block_size
              << ", length = " << length
              << ", runs freq/~len = " << std::setprecision(5) << prob << "/"
              << 1 / run_1_interrupt_prob << "; ";
    bool succ = true;
    for (int i = 0; succ && i < test_num; ++i)
        succ = test_random_runs<block_size>(length, prob, run_1_interrupt_prob);
    if (succ)
        std::cout << "OK" << std::endl;
}

void test_compression_boosting()
{
    test_random_run_1000<512>(1000000, 0.001, 0.01, 20);
    test_random_run_1000<512>(1000000, 0.00001, 0.0001, 20);
    test_random_run_1000<512>(1000000, 0.00001, 0.01, 20);
    test_random_1000<512>(1000011, 0.999, 20);
    test_random_1000<512>(1000011, 0.99, 20);
    test_random_1000<512>(1000011, 0.9, 20);
    test_random_1000<512>(1000011, 0.7, 20);
    test_random_1000<512>(1000011, 0.5, 20);
    test_random_1000<512>(1000011, 0.3, 20);
    test_random_1000<512>(1000011, 0.1, 20);
    test_random_1000<512>(1000011, 0.01, 20);
    test_random_1000<512>(1000011, 0.001, 20);
}
