
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <stdint.h>
#include <vector>

#include "elias-fano.hpp"
#include <sdsl/rrr_vector.hpp>
#include <sdsl/sd_vector.hpp>

template <uint32_t vector_size>
static bool test_vector_rank(const elias_fano_bitvector<vector_size>* bv, uint32_t ones_num, uint32_t pos, uint32_t ans, uint8_t ans_bit)
{
    uint8_t ibit;
    uint32_t got = bv->rank1(pos, ones_num, ibit);
    if (got != ans)
        std::cerr << "rank error: pos = " << pos << " ans/got = " << ans << "/"
                  << got << std::endl;
    else if (ibit != ans_bit)
        std::cerr << "ibit error: pos = " << pos << " ans/got = " << (int)ans_bit
                  << "/" << (int)ibit << std::endl;
    return got == ans && ibit == ans_bit;
}

template <uint32_t vector_size>
static bool test_vector_select(const elias_fano_bitvector<vector_size>* bv, uint32_t ones_num, uint32_t pos, uint32_t ans)
{
    uint32_t got = bv->select1(pos, ones_num);
    if (got != ans)
        std::cerr << "select error: index = " << pos << " ans/got = " << ans << "/"
                  << got << std::endl;
    return got == ans;
}

template <uint32_t vector_size>
static bool test_vector(const std::vector<uint32_t>& positions_of_ones,
    std::chrono::milliseconds& rank_ms,
    std::chrono::milliseconds& select_ms,
    std::chrono::milliseconds& sd_rank_ms,
    std::chrono::milliseconds& sd_select_ms)
{
    auto positions = positions_of_ones;
    assert((std::is_sorted(positions.begin(), positions.end())));
    size_t size = elias_fano_bitvector<vector_size>::size_in_bytes(
        positions.begin(), positions.end());
    std::vector<uint32_t> mem((size + 3) / 4); // 4 byte aligned
    auto bv = new (mem.data())
        elias_fano_bitvector<vector_size>(positions.begin(), positions.end());
    uint32_t ones_num = (uint32_t)positions.size();

    positions.push_back(vector_size - 1); // to fix the size of sd_vector
    typedef sdsl::sd_vector<> test_bitvector_type;
    test_bitvector_type sd(positions.begin(), positions.end());
    test_bitvector_type::rank_1_type sd_rank(&sd);
    test_bitvector_type::select_1_type sd_select(&sd);

    positions.back() = vector_size; // sentinel element
    typedef struct {
        uint32_t ans;
        uint32_t pos;
        uint8_t bit;
    } test_desc;
    std::vector<test_desc> tests;
    for (uint32_t ans = 0, pos = 0, idx = 0; pos < vector_size; ++pos) {
        if (positions[idx] < pos)
            ans++, idx++;
        tests.push_back({ ans, pos, positions[idx] == pos });
    }
    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_index(0, vector_size - 1);
    std::random_shuffle(tests.begin(), tests.end(),
        [&](int i) { return rand_index(gen) % i; });

    auto rank_start_time = std::chrono::high_resolution_clock::now();
    bool succ = true;
    for (auto test : tests)
        if (!test_vector_rank(bv, ones_num, test.pos, test.ans, test.bit)) {
            succ = false;
            break;
        }
    auto rank_end_time = std::chrono::high_resolution_clock::now();
    rank_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        rank_end_time - rank_start_time);

    rank_start_time = std::chrono::high_resolution_clock::now();
    for (auto test : tests) {
        auto sd_bit = test.pos == vector_size - 1 ? test.bit : sd[test.pos];
        if (sd_bit != test.bit) // if (sd_rank(test.pos) + sd_bit != test.ans + test.bit)
            std::cerr << "it cannot happen" << std::endl;
    }
    rank_end_time = std::chrono::high_resolution_clock::now();
    sd_rank_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        rank_end_time - rank_start_time);

    positions.pop_back(); // remove the sentinel element
    std::vector<uint32_t> sel_test(positions.size());
    std::iota(sel_test.begin(), sel_test.end(), 1);
    std::random_shuffle(sel_test.begin(), sel_test.end(),
        [&](int i) { return rand_index(gen) % i; });
    auto select_start_time = std::chrono::high_resolution_clock::now();
    for (auto i : sel_test)
        if (!test_vector_select(bv, ones_num, i, positions[i - 1])) {
            succ = false;
            break;
        }
    auto select_end_time = std::chrono::high_resolution_clock::now();
    select_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        select_end_time - select_start_time);

    select_start_time = std::chrono::high_resolution_clock::now();
    for (auto i : sel_test)
        if (sd_select(i) != positions[i - 1])
            std::cout << "it cannot happen" << std::endl;
    select_end_time = std::chrono::high_resolution_clock::now();
    sd_select_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        select_end_time - select_start_time);
    return succ;
}

template <uint32_t vector_size>
static bool test_vector(const std::vector<uint32_t>& positions,
    std::chrono::milliseconds& rank_ms,
    std::chrono::milliseconds& select_ms)
{
    std::chrono::milliseconds sd_rank_ms, sd_select_ms;
    return test_vector<vector_size>(positions, rank_ms, select_ms, sd_rank_ms,
        sd_select_ms);
}

template <uint32_t vector_size>
static bool test_random(double probability_of_one,
    std::chrono::milliseconds& rank_ms,
    std::chrono::milliseconds& select_ms,
    std::chrono::milliseconds& sd_rank_ms,
    std::chrono::milliseconds& sd_select_ms)
{
    int x = 100000;
    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_elem(0, x - 1);

    std::vector<uint32_t> positions;
    uint32_t ones_num = 0;
    for (uint32_t i = 0; i < vector_size; ++i)
        if ((double)rand_elem(gen) / x <= probability_of_one) {
            positions.push_back(i);
            ones_num++;
        }
    if (ones_num == vector_size)
        positions.erase(positions.begin() + rand_elem(gen) % vector_size);
    if (ones_num == 0)
        positions.push_back(rand_elem(gen) % vector_size);
    return test_vector<vector_size>(positions, rank_ms, select_ms, sd_rank_ms,
        sd_select_ms);
}

template <uint32_t vector_size>
static bool test_random_runs(double run_prob, double run_1_interrupt_prob,
    std::chrono::milliseconds& rank_ms,
    std::chrono::milliseconds& select_ms,
    std::chrono::milliseconds& sd_rank_ms,
    std::chrono::milliseconds& sd_select_ms)
{
    int x = 100000;
    std::random_device rand_dev;
    std::default_random_engine gen(rand_dev());
    std::uniform_int_distribution<int> rand_elem(0, x - 1);

    std::vector<uint32_t> positions;
    uint32_t ones_num = 0;
    bool in_run = (rand_elem(gen) % 2 == 0);
    for (uint32_t i = 0; i < vector_size; ++i) {
        double rand_num = (double)rand_elem(gen) / x;
        if (in_run)
            in_run = !(rand_num <= run_1_interrupt_prob);
        else
            in_run = (rand_num <= run_prob);
        if (in_run) {
            positions.push_back(i);
            ones_num++;
        }
    }
    if (ones_num == vector_size)
        positions.erase(positions.begin() + rand_elem(gen) % vector_size);
    if (ones_num == 0)
        positions.push_back(rand_elem(gen) % vector_size);
    return test_vector<vector_size>(positions, rank_ms, select_ms, sd_rank_ms,
        sd_select_ms);
}

template <uint32_t vector_size>
static void test_random_1000(double prob, const int test_num = 1000)
{
    std::cout << "test: vector_size = " << vector_size
              << ", one's density = " << std::setprecision(5) << prob << "; ";
    bool succ = true;
    uint64_t rank_total = 0, select_total = 0, sd_rank_total = 0,
             sd_select_total = 0;
    for (int i = 0; succ && i < test_num; ++i) {
        std::chrono::milliseconds rank_ms, select_ms, sd_rank_ms, sd_select_ms;
        succ = test_random<vector_size>(prob, rank_ms, select_ms, sd_rank_ms,
            sd_select_ms);
        rank_total += static_cast<uint64_t>(rank_ms.count());
        select_total += static_cast<uint64_t>(select_ms.count());
        sd_rank_total += static_cast<uint64_t>(sd_rank_ms.count());
        sd_select_total += static_cast<uint64_t>(sd_select_ms.count());
    }
    if (succ)
        std::cout << "OK (rnk/sdrnk: " << rank_total << "/" << sd_rank_total
                  << ", sel/sdsel: " << select_total << "/" << sd_select_total
                  << ")" << std::endl;
}

template <uint32_t vector_size>
static void test_random_run_1000(double prob, double run_1_interrupt_prob,
    const int test_num = 1000)
{
    std::cout << "test runs: vector_size = " << vector_size
              << ", runs freq/~len = " << std::setprecision(5) << prob << "/"
              << 1 / run_1_interrupt_prob << "; ";
    bool succ = true;
    uint64_t rank_total = 0, select_total = 0, sd_rank_total = 0,
             sd_select_total = 0;
    for (int i = 0; succ && i < test_num; ++i) {
        std::chrono::milliseconds rank_ms, select_ms, sd_rank_ms, sd_select_ms;
        succ = test_random_runs<vector_size>(prob, run_1_interrupt_prob, rank_ms,
            select_ms, sd_rank_ms, sd_select_ms);
        rank_total += static_cast<uint64_t>(rank_ms.count());
        select_total += static_cast<uint64_t>(select_ms.count());
        sd_rank_total += static_cast<uint64_t>(sd_rank_ms.count());
        sd_select_total += static_cast<uint64_t>(sd_select_ms.count());
    }
    if (succ)
        std::cout << "OK (rnk/sdrnk: " << rank_total << "/" << sd_rank_total
                  << ", sel/sdsel: " << select_total << "/" << sd_select_total
                  << ")" << std::endl;
}

static void test_special()
{
    std::cout << "some special tests;";
    std::chrono::milliseconds rank_ms, select_ms;
    std::vector<uint32_t> poss;
    bool succ = true;
    succ = succ && test_vector<64>({ 0 }, rank_ms, select_ms);
    succ = succ && test_vector<64>({ 0, 1 }, rank_ms, select_ms);
    succ = succ && test_vector<64>({ 0, 1, 2, 3, 4, 5, 6, 7, 8 }, rank_ms, select_ms);
    succ = succ && test_vector<64>({ 60 }, rank_ms, select_ms);
    succ = succ && test_vector<64>({ 0, 2, 4, 8, 10, 12, 14, 16, 18, 20 }, rank_ms, select_ms);
    poss.resize(29);
    int i = 0;
    std::generate(poss.begin(), poss.end(), [&] { return i++; });
    succ = succ && test_vector<64>(poss, rank_ms, select_ms);
    std::cout << (succ ? " OK" : " ERROR!!!") << std::endl
              << std::endl;
}

void test_elias_fano_bitvector()
{
    test_special();

    //	test_random_1000<1048576>(0.999, 100);
    //	test_random_1000<1048576>(0.001, 100);
    test_random_run_1000<1048576>(0.0001, 0.01, 100);
    test_random_run_1000<1048576>(0.01, 0.0001, 100);
    test_random_run_1000<262144>(0.0002, 0.01, 100);
    test_random_run_1000<262144>(0.01, 0.0002, 100);
    test_random_run_1000<65536>(0.001, 0.001, 100);
    test_random_run_1000<65536>(0.0004, 0.001, 100);
    test_random_1000<262144>(0.999, 20);
    test_random_1000<262144>(0.001, 20);
    test_random_1000<65536>(0.001, 100);
    test_random_1000<65536>(0.999, 100);
    test_random_1000<4096>(0.1);

    test_random_1000<64>(0.99);
    test_random_1000<64>(0.9);
    test_random_1000<64>(0.5);
    test_random_1000<64>(0.1);
    test_random_1000<64>(0.01);

    test_random_1000<128>(0.99);
    test_random_1000<128>(0.9);
    test_random_1000<128>(0.5);
    test_random_1000<128>(0.1);
    test_random_1000<128>(0.01);

    test_random_1000<512>(0.99);
    test_random_1000<512>(0.9);
    test_random_1000<512>(0.5);
    test_random_1000<512>(0.1);
    test_random_1000<512>(0.01);

    test_random_1000<4096>(0.999);
    test_random_1000<4096>(0.99);
    test_random_1000<4096>(0.9);
    test_random_1000<4096>(0.5);
    test_random_1000<4096>(0.1);
    test_random_1000<4096>(0.01);
    test_random_1000<4096>(0.001);

    test_random_1000<16384>(0.9999);
    test_random_1000<16384>(0.999);
    test_random_1000<16384>(0.99);
    test_random_1000<16384>(0.9);
    test_random_1000<16384>(0.5);
    test_random_1000<16384>(0.1);
    test_random_1000<16384>(0.01);
    test_random_1000<16384>(0.001);
    test_random_1000<16384>(0.0001);

    test_random_1000<65536>(0.9999);
    test_random_1000<65536>(0.999);
    test_random_1000<65536>(0.99);
    test_random_1000<65536>(0.9);
    test_random_1000<65536>(0.7);
    test_random_1000<65536>(0.5);
    test_random_1000<65536>(0.3);
    test_random_1000<65536>(0.1);
    test_random_1000<65536>(0.01);
    test_random_1000<65536>(0.001);
    test_random_1000<65536>(0.0001);

    test_random_run_1000<65536>(0.00001, 0.0001);
    test_random_run_1000<65536>(0.0001, 0.0001);
    test_random_run_1000<65536>(0.001, 0.001);
    test_random_run_1000<65536>(0.01, 0.01);
}
