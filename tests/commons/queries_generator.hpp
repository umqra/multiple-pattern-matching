#ifndef QUERIES_GENERATOR_HPP_INCLUDED
#define QUERIES_GENERATOR_HPP_INCLUDED

#include <vector>
#include "random.hpp"

class RandomQueriesGenerator
{
private:
    Random random;
    const Tree& tree;
public:
    explicit RandomQueriesGenerator(const Tree& _tree) : random(0), tree(_tree)
    {
    }
    size_t next_vertex()
    {
        return random.next_int(0, tree.size());
    }
};

class FatVerticesQueriesGenerator
{
private:
    Random random;
    const Tree& tree;
    std::vector<uint64_t> cumulative_distribution;
public:
    explicit FatVerticesQueriesGenerator(const Tree& _tree) : random(0), tree(_tree)
    {
        auto subtree_sizes = tree.subtree_sizes();
        cumulative_distribution = std::vector<uint64_t>(tree.size());
        for (size_t i = 1; i < tree.size(); i++)
        {
            cumulative_distribution[i] = cumulative_distribution[i - 1] + subtree_sizes[i - 1];
        }
    }
    size_t next_vertex()
    {
        auto sample = random.next_long(0, cumulative_distribution.back());
        auto upper_bound_ptr = std::upper_bound(cumulative_distribution.begin(), cumulative_distribution.end(), sample);
        auto parent = static_cast<size_t>(upper_bound_ptr - cumulative_distribution.begin() - 1);
        auto adjacent_vertices = tree.adjacent_vertices(parent);
        if (adjacent_vertices.empty())
            return parent;
        return random.sample(adjacent_vertices);
    }
};

template<typename TreeQueriesGenerator>
class BracketSequenceQueriesGenerator
{
private:
    TreeQueriesGenerator tree_queries_generator;
    std::vector<size_t> closing_bracket_position;
public:
    explicit BracketSequenceQueriesGenerator(const Tree& _tree) : tree_queries_generator(_tree), closing_bracket_position()
    {
        auto dfuds_sequence = _tree.dfuds_sequence();
        closing_bracket_position = std::vector<size_t>();
        for (size_t i = 0; i < dfuds_sequence.size(); i++)
        {
            if (dfuds_sequence[i] == 1)
            {
                closing_bracket_position.push_back(i);
            }
        }
    }
    size_t next_pair_query()
    {
        auto vertex = tree_queries_generator.next_vertex();
        return closing_bracket_position[vertex];
    }
    size_t next_select_query()
    {
        return tree_queries_generator.next_vertex();
    }
};

#endif // QUERIES_GENERATOR_HPP_INCLUDED