#ifndef RANDOM_UTILS_HPP_INCLUDED
#define RANDOM_UTILS_HPP_INCLUDED

#include <random>

class Random
{
private:
    std::mt19937_64 _generator;
public:
    explicit Random(int seed) : _generator(seed)
    {
    }
    size_t next_int(size_t from_inclusive, size_t to_exclusive)
    {
        return std::uniform_int_distribution<size_t>(from_inclusive, to_exclusive - 1)(_generator);
    }
    uint64_t next_long(uint64_t from_inclusive, uint64_t to_exclusive)
    {
        return std::uniform_int_distribution<uint64_t>(from_inclusive, to_exclusive - 1)(_generator);
    }
    template<typename T>
    T sample(std::vector<T> sequence)
    {
        return sequence[next_int(0, sequence.size())];
    }
    std::mt19937_64 generator() const
    {
        return _generator;
    }
};

#endif // RANDOM_UTILS_HPP_INCLUDED