#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

#include <chrono>
#include <ctime>
#include <map>
#include <ostream>
#include <sdsl/bit_vectors.hpp>
#include <unordered_map>
#include <vector>

#include "aho_build_tools.hpp"
#include "alphabet_concepts.hpp"
#include "string_seq.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "tree.hpp"

template <class T>
std::ostream& operator<<(std::ostream& ostream, const std::vector<T>& v)
{
    ostream << "(";
    for (const auto& element : v)
        ostream << element << ", ";
    return ostream << ")";
}

std::mt19937 random_generator;

void initilize_random(size_t seed)
{
    random_generator = std::mt19937(seed);
}

size_t random_int(size_t max_value_exclusive)
{
    return random_generator() % max_value_exclusive;
}

std::unordered_map<size_t, std::vector<size_t>> get_adjacency_map(std::vector<size_t> parents)
{
    std::unordered_map<size_t, std::vector<size_t>> adjecent_vertices;
    for (size_t v = 1; v < parents.size(); v++)
        adjecent_vertices[parents[v]].push_back(v);
    return adjecent_vertices;
}

void add_degrees(size_t v, std::vector<size_t>& degrees, const std::unordered_map<size_t, std::vector<size_t>>& adjecent_vertices)
{
    auto iterator = adjecent_vertices.find(v);
    if (iterator == adjecent_vertices.end()) {
        degrees.push_back(0);
        return;
    }
    degrees.push_back(iterator->second.size());
    for (const auto& adjacent : iterator->second)
        add_degrees(adjacent, degrees, adjecent_vertices);
}

void get_parents(size_t v, size_t p, std::vector<size_t>& parents, const std::unordered_map<size_t, std::vector<size_t>>& adjecent_vertices)
{
    size_t v_id = parents.size();
    parents.push_back(p);
    auto iterator = adjecent_vertices.find(v);
    if (iterator == adjecent_vertices.end())
        return;
    for (const auto& adjacent : iterator->second)
        get_parents(adjacent, v_id, parents, adjecent_vertices);
}

std::vector<size_t> get_tree_degrees(std::vector<size_t> parents)
{
    std::vector<size_t> degrees = {};
    add_degrees(0, degrees, get_adjacency_map(parents));
    return degrees;
}

std::vector<size_t> get_order_tree_parents(std::vector<size_t> parents)
{
    std::vector<size_t> order_parents = {};
    get_parents(0, 0, order_parents, get_adjacency_map(parents));
    return order_parents;
}

struct Trie {
    std::mt19937 generator;
    std::vector<size_t> v_degrees;
    std::vector<size_t> v_parents;
    std::vector<size_t> v_characters;
    Trie(size_t vertices, size_t sigma, size_t seed = 0)
    {
        generator = std::mt19937(seed);
        auto r_parents = std::vector<size_t>(vertices, 0);
        v_degrees = std::vector<size_t>(vertices, 0);
        for (size_t v = 1; v < vertices; v++) {
            do {
                r_parents[v] = generator() % v;
            } while (v_degrees[r_parents[v]] == sigma);
            v_degrees[r_parents[v]] += 1;
        }
        v_degrees = get_tree_degrees(r_parents);
        v_parents = get_order_tree_parents(r_parents);
        std::vector<size_t> alphabet = std::vector<size_t>(sigma, 0);
        std::iota(alphabet.begin(), alphabet.end(), 1);
        auto vertex_labels = std::vector<std::vector<size_t>>(vertices);
        for (size_t v = 0; v < vertices; v++) {
            std::shuffle(alphabet.begin(), alphabet.end(), std::default_random_engine(seed));
            sort(alphabet.begin(), alphabet.begin() + v_degrees[v], std::greater<size_t>());
            vertex_labels[v] = std::vector<size_t>(alphabet.begin(), alphabet.begin() + v_degrees[v]);
        }

        v_characters = std::vector<size_t>(vertices, 0);
        for (size_t v = 1; v < vertices; v++) {
            v_characters[v] = vertex_labels[v_parents[v]].back();
            vertex_labels[v_parents[v]].pop_back();
        }
    }
    size_t size() const
    {
        return v_parents.size();
    }
    size_t parent(size_t vertex) const
    {
        return v_parents[vertex];
    }
    std::vector<size_t> degrees() const
    {
        return v_degrees;
    }
    std::vector<size_t> characters() const
    {
        return v_characters;
    }
    std::vector<size_t> parents() const
    {
        return v_parents;
    }
};

std::ostream& operator<<(std::ostream& ostream, const Tree& tree)
{
    ostream << "tree (";
    for (size_t p : tree.parents())
        ostream << p << ", ";
    return ostream << ")";
}

std::ostream& operator<<(std::ostream& ostream, const Trie& trie)
{
    ostream << "trie (";
    for (size_t v = 0; v < trie.size(); v++)
        ostream << "[" << trie.v_parents[v] << "," << trie.v_characters[v] << "], ";
    return ostream << ")";
}

template <typename tree_type>
size_t get_parent(const tree_type& tree, size_t v)
{
    size_t parent = 0;
    EXPECT_TRUE(tree.try_get_parent(v, parent));
    return parent;
}

template <typename tree_type>
bool is_parent_exists(const tree_type& tree, size_t v)
{
    size_t parent;
    return tree.try_get_parent(v, parent);
}

std::tuple<std::string, std::string> extract_key_value(const std::string& s)
{
    auto colon_pos = s.find(":");
    if (colon_pos == std::string::npos)
        throw std::logic_error("Malformed line in config file: '" + s + "' (must be in format 'key:value')");
    return std::make_tuple(s.substr(0, colon_pos), s.substr(colon_pos + 1));
}

template <typename alphabet>
class test_configuration {
private:
    const std::string OrderedDictFileKey = "ordered_dict_file";
    const std::string HexDictFileKey = "hex_dict_file";
    const std::string TextFileKey = "text_file";
    const std::string OccurencesCountKey = "occurences_count";
    const std::string DescriptionKey = "description";

    std::string base_dir;
    std::string config_filename;
    std::map<std::string, std::string> keys;

public:
    test_configuration(std::string _base_dir, std::string _config_filename)
    {
        base_dir = _base_dir;
        config_filename = _config_filename;
        for (auto line : read_lines(get_full_filename(config_filename))) {
            if (line.empty() || line[0] == '#')
                continue;
            std::string key, value;
            std::tie(key, value) = extract_key_value(line);
            keys[key] = value;
        }
    }
    std::unique_ptr<dictionary> dict() const
    {
        if (has_key(OrderedDictFileKey))
            return std::make_unique<ordered_dictionary_file<alphabet>>(get_full_filename(get_key(OrderedDictFileKey)));
        if (has_key(HexDictFileKey)) {
            auto dict_filename = get_full_filename(get_key(HexDictFileKey));
            return std::make_unique<hex_dictionary<alphabet>>(ordered_dictionary_file<byte_alphabet>(dict_filename));
        }
        throw std::logic_error("Unable to find suitable key for dictionary file in config file '" + get_full_filename(config_filename) + "'");
    }
    size_t text_size_in_bytes() const
    {
        return sdsl::util::file_size(get_full_filename(get_key(TextFileKey)));
    }
    // TODO: copy-paste from dict() (don't want to call dict() because it will cause full reading of dictionary file)
    size_t dict_size_in_bytes() const
    {
        if (has_key(OrderedDictFileKey))
            return sdsl::util::file_size(get_full_filename(get_key(OrderedDictFileKey)));
        if (has_key(HexDictFileKey))
            return sdsl::util::file_size(get_full_filename(get_key(HexDictFileKey))) / 2; // One byte encoded with two hex-digits
        throw std::logic_error("Unable to find suitable key for dictionary file in config file '" + get_full_filename(config_filename) + "'");
    }
    string_seq text() const
    {
        return read_seq_all<alphabet>(get_full_filename(get_key(TextFileKey)));
    }
    std::string dictionary_location() const
    {
        if (has_key(OrderedDictFileKey))
            return get_full_filename(get_key(OrderedDictFileKey));
        if (has_key(HexDictFileKey))
            return get_full_filename(get_key(HexDictFileKey));
        throw std::logic_error("Unable to find suitable key for dictionary file in config file '" + get_full_filename(config_filename) + "'");
    }
    std::string text_location() const
    {
        return get_full_filename(get_key(TextFileKey));
    }
    std::string description() const
    {
        return get_key(DescriptionKey);
    }
    size_t occurences_count() const
    {
        return atoi(get_key(OccurencesCountKey).c_str());
    }

private:
    std::string get_full_filename(const std::string& filename) const
    {
        return base_dir + "/" + filename;
    }
    std::string get_key(const std::string& key) const
    {
        if (keys.count(key))
            return keys.find(key)->second;
        throw std::logic_error("Unable to find key '" + key + "' in config file '" + config_filename + "'");
    }
    bool has_key(const std::string& key) const
    {
        return keys.count(key);
    }
};

// C++ stream interface
class Logger : public std::stringstream {
private:
    time_t create_time;
    std::string logger_name;

public:
    Logger(const std::string& _logger_name)
    {
        logger_name = _logger_name;
        create_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    }
    ~Logger()
    {
        const int MAX_SIZE = 80;
        char buffer[MAX_SIZE];

        auto timeinfo = localtime(&create_time);
        strftime(buffer, MAX_SIZE, "%H:%M:%S", timeinfo);
        fprintf(stderr, "[ %s ]( %s ): %s\n", logger_name.c_str(), buffer, str().c_str());
    }
};

template<typename T>
sdsl::bit_vector create_bit_vector(const std::vector<T> &sequence)
{
    auto bv = sdsl::bit_vector(sequence.size());
    for (size_t i = 0; i < sequence.size(); i++)
    {
        bv[i] = sequence[i];
    }
    return bv;
}

#define INFO Logger("INFO")
#define DEBUG Logger("DEBUG")

#endif // TEST_UTILS_HPP
