#ifndef TREE_HPP_INCLUDED
#define TREE_HPP_INCLUDED

#include <vector>
#include <algorithm>

class Tree {
private:
    size_t vertices_count;
    std::vector<std::vector<size_t>> adjacency_list;
    std::vector<size_t> v_parents;

    void dfs_order_vertices(size_t vertex, std::vector<size_t>& vertices) const
    {
        vertices.push_back(vertex);
        for (auto child : adjacency_list[vertex])
        {
            dfs_order_vertices(child, vertices);
        }
    }
    void dfs_sizes(size_t vertex, std::vector<uint64_t>& v_sizes) const
    {
        v_sizes[vertex] = 1;
        for (auto child : adjacency_list[vertex])
        {
            dfs_sizes(child, v_sizes);
            v_sizes[vertex] += v_sizes[child];
        }
    }
public:
    explicit Tree(size_t _vertices_count) : vertices_count(_vertices_count), adjacency_list(_vertices_count), v_parents(_vertices_count)
    {
    }
    void add_edge(size_t parent, size_t child)
    {
        adjacency_list[parent].push_back(child);
        v_parents[child] = parent;
    }
    size_t parent(size_t vertex) const
    {
        return v_parents[vertex];
    }
    Tree normalize_tree() const
    {
        auto vertices = std::vector<size_t>();
        dfs_order_vertices(0, vertices);
        auto vertex_position = std::vector<size_t>(size());
        for (size_t i = 0; i < size(); i++)
            vertex_position[vertices[i]] = i;
        auto normalized_tree = Tree(size());
        for (size_t v = 0; v < size(); v++)
        {
            for (auto child : adjacency_list[v])
            {
                normalized_tree.add_edge(vertex_position[v], vertex_position[child]);
            }
        }
        return normalized_tree;
    }
    sdsl::bit_vector dfuds_sequence() const
    {
        auto bp_sequence = sdsl::bit_vector(2 * size());
        size_t sequence_pointer = 1;
        for (auto degree : degrees())
        {
            bp_sequence[sequence_pointer] = 1;
            sequence_pointer += degree + 1;
        }
        return bp_sequence;
    }
    std::vector<uint64_t> subtree_sizes() const
    {
        auto v_sizes = std::vector<uint64_t >(size());
        dfs_sizes(0, v_sizes);
        return v_sizes;
    }
    std::vector<size_t> degrees() const
    {
        auto v_degree = std::vector<size_t>(size());
        std::transform(adjacency_list.begin(), adjacency_list.end(), v_degree.begin(), [](const std::vector<size_t> &a) {
            return a.size();
        });
        return v_degree;
    }
    std::vector<size_t> adjacent_vertices(size_t vertex) const
    {
        return adjacency_list[vertex];
    }
    std::vector<size_t> parents() const
    {
        return v_parents;
    }
    size_t size() const
    {
        return vertices_count;
    }
};


#endif // TREE_HPP_INCLUDED