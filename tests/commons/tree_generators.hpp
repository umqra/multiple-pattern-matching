#ifndef TREE_GENERATORS_HPP_INCLUDED
#define TREE_GENERATORS_HPP_INCLUDED

#include "tree.hpp"
#include "random.hpp"

class RandomTreeGenerator
{
private:
    Random random;
public:
    explicit RandomTreeGenerator() : random(0)
    {
    }
    Tree generate(size_t vertices_count)
    {
        auto tree = Tree(vertices_count);
        for (size_t i = 1; i < vertices_count; i++)
        {
            auto parent = random.next_int(0, i);
            tree.add_edge(parent, i);
        }
        return tree.normalize_tree();
    }
};

template<size_t BushWideFactor>
class BushLikeTreeGenerator
{
    static_assert(BushWideFactor > 1);
private:
    Random random;
public:
    explicit BushLikeTreeGenerator() : random(0)
    {
    }
    Tree generate(size_t vertices_count)
    {
        auto tree = Tree(vertices_count);
        for (size_t i = 1; i < vertices_count; i++)
        {
            auto parent = random.next_int(0, i / BushWideFactor + 1);
            tree.add_edge(parent, i);
        }
        return tree.normalize_tree();
    }
};

template<size_t BambooHeightFactor>
class BambooLikeTreeGenerator
{
    static_assert(BambooHeightFactor > 1);
private:
    Random random;
public:
    explicit BambooLikeTreeGenerator() : random(0)
    {
    }
    Tree generate(size_t vertices_count)
    {
        auto tree = Tree(vertices_count);
        for (size_t i = 1; i < vertices_count; i++)
        {
            auto parent = random.next_int(i - i / BambooHeightFactor - 1, i);
            tree.add_edge(parent, i);
        }
        return tree.normalize_tree();
    }
};

enum BracketSequenceGeneratorOption
{
    ZeroIsOpen,
    ZeroIsClose,
};

class BracketSequenceGenerator
{
private:
    BracketSequenceGeneratorOption option;
public:
    BracketSequenceGenerator(BracketSequenceGeneratorOption _option) : option(_option)
    {
    }
    sdsl::bit_vector generate_sequence(const Tree& tree)
    {
        auto dfuds = tree.dfuds_sequence();
        if (option == BracketSequenceGeneratorOption::ZeroIsClose)
        {
            for (size_t i = 0; i < dfuds.size(); i++)
                dfuds[i] = 1 ^ dfuds[i];
        }
        return dfuds;
    }
};

#endif // TREE_GENERATORS_HPP_INCLUDED