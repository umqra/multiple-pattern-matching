#!/usr/bin/python3
import glob
for dict_filename in glob.glob('*.dict'):
    if 'wiki' not in dict_filename : continue
    with open(dict_filename, 'r') as dict_file:
        lines = list(filter(lambda l: len(l) > 3, dict_file.readlines()))
    with open(dict_filename, 'w') as dict_file:
        dict_file.writelines(lines)

