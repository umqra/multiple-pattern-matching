#include <benchmark/benchmark.h>
#include <sdsl/io.hpp>
#include <sdsl/bp_support_sada.hpp>
#include <bp_supports/sdsl_near_match_finder.hpp>
#include <bp_supports/naive_block_processor.hpp>
#include <bp_supports/sse_block_processor.hpp>
#include <bp_supports/min_precalc_block_processor.hpp>
#include <bp_supports/paired_naive_block_processor.hpp>
#include <bp_supports/near_match_finder.hpp>
#include "commons/tree_generators.hpp"

template<typename TreeGenerator, typename NearMatcherAlgorithm>
void PerformanceNearMatchFinderTestClose(benchmark::State &state)
{
    auto sequence_size = static_cast<size_t>(state.range(0));
    auto tree = TreeGenerator().generate(sequence_size);
    auto sequence = BracketSequenceGenerator(ZeroIsClose).generate_sequence(tree);
    auto matcher = NearMatcherAlgorithm(&sequence);
    for (auto _ : state) {
        benchmark::DoNotOptimize(matcher.try_find_close(0, sequence.size(), 1));
    }
    state.SetBytesProcessed(state.iterations() * sequence.size() / 8);
}

template<typename TreeGenerator, typename NearMatcherAlgorithm>
void PerformanceNearMatchFinderTestOpen(benchmark::State &state)
{
    auto sequence_size = static_cast<size_t>(state.range(0));
    auto tree = TreeGenerator().generate(sequence_size);
    auto sequence = BracketSequenceGenerator(ZeroIsClose).generate_sequence(tree);
    auto matcher = NearMatcherAlgorithm(&sequence);
    for (auto _ : state) {
        benchmark::DoNotOptimize(matcher.try_find_open(sequence.size(), 0, 1));
    }
    state.SetBytesProcessed(state.iterations() * sequence.size() / 8);
}

template<typename TreeGenerator>
void PerformanceNearMatchFinderTestOpen_SDSL(benchmark::State &state)
{
    auto sequence_size = static_cast<size_t>(state.range(0));
    auto tree = TreeGenerator().generate(sequence_size);
    auto sequence = BracketSequenceGenerator(ZeroIsClose).generate_sequence(tree);
    for (auto _ : state) {
        benchmark::DoNotOptimize(sdsl::near_bwd_excess(sequence, sequence.size() - 2, sequence.size(), -1));
    }
    state.SetBytesProcessed(state.iterations() * sequence.size() / 8);
}

void customize_benchmark(benchmark::internal::Benchmark* bench)
{
    bench->ArgName("bp_size")->RangeMultiplier(2)->Range(8, 512);
}

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, RandomTreeGenerator, SdslNearMatchFinder)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BushLikeTreeGenerator<100>, SdslNearMatchFinder)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BambooLikeTreeGenerator<100>, SdslNearMatchFinder)->Apply(customize_benchmark);

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, RandomTreeGenerator, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BushLikeTreeGenerator<100>, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BambooLikeTreeGenerator<100>, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen_SDSL, RandomTreeGenerator)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen_SDSL, BushLikeTreeGenerator<100>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen_SDSL, BambooLikeTreeGenerator<100>)->Apply(customize_benchmark);

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen, RandomTreeGenerator, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen, BushLikeTreeGenerator<100>, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestOpen, BambooLikeTreeGenerator<100>, NearMatchFinder<MinPrecalcBlockProcessor>)->Apply(customize_benchmark);


/* Benchmark only most performant algorithms
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, RandomTreeGenerator, NearMatchFinder<NaiveBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BushLikeTreeGenerator<100>, NearMatchFinder<NaiveBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BambooLikeTreeGenerator<100>, NearMatchFinder<NaiveBlockProcessor>)->Apply(customize_benchmark);

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, RandomTreeGenerator, NearMatchFinder<PairedNaiveBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BushLikeTreeGenerator<100>, NearMatchFinder<PairedNaiveBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BambooLikeTreeGenerator<100>, NearMatchFinder<PairedNaiveBlockProcessor>)->Apply(customize_benchmark);

BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, RandomTreeGenerator, NearMatchFinder<SseBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestClose, BushLikeTreeGenerator<100>, NearMatchFinder<SseBlockProcessor>)->Apply(customize_benchmark);
BENCHMARK_TEMPLATE(PerformanceNearMatchFinderTestCloseClose, BambooLikeTreeGenerator<100>, NearMatchFinder<SseBlockProcessor>)->Apply(customize_benchmark);
*/

BENCHMARK_MAIN();