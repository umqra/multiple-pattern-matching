#include <benchmark/benchmark.h>
#include <sdsl/io.hpp>
#include <sdsl/bp_support_sada.hpp>
#include <bp_supports/bp_support_sada_light.hpp>
#include <bp_supports/min_precalc_block_processor.hpp>
#include "commons/tree_generators.hpp"
#include "commons/queries_generator.hpp"

template<typename TreeGenerator, typename QueriesGenerator, typename SuccinctBPAlgorithm>
void PerformanceBpPairTest(benchmark::State &state)
{
    auto sequence_size = static_cast<size_t>(state.range(0));
    auto tree = TreeGenerator().generate(sequence_size);
    auto sequence = BracketSequenceGenerator(ZeroIsOpen).generate_sequence(tree);

    auto queries_generator = BracketSequenceQueriesGenerator<QueriesGenerator>(tree);
    auto bp_index = SuccinctBPAlgorithm(&sequence);
    sdsl::util::init_support(bp_index, &sequence);
    MinPrecalcBlockProcessor::reset_rates();
    for (auto _ : state) {
        auto position = queries_generator.next_pair_query();
        benchmark::DoNotOptimize(bp_index.find_open(position));
        std::cerr << MinPrecalcBlockProcessor::get_bad_rate() << std::endl;
    }
    state.counters["used_bytes"] = sdsl::size_in_bytes(bp_index);
}

BENCHMARK_TEMPLATE(PerformanceBpPairTest, RandomTreeGenerator, RandomQueriesGenerator, sdsl::bp_support_sada<>)
    ->ArgName("bp_size")
    ->Range(100, 100000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, RandomTreeGenerator, RandomQueriesGenerator, bp_support_sada_light<>)
        ->ArgName("bp_size")
        ->Range(100, 100000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<50>, RandomQueriesGenerator, sdsl::bp_support_sada<>)
        ->ArgName("bp_size")
        ->Range(100, 100000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<50>, RandomQueriesGenerator, bp_support_sada_light<>)
        ->ArgName("bp_size")
        ->Range(100, 100000);

BENCHMARK_MAIN();