#include "aho.hpp"
#include "aho_korasick_matcher.hpp"
#include "belazzougui.hpp"
#include "compressed_matcher.hpp"
#include "compression_boosting.hpp"
#include "dfuds_tree.hpp"
#include "dictionary.hpp"
#include "file_utils.hpp"
#include "skip_layer_tree.hpp"
#include "standard_matcher.hpp"
#include "commons/test_utils.hpp"
#include "trie_matcher.hpp"
#include "ultra_dfuds_tree.hpp"
#include "code_utils.hpp"
#include <chrono>
#include <fstream>
#include <sdsl/io.hpp>

#include "commons/gtest_typed_test_extname.hpp"
#include "gmock/gmock.h"

using namespace ::testing;
using namespace sdsl;

std::string base_dir;

std::string get_test_fullname()
{
    auto test_info = UnitTest::GetInstance()->current_test_info();
    return std::string(test_info->name()) + "<" + test_info->type_param() + ">";
}

std::string get_test_data_file()
{
    return get_test_fullname() + ".data";
}

std::string get_html_dump_file()
{
    return get_test_fullname() + ".dump.html";
}

template <typename T>
class MemoryTest : public Test {
public:
	MemoryTest()
	{
		if (base_dir == "") {
			std::cerr << "You should specify --base_dir command line parameter with directory that contains configurations for performance tests" << std::endl;
			exit(0);
		}
	}
    ~MemoryTest()
    {
        DEBUG << "Start clean up after test: '" << get_test_fullname() << "'";

        DEBUG << "Remove automaton structure dump: '" << get_test_data_file() << "'";
        remove_file(get_test_data_file());

        DEBUG << "End clean up after test: '" << get_test_fullname() << "'";
    }
};

struct belazzougui_simple {
    using algo_type = standard_matcher<>;
};

struct belazzougui_compression_boosting {
    using algo_type = compressed_matcher<>;
};

template <size_t skip_count>
struct belazzougui_compression_boosting_partial_failure_tree {
    using algo_type = compressed_matcher<skip_layer_tree<skip_count>>;
};

struct trie_matcher_simple {
    using algo_type = trie_matcher<>;
};

typedef Types<
    belazzougui_simple,
    belazzougui_compression_boosting,
    belazzougui_compression_boosting_partial_failure_tree<4>,
    belazzougui_compression_boosting_partial_failure_tree<8>,
    belazzougui_compression_boosting_partial_failure_tree<10>,
    trie_matcher_simple>
    DictionaryMatchingAlgorithms;
TYPED_TEST_CASE(MemoryTest, DictionaryMatchingAlgorithms);

template <typename T>
void verbose_record_property(const std::string& key, const T& value)
{
    INFO << key << ": " << value;
    Test::RecordProperty(key, value);
}

template <typename alias_type, typename alphabet>
void performance_test(const test_configuration<alphabet>& test_config)
{
    auto test_data = get_test_data_file();

    verbose_record_property("test_description", test_config.description());
    verbose_record_property("dict_size_in_bytes", test_config.dict_size_in_bytes());

    DEBUG << "Load dictionary from '" << test_config.dictionary_location() << "'";
    auto dict = test_config.dict();
    auto trie = raw_trie(*dict);

    verbose_record_property("effective_sigma", trie.effective_alphabet_size());
    verbose_record_property("trie_edges", trie.size() - 1);

    DEBUG << "Build started: (" << get_test_fullname() << ")";
    auto build_start_time = std::chrono::high_resolution_clock::now();
    auto automaton = typename alias_type::algo_type(*dict);
    DEBUG << "Build finished: (" << get_test_fullname() << ")";

    auto build_end_time = std::chrono::high_resolution_clock::now();
    auto build_elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(build_end_time - build_start_time);
    verbose_record_property("build_time_ms", build_elapsed_ms.count());
    verbose_record_property("algo_used_bytes", sdsl::size_in_bytes(automaton));

    DEBUG << "Serialize automaton structure to the file: " << test_data;
    sdsl::store_to_file(automaton, test_data);

    DEBUG << "Write automaton HTML dump to the file: " << get_html_dump_file();
    std::ofstream out(get_html_dump_file());
    write_structure<HTML_FORMAT>(automaton, out);

    DEBUG << "Load automaton structure from the file: " << test_data << "(with size: " << sdsl::util::file_size(test_data) << ")";
    EXPECT_TRUE(sdsl::load_from_file(automaton, test_data));

    auto query_start_time = std::chrono::high_resolution_clock::now();

    DEBUG << "Load query text from '" << test_config.text_location() << "'";
    auto text = test_config.text();
    verbose_record_property("text_size_in_bytes", test_config.text_size_in_bytes());
    verbose_record_property("text_length", text.length());
    DEBUG << "Query phase started";
    verbose_record_property("occurences", automaton.enumerate_occurence_positions(text).size());
    DEBUG << "Query phase finished";

    auto query_end_time = std::chrono::high_resolution_clock::now();
    auto query_elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(query_end_time - query_start_time);

    verbose_record_property("query_time_ms", query_elapsed_ms.count());
}

void clean_up()
{
    DEBUG << "Clean up";
    auto test_data = get_test_data_file();
    DEBUG << "Deleting automaton dump file: '" << test_data << "'";
    remove_file(test_data);
}

TYPED_TEST_EXTNAME(MemoryTest, UrlsDatabase)
{
    performance_test<TypeParam>(test_configuration<utf32_alphabet>(base_dir, "urls.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, VirusSignatureDatabase)
{
    performance_test<TypeParam>(test_configuration<byte_alphabet>(base_dir, "virus_sign.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, DnaDatabase)
{
    performance_test<TypeParam>(test_configuration<byte_alphabet>(base_dir, "dna.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, WikiEnDatabase)
{
    performance_test<TypeParam>(test_configuration<utf32_alphabet>(base_dir, "wiki_en.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, WikiRuDatabase)
{
    performance_test<TypeParam>(test_configuration<utf32_alphabet>(base_dir, "wiki_ru.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, WikiChineseDatabase)
{
    performance_test<TypeParam>(test_configuration<utf32_alphabet>(base_dir, "wiki_zh.config"));
}

TYPED_TEST_EXTNAME(MemoryTest, WikiJapaneseDatabase)
{
    performance_test<TypeParam>(test_configuration<utf32_alphabet>(base_dir, "wiki_ja.config"));
}

int main(int argc, char** argv)
{
    dbg("Running performance tests in DEBUG");
    ::testing::InitGoogleTest(&argc, argv);
    if (argc > 2 && strcmp(argv[1], "--base_dir") == 0)
        base_dir = argv[2];

    return RUN_ALL_TESTS();
}
