#include <benchmark/benchmark.h>
#include <naive_tree.hpp>
#include "commons/test_utils.hpp"
#include "dfuds_tree.hpp"
#include "commons/queries_generator.hpp"
#include "commons/tree_generators.hpp"

template<typename TreeGenerator, typename QueriesGenerator, typename SuccinctTreeAlgorithm>
void PerformanceBpPairTest(benchmark::State &state)
{
    auto tree_size = static_cast<size_t>(state.range(0));
    auto tree = TreeGenerator().generate(tree_size);
    auto queries_generator = QueriesGenerator(tree);
    auto tree_index = SuccinctTreeAlgorithm(tree.degrees());
    for (auto _ : state) {
        auto vertex = queries_generator.next_vertex();
        benchmark::DoNotOptimize(tree_index.parent(vertex));
    }
    state.counters["used_bytes"] = sdsl::size_in_bytes(tree_index);
}

BENCHMARK_TEMPLATE(PerformanceBpPairTest, RandomTreeGenerator, RandomQueriesGenerator, dfuds_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, RandomTreeGenerator, RandomQueriesGenerator, naive_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BambooLikeTreeGenerator<10>, RandomQueriesGenerator, dfuds_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BambooLikeTreeGenerator<10>, RandomQueriesGenerator, naive_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<10>, RandomQueriesGenerator, dfuds_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<10>, RandomQueriesGenerator, naive_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<10>, FatVerticesQueriesGenerator, dfuds_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_TEMPLATE(PerformanceBpPairTest, BushLikeTreeGenerator<10>, FatVerticesQueriesGenerator, naive_tree<>)
        ->ArgName("tree_size")
        ->Range(100, 10000);

BENCHMARK_MAIN();