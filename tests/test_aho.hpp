#include "aho.hpp"
#include "aho_korasick_matcher.hpp"
#include "alphabet_concepts.hpp"
#include "belazzougui.hpp"
#include "compressed_matcher.hpp"
#include "dictionary.hpp"
#include "standard_matcher.hpp"
#include "commons/test_utils.hpp"
#include "transitions.h"
#include "trees.h"
#include "trie_matcher.hpp"
#include "gmock/gmock.h"

#include <iostream>

using namespace ::testing;

template <typename T>
class DictionaryMatcherTest : public Test {
};

template <typename T>
std::vector<T> get_sorted(std::vector<T> data)
{
    std::sort(data.begin(), data.end());
    return data;
}

template <uint8_t blocks_count, typename compression_boosting_bit_vector_type>
class fixed_block_compression_boosting {
public:
    template <typename iter_type>
    static compression_boosting_bit_vector_type build(size_t, size_t, iter_type begin, iter_type end)
    {
        return compression_boosting_bit_vector_type(blocks_count, begin, end);
    }
};

typedef Types<
    aho_korasick_matcher,
    standard_matcher<>,
    compressed_matcher<>,
    compressed_matcher<skip_layer_tree<4>>,
    compressed_matcher<skip_layer_tree<10>>,
    trie_matcher<>>
    DictionaryMatchingAlgorithms;
TYPED_TEST_CASE(DictionaryMatcherTest, DictionaryMatchingAlgorithms);

TYPED_TEST(DictionaryMatcherTest, EmptyDictionary)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({}));
    EXPECT_THAT(automaton.enumerate_occurence_positions(string_seq("sample text")), SizeIs(0));
}

TYPED_TEST(DictionaryMatcherTest, NoOccurences)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "ab" }));
    EXPECT_THAT(automaton.enumerate_occurence_positions(string_seq("ba")), SizeIs(0));
}

TYPED_TEST(DictionaryMatcherTest, SingleOccurence)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "ab" }));
    EXPECT_EQ(get_sorted(automaton.enumerate_occurence_positions(string_seq("ab"))), std::vector<occurence>{ occurence(0, 1) });
    EXPECT_EQ(get_sorted(automaton.enumerate_occurence_positions(string_seq("bab"))), std::vector<occurence>{ occurence(1, 2) });
}

TYPED_TEST(DictionaryMatcherTest, ManyOccurences)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "ab" }));
    EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(string_seq("babbab"))),
        get_sorted(std::vector<occurence>{ occurence(1, 2), occurence(4, 5) }));
}

TYPED_TEST(DictionaryMatcherTest, DictionaryWithFullyContainedWords)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "a", "aa" }));
    EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(string_seq("aaba"))),
        get_sorted(std::vector<occurence>{
            occurence(0, 0),
            occurence(0, 1),
            occurence(1, 1),
            occurence(3, 3) }));
}

TYPED_TEST(DictionaryMatcherTest, LargeDictionaryAndManyOccurences)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "ab", "baa", "aa" }));
    EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(string_seq("abaabbaa"))),
        get_sorted(std::vector<occurence>{
            occurence(0, 1),
            occurence(1, 3),
            occurence(2, 3),
            occurence(3, 4),
            occurence(5, 7),
            occurence(6, 7) }));
}

TYPED_TEST(DictionaryMatcherTest, HexDictionary)
{
    auto automaton = TypeParam(hex_dictionary<byte_alphabet>(unordered_dictionary<byte_alphabet>({ "6901", "01" })));
    auto text = string_seq({ 105, 1, 105, 255, 1, 105 }, 1);
    EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(text)),
        get_sorted(std::vector<occurence>{ occurence(0, 1), occurence(1, 1), occurence(4, 4) }));
}

TYPED_TEST(DictionaryMatcherTest, AllSubstringsAreOccurences)
{
    auto automaton = TypeParam(unordered_dictionary<byte_alphabet>({ "a", "aa", "aaa", "aaaa" }));
    std::string text = "aaaa";
    std::vector<occurence> expected;
    for (size_t r = 0; r < text.length(); r++)
        for (size_t l = 0; l <= r; l++)
            expected.push_back(occurence(l, r));

    EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(string_seq(text))),
        get_sorted(expected));
}

std::vector<std::vector<std::string>> dictionaries = {
    { "a" },
    { "b" },
    { "a", "ab" },
    { "a", "b", "aa", "ab", "ba", "bb" },
    { "aaba", "ba", "aa", "a", "bbbab", "bba" },
    { "abababab", "bab" },
    { "aaaaaaaa", "bbbbbbbb", "abababab" },
};

// TODO: move to the test utils
std::vector<std::string> binary_strings_with_len(size_t max_length)
{
    auto result = std::vector<std::string>();
    for (size_t mask = 1; mask < (1ULL << max_length); mask++) {
        std::string s = "";
        for (size_t id = 0; id < max_length; id++) {
            s.push_back("ab"[(mask >> id) & 1]);
        }
        result.push_back(s);
    }
    return result;
}

// TODO: implement this dictionary matching algorithm in separate class?
std::vector<occurence> find_occurences(const std::vector<std::string>& dictionary, const std::string& text)
{
    auto result = std::vector<occurence>();
    for (const auto& word : dictionary) {
        for (size_t i = 0; i + word.length() <= text.length(); i++) {
            if (text.substr(i, word.length()) == word)
                result.push_back(occurence(i, i + word.length() - 1));
        }
    }
    std::sort(result.begin(), result.end(), [](const occurence& a, const occurence& b) {
        if (a.right_pos != b.right_pos)
            return a.right_pos < b.right_pos;
        return a.left_pos < b.left_pos;
    });
    return result;
}

std::vector<std::string> binary_strings_shorter_than(size_t max_length)
{
    auto result = std::vector<std::string>();
    for (size_t l = 1; l < max_length; l++)
        for (const auto& s : binary_strings_with_len(l))
            result.push_back(s);
    return result;
}

TYPED_TEST(DictionaryMatcherTest, StressDictionaries)
{
    auto texts = binary_strings_shorter_than(11);
    for (const auto& d : dictionaries) {
        //std::cout << "Test dictionary: " << d << std::endl;
        for (const auto& text : texts) {
            auto automaton = TypeParam(unordered_dictionary<byte_alphabet>(d));
            auto expected = find_occurences(d, text);
            EXPECT_THAT(get_sorted(automaton.enumerate_occurence_positions(string_seq(text))),
                get_sorted(expected));
        }
    }
}
