#include "alphabet_concepts.hpp"
#include "dictionary.hpp"
#include "commons/test_utils.hpp"
#include "gmock/gmock.h"
#include "commons/random.hpp"

#include <map>
#include <random>
#include <vector>
#include <bp_supports/naive_block_processor.hpp>
#include <bp_supports/sdsl_near_match_finder.hpp>
#include <bp_supports/sse_block_processor.hpp>
#include <bp_supports/min_precalc_block_processor.hpp>
#include <bp_supports/paired_naive_block_processor.hpp>
#include <bp_supports/near_match_finder.hpp>
#include <bp_supports/bp_support_sada_light.hpp>
#include <sdsl/bp_support_sada.hpp>

using namespace ::testing;

template <typename T>
class BpTest : public Test {
};

typedef Types<sdsl::bp_support_sada<>, bp_support_sada_light<>> BpImplementations;
TYPED_TEST_CASE(BpTest, BpImplementations);

template<typename bp_impl>
void validate_sequence(const sdsl::bit_vector &sequence, bp_impl impl)
{
    sdsl::util::init_support(impl, &sequence);
    auto open_stack = std::vector<size_t>();
    for (size_t i = 0; i < sequence.size(); i++)
    {
        if (sequence[i] == 1)
            open_stack.push_back(i);
        else
        {
            auto pair_open = open_stack.back();
            open_stack.pop_back();
            EXPECT_EQ(impl.find_open(i), pair_open);
        }
    }
}

TYPED_TEST(BpTest, SingleBracketPair)
{
    auto sequence = sdsl::bit_vector({1, 0});
    validate_sequence(sequence, TypeParam(&sequence));
}

TYPED_TEST(BpTest, LongBracketSequence)
{
    auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
    validate_sequence(sequence, TypeParam(&sequence));
}

TYPED_TEST(BpTest, MatchInLongSequence)
{
    auto random = Random(0);
    const size_t sequence_length = 1000;
    for (auto it = 0; it < 1000; it++) {
        auto raw_sequence = std::vector<size_t>(sequence_length);
        for (size_t i = 0; i < sequence_length / 2; i++)
            raw_sequence[i] = 1;
        std::shuffle(raw_sequence.begin(), raw_sequence.end(), random.generator());
        int32_t balance = 0, min_balance_value = raw_sequence[0];
        size_t min_balance_pos = 0;

        for (size_t i = 0; i < sequence_length; i++)
        {
            if (raw_sequence[i] == 1)
                balance++;
            else
                balance--;
            if (balance < min_balance_value)
            {
                min_balance_value = balance;
                min_balance_pos = i;
            }
        }
        std::rotate(raw_sequence.begin(), raw_sequence.begin() + min_balance_pos + 1, raw_sequence.end());
        raw_sequence.insert(raw_sequence.begin(), 1);
        raw_sequence.insert(raw_sequence.end(), 0);
        auto sequence = create_bit_vector(raw_sequence);
        validate_sequence(sequence, TypeParam());
    }
}