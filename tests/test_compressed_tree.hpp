#include "compressed_tree.hpp"
#include "gmock/gmock.h"
#include "commons/test_utils.hpp"
#include "commons/tree_generators.hpp"

#include <sdsl/bit_vectors.hpp>

#include <map>
#include <vector>

using namespace ::testing;

TEST(CompressedTreeTest, SingleVertexTree)
{
    auto tree = compressed_tree<>(get_tree_degrees({ 0 }), sdsl::bit_vector({ 1 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
}

TEST(CompressedTreeTest, SingleEdgeTree)
{
    auto tree = compressed_tree<>(get_tree_degrees({ 0, 0 }), sdsl::bit_vector({ 1, 0 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_FALSE(is_parent_exists(tree, 1));
}

TEST(CompressedTreeTest, VLikeTree)
{
    auto tree = compressed_tree<>(get_tree_degrees({ 0, 0, 0 }), sdsl::bit_vector({ 1, 1, 0 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_EQ(get_parent(tree, 1), 0);
    EXPECT_FALSE(is_parent_exists(tree, 2));
}

TEST(CompressedTreeTest, YLikeTree)
{
    auto tree = compressed_tree<>(get_tree_degrees({ 0, 0, 1, 1 }), sdsl::bit_vector({ 1, 0, 1, 1 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_FALSE(is_parent_exists(tree, 1));
    EXPECT_EQ(get_parent(tree, 2), 1);
    EXPECT_EQ(get_parent(tree, 3), 1);
}

class CompressedTreeTest : public TestWithParam<size_t> {
};

TEST_P(CompressedTreeTest, StressTest)
{
    size_t vertices = GetParam();
    Tree random_tree = RandomTreeGenerator().generate(vertices);
    auto marked = sdsl::bit_vector(vertices, 0);
    for (size_t v = 0; v < vertices; v++) {
        marked[v] = random_int(2);
    }
    //    GTEST_LOG_(INFO) << "Test with " << vertices << " vertices for tree [" << random_tree << "] with marked vertices " << marked;
    auto tree = compressed_tree<>(random_tree.degrees(), marked);
    for (size_t v = 0; v < vertices; v++) {
        if (!marked[v])
            EXPECT_FALSE(is_parent_exists(tree, v));
        else
            EXPECT_EQ(get_parent(tree, v), random_tree.parent(v));
    }
}

INSTANTIATE_TEST_CASE_P(CompressedTreeStressTest, CompressedTreeTest, Range<size_t>(1, 100, 10));
INSTANTIATE_TEST_CASE_P(CompressedTreeBigDataStressTest, CompressedTreeTest, Range<size_t>((size_t)1e4, (size_t)1e5, (size_t)1e4));
