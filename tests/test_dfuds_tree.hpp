#include "dfuds_tree.hpp"
#include "gmock/gmock.h"
#include "commons/test_utils.hpp"
#include "ultra_dfuds_tree.hpp"
#include "naive_tree.hpp"
#include "commons/tree_generators.hpp"

#include <map>
#include <random>
#include <vector>

using namespace ::testing;

template <typename T>
class SuccinctTreeTest : public Test {
};

typedef Types<dfuds_tree<>, ultra_dfuds_tree<>, naive_tree<> > DfsTreeImplementation;
TYPED_TEST_CASE(SuccinctTreeTest, DfsTreeImplementation);

TYPED_TEST(SuccinctTreeTest, SingleVertexTree)
{
    auto tree = TypeParam({ 0 });
    EXPECT_EQ(tree.parent(0), 0);
}

TYPED_TEST(SuccinctTreeTest, SingleEdgeTree)
{
    auto tree = TypeParam(get_tree_degrees({ 0, 0 }));
    EXPECT_EQ(tree.parent(0), 0);
    EXPECT_EQ(tree.parent(1), 0);
}

TYPED_TEST(SuccinctTreeTest, BambooTree)
{
    std::vector<size_t> degrees = get_tree_degrees({ 0, 0, 1, 2, 3 });
    auto tree = TypeParam(degrees);
    for (size_t vertex = 1; vertex < degrees.size(); vertex++) {
        EXPECT_EQ(tree.parent(vertex), vertex - 1);
    }
}

TYPED_TEST(SuccinctTreeTest, VLikeTree)
{
    auto tree = TypeParam(get_tree_degrees({ 0, 0, 0 }));
    EXPECT_EQ(tree.parent(0), 0);
    EXPECT_EQ(tree.parent(1), 0);
    EXPECT_EQ(tree.parent(2), 0);
}

TYPED_TEST(SuccinctTreeTest, YLikeTree)
{
    auto tree = TypeParam(get_tree_degrees({ 0, 0, 1, 1 }));
    EXPECT_EQ(tree.parent(1), 0);
    EXPECT_EQ(tree.parent(2), 1);
    EXPECT_EQ(tree.parent(3), 1);
}

TYPED_TEST(SuccinctTreeTest, ComplexTree)
{
    auto tree = TypeParam(get_tree_degrees({ 0, 0, 1, 1, 0, 4, 4, 6, 6, 4 }));
    EXPECT_EQ(tree.parent(1), 0);
    EXPECT_EQ(tree.parent(2), 1);
    EXPECT_EQ(tree.parent(3), 1);
    EXPECT_EQ(tree.parent(4), 0);
    EXPECT_EQ(tree.parent(5), 4);
    EXPECT_EQ(tree.parent(6), 4);
    EXPECT_EQ(tree.parent(7), 6);
    EXPECT_EQ(tree.parent(8), 6);
    EXPECT_EQ(tree.parent(9), 4);
}

TYPED_TEST(SuccinctTreeTest, TreeWithBigDegree)
{
    std::vector<size_t> degrees = {};
    size_t first_level_size = 100, second_level_size = 70;
    degrees.push_back(first_level_size);
    for (size_t first_level = 0; first_level < first_level_size; first_level++) {
        degrees.push_back(second_level_size);
        for (size_t second_level = 0; second_level < second_level_size; second_level++) {
            degrees.push_back(0);
        }
    }
    auto tree = TypeParam(degrees);

    size_t vertex_id = 1;
    for (size_t first_level = 0; first_level < first_level_size; first_level++, vertex_id++) {

        size_t first_level_v = vertex_id;
        EXPECT_EQ(tree.parent(vertex_id), 0);

        for (size_t second_level = 0; second_level < second_level_size; second_level++, vertex_id++) {
            EXPECT_EQ(tree.parent(vertex_id + 1), first_level_v);
        }
    }
}

TYPED_TEST(SuccinctTreeTest, BigTree)
{
    auto vertices = (size_t)1e4;
    Tree random_tree = RandomTreeGenerator().generate(vertices);

    auto tree = TypeParam(random_tree.degrees());
    for (size_t v = 0; v < vertices; v++) {
        EXPECT_EQ(tree.parent(v), tree.parent(v)) << " unexpected parent for vertex " << v << " in tree: " << random_tree;
    }
}
