#include "alphabet_concepts.hpp"
#include "dictionary.hpp"
#include "commons/test_utils.hpp"
#include "gmock/gmock.h"

#include <map>
#include <random>
#include <vector>

using namespace ::testing;

TEST(DictionaryTest, TestHexDictionary)
{
    auto raw_dict = unordered_dictionary<byte_alphabet>({ "627965", "68656c6c6f" });
    auto decoded_dict = hex_dictionary<byte_alphabet>(raw_dict);
    EXPECT_EQ(decoded_dict[0], string_seq("bye"));
    EXPECT_EQ(decoded_dict[1], string_seq("hello"));
}

TEST(DictionaryTest, TestUnicodeDictionary)
{
    auto raw_dict = unordered_dictionary<utf16_alphabet>({ u"привет", u"cześć" });
    EXPECT_EQ(raw_dict[0], string_seq({ 99, 122, 101, 347, 263 }, 2));
    EXPECT_EQ(raw_dict[1], string_seq({ 1087, 1088, 1080, 1074, 1077, 1090 }, 2));
}

void check_transition(const trie_iterator& iterator, size_t from, size_t to, size_t character, bool is_terminal)
{
    EXPECT_EQ(iterator.from(), from);
    EXPECT_EQ(iterator.to(), to);
    EXPECT_EQ(iterator.character(), character);
    EXPECT_EQ(iterator.is_terminal(), is_terminal);
}

TEST(DictionaryTest, TestTrieIteratorWithDuplicates)
{
    auto raw_dict = unordered_dictionary<byte_alphabet>({ "a", "a" });
    auto iterator = trie_iterator(raw_dict);
    EXPECT_TRUE(iterator.move());
    check_transition(iterator, 0, 1, 'a', true);

    EXPECT_FALSE(iterator.move());
}

TEST(DictionaryTest, TestTrieIterator)
{
    auto raw_dict = unordered_dictionary<byte_alphabet>({ "a", "abb", "b" });
    auto iterator = trie_iterator(raw_dict);
    EXPECT_TRUE(iterator.move());
    check_transition(iterator, 0, 1, 'a', true);

    EXPECT_TRUE(iterator.move());
    check_transition(iterator, 1, 2, 'b', false);

    EXPECT_TRUE(iterator.move());
    check_transition(iterator, 2, 3, 'b', true);

    EXPECT_TRUE(iterator.move());
    check_transition(iterator, 0, 4, 'b', true);

    EXPECT_FALSE(iterator.move());
}
