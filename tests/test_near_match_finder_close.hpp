#include "alphabet_concepts.hpp"
#include "commons/random.hpp"
#include "commons/test_utils.hpp"
#include "dictionary.hpp"
#include "gmock/gmock.h"

#include <bp_supports/min_precalc_block_processor.hpp>
#include <bp_supports/naive_block_processor.hpp>
#include <bp_supports/near_match_finder.hpp>
#include <bp_supports/paired_naive_block_processor.hpp>
#include <bp_supports/sdsl_near_match_finder.hpp>
#include <bp_supports/sse_block_processor.hpp>
#include <map>
#include <random>
#include <vector>

using namespace ::testing;

template <typename T> class NearMatchFinderTestClose : public Test {};

typedef Types<NearMatchFinder<NaiveBlockProcessor>,
              NearMatchFinder<PairedNaiveBlockProcessor>,
              NearMatchFinder<MinPrecalcBlockProcessor>, SdslNearMatchFinder>
    NearMatchFinderImplementations;
TYPED_TEST_CASE(NearMatchFinderTestClose, NearMatchFinderImplementations);

TYPED_TEST(NearMatchFinderTestClose, SingleBracketPair) {
  auto sequence = sdsl::bit_vector({1, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, 2, 0), 1);
}

TYPED_TEST(NearMatchFinderTestClose, TooFarBracketMatch) {
  auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, 9, 0), -1);
}

TYPED_TEST(NearMatchFinderTestClose, MatchFarBracketPair) {
  auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, 10, 0), 9);
}

TYPED_TEST(NearMatchFinderTestClose, MatchWithNonZeroStartBalance) {
  auto sequence = sdsl::bit_vector({0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, 5, 2), 3);
}

TYPED_TEST(NearMatchFinderTestClose, MatchWithNonZeroOffset) {
  auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(3, 10, 0), 8);
}

TYPED_TEST(NearMatchFinderTestClose, MatchInOddPosition) {
  auto sequence = sdsl::bit_vector({1, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, 3, 1), 2);
}

TYPED_TEST(NearMatchFinderTestClose, StressTest) {
    const int sequence_length = 100;
    auto random = Random(0);
    for (auto it = 0; it < 10000; it++) {
        auto sequence_v = std::vector<int>();
        for (int i = 0; i < sequence_length; i++) {
            sequence_v.push_back(static_cast<int>(random.next_int(0, 2)));
        }
        auto sequence = create_bit_vector(sequence_v);
        int balance = static_cast<int>(random.next_int(1, 5));
        auto start_balance = balance;
        int start_position_inclusive = static_cast<int>(random.next_int(0, sequence_length));
        auto match = TypeParam(&sequence).try_find_close(start_position_inclusive, sequence_length, balance);
        bool found = false;
        for (int s = start_position_inclusive; s < sequence_length; s++) {
            if (sequence[s] == 0)
                balance--;
            else
                balance++;
            if (balance == 0) {
                found = true;
                EXPECT_EQ(match, s) << " at position " << start_position_inclusive << " with balance " << start_balance << " on sequence " << sequence_v;
                break;
            }
        }
        if (!found)
            EXPECT_EQ(match, -1) << " at position " << start_position_inclusive << " with balance " << start_balance << " on sequence " << sequence_v;
    }
}

/*
TYPED_TEST(NearMatchFinderTestCloseClose, TestTrulyBlockSize_AlignedMatch) {
  auto sequence = sdsl::bit_vector({1, 0, 1, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(2, 2, 0), 3);
}

TYPED_TEST(NearMatchFinderTestCloseClose, TestTrulyBlockSize_MatchNotFound) {
    auto sequence = sdsl::bit_vector({0, 1, 0, 1, 1, 0, 0});
    EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(3, 3, 0), -1);
}

TYPED_TEST(NearMatchFinderTestCloseClose, TestTrulyBlockSize_NotAlignedMatch) {
    auto sequence = sdsl::bit_vector({1, 0, 1, 1, 0, 0});
    EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(3, 3, 0), 4);
}
*/

TYPED_TEST(NearMatchFinderTestClose, MatchInLongSequence) {
  auto a = 1;
  auto random = Random(0);
  const size_t sequence_length = 1000;
  auto raw_sequence = std::vector<size_t>(sequence_length);
  for (size_t i = 0; i < sequence_length / 2; i++)
    raw_sequence[i] = 1;
  std::shuffle(raw_sequence.begin(), raw_sequence.end(), random.generator());
  int32_t balance = 0, min_balance_value = raw_sequence[0];
  size_t min_balance_pos = 0;

  for (size_t i = 0; i < sequence_length; i++) {
    if (raw_sequence[i] == 1)
      balance++;
    else
      balance--;
    if (balance < min_balance_value) {
      min_balance_value = balance;
      min_balance_pos = i;
    }
  }
  std::rotate(raw_sequence.begin(), raw_sequence.begin() + min_balance_pos + 1,
              raw_sequence.end());
  raw_sequence.insert(raw_sequence.begin(), 1);
  raw_sequence.insert(raw_sequence.end(), 0);
  auto sequence = create_bit_vector(raw_sequence);
  EXPECT_EQ(TypeParam(&sequence).try_find_close(0, sequence.size(), 0),
            sequence.size() - 1);
}