#include "alphabet_concepts.hpp"
#include "commons/random.hpp"
#include "commons/test_utils.hpp"
#include "dictionary.hpp"
#include "gmock/gmock.h"

#include <bp_supports/min_precalc_block_processor.hpp>
#include <bp_supports/naive_block_processor.hpp>
#include <bp_supports/near_match_finder.hpp>
#include <bp_supports/paired_naive_block_processor.hpp>
#include <bp_supports/sdsl_near_match_finder.hpp>
#include <bp_supports/sse_block_processor.hpp>
#include <map>
#include <random>
#include <vector>

using namespace ::testing;

template <typename T> class NearMatchFinderTestOpen : public Test {};

typedef Types<NearMatchFinder<NaiveBlockProcessor>,
              NearMatchFinder<PairedNaiveBlockProcessor>,
              NearMatchFinder<MinPrecalcBlockProcessor>>
    NearMatchFinderImplementations_SmallSet;
TYPED_TEST_CASE(NearMatchFinderTestOpen,
                NearMatchFinderImplementations_SmallSet);

TYPED_TEST(NearMatchFinderTestOpen, SingleBracketPair) {
  auto sequence = sdsl::bit_vector({1, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(2, 0, 0), 0);
}

TYPED_TEST(NearMatchFinderTestOpen, TooFarBracketMatch) {
  auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(9, 0, 0), 3);
}

TYPED_TEST(NearMatchFinderTestOpen, MatchFarBracketPair) {
  auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 0, 1, 0, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(10, 0, 0), 0);
}

TYPED_TEST(NearMatchFinderTestOpen, MatchWithNonZeroStartBalance) {
  auto sequence = sdsl::bit_vector({1, 1, 1, 0, 1});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(5, 0, 2), 1);
}

TYPED_TEST(NearMatchFinderTestOpen, MatchWithNonZeroOffset) {
  auto sequence = sdsl::bit_vector({1, 1, 1, 0, 1, 0, 0, 1, 0, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(7, 0, 0), 1);
}

TYPED_TEST(NearMatchFinderTestOpen, MatchInOddPosition) {
  auto sequence = sdsl::bit_vector({1, 1, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_open(3, 0, 1), 0);
}

TYPED_TEST(NearMatchFinderTestOpen, TestBiggerThanPrecalc) {
    auto sequence = sdsl::bit_vector({1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1});
    EXPECT_EQ(TypeParam(&sequence).try_find_open(13, 0, 3), 4);
}

TYPED_TEST(NearMatchFinderTestOpen, StressTest) {
    const int sequence_length = 100;
    auto random = Random(0);
    for (auto it = 0; it < 10000; it++) {
        auto sequence_v = std::vector<int>();
        for (int i = 0; i < sequence_length; i++) {
            sequence_v.push_back(static_cast<int>(random.next_int(0, 2)));
        }
        auto sequence = create_bit_vector(sequence_v);
        int balance = static_cast<int>(random.next_int(1, 5));
        auto start_balance = balance;
        int start_position_exclusive = static_cast<int>(random.next_int(1, sequence_length + 1));
        auto match = TypeParam(&sequence).try_find_open(start_position_exclusive, 0, balance);
        bool found = false;
        for (int s = start_position_exclusive - 1; s >= 0; s--) {
            if (sequence[s] == 0)
                balance++;
            else
                balance--;
            if (balance == 0) {
                found = true;
                EXPECT_EQ(match, s) << " at position " << start_position_exclusive - 1 << " with balance " << start_balance << " on sequence " << sequence_v;
                break;
            }
        }
        if (!found)
            EXPECT_EQ(match, -1) << " at position " << start_position_exclusive - 1 << " with balance " << start_balance << " on sequence " << sequence_v;
    }
}

/*
TYPED_TEST(NearMatchFinderTestOpenOpen, TestTrulyBlockSize_AlignedMatch) {
  auto sequence = sdsl::bit_vector({1, 0, 1, 0});
  EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(2, 2, 0), 3);
}

TYPED_TEST(NearMatchFinderTestOpenOpen, TestTrulyBlockSize_MatchNotFound) {
    auto sequence = sdsl::bit_vector({0, 1, 0, 1, 1, 0, 0});
    EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(3, 3, 0), -1);
}

TYPED_TEST(NearMatchFinderTestOpenOpen, TestTrulyBlockSize_NotAlignedMatch) {
    auto sequence = sdsl::bit_vector({1, 0, 1, 1, 0, 0});
    EXPECT_EQ(TypeParam(&sequence).try_find_close_within_block(3, 3, 0), 4);
}
*/

TYPED_TEST(NearMatchFinderTestOpen, MatchInLongSequence) {
  auto random = Random(0);
  for (auto it = 0; it < 100; it++) {
    const size_t sequence_length = 1000;
    auto raw_sequence = std::vector<size_t>(sequence_length);
    for (size_t i = 0; i < sequence_length / 2; i++)
      raw_sequence[i] = 1;
    std::shuffle(raw_sequence.begin(), raw_sequence.end(), random.generator());
    int32_t balance = 0, min_balance_value = raw_sequence[0];
    size_t min_balance_pos = 0;

    for (size_t i = 0; i < sequence_length; i++) {
      if (raw_sequence[i] == 1)
        balance++;
      else
        balance--;
      if (balance < min_balance_value) {
        min_balance_value = balance;
        min_balance_pos = i;
      }
    }
    std::rotate(raw_sequence.begin(),
                raw_sequence.begin() + min_balance_pos + 1, raw_sequence.end());
    raw_sequence.insert(raw_sequence.begin(), 1);
    raw_sequence.insert(raw_sequence.end(), 0);
    auto sequence = create_bit_vector(raw_sequence);
    EXPECT_EQ(TypeParam(&sequence).try_find_open(sequence.size(), 0, 0), 0);
  }
}