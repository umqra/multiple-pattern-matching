#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import matplotlib.units as units
import matplotlib.dates as dates
import matplotlib.ticker as ticker
from xml.etree import ElementTree
import re

DatasetColumn = 'Dataset'
AlgorithmColumn = 'Algorithm'
MemoryColumn = 'Memory'
QueryTimeColumn = 'Query time'
BuildTimeColumn = 'Build time'

database_name_alias = {
    'UrlsDatabase': 'Urls',
    'VirusSignatureDatabase': 'Viruses',
    'DnaDatabase': 'DNA',
    'WikiEnDatabase': 'WikiEn',
    'WikiRuDatabase': 'WikiRu',
    'WikiChineseDatabase': 'WikiZh',
    'WikiJapaneseDatabase': 'WikiJa',
}

def parse_type_param(s):
    return re.sub(r'\<(\d+).*?\>', r'(\1)', s.replace('_', ' ').capitalize())

# TODO: barplot(... units=???)
def draw_barplot(data_frame, y_column, measure_units, ax):
    sns.barplot(x=DatasetColumn, y=y_column, hue=AlgorithmColumn, data=data_frame, ax=ax)
    ax.set_title(y_column)
    ax.set_ylabel(measure_units)
#    for tick in ax.get_xticklabels():
#        tick.set_rotation(90)
    ax.legend_.remove()

def main():
    tree = ElementTree.parse('test_mem_result.xml')
    rows = []
    fig, axes = plt.subplots(1, 3, figsize=(18, 8))
    for child in tree.getroot().iter('testcase'):
        rows.append([
            database_name_alias[child.attrib['name']], 
            parse_type_param(child.attrib['type_param']), 
            int(child.attrib['used_bytes']) / int(child.attrib['dataset_size']),
            int(child.attrib['query_time']) / int(child.attrib['text_size']),
            int(child.attrib['build_time']) / int(child.attrib['dataset_size'])])

    columns = [DatasetColumn, AlgorithmColumn, MemoryColumn, QueryTimeColumn, BuildTimeColumn]
    data_frame = pd.DataFrame(rows, columns=columns)

    draw_barplot(data_frame, QueryTimeColumn, 'milliseconds per byte', axes[0])
    draw_barplot(data_frame, BuildTimeColumn, 'milliseconds per byte', axes[1])
    draw_barplot(data_frame, MemoryColumn, 'bytes per text byte', axes[2])

    handles, labels = axes[-1].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper left')

    fig.savefig('plot.png')

if __name__ == '__main__':
    main()
