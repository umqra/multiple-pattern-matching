#include "gmock/gmock.h"
#include "skip_layer_tree.hpp"
#include "commons/test_utils.hpp"

#include <sdsl/bit_vectors.hpp>

#include <map>
#include <vector>

using namespace ::testing;

// TODO: it's better to test logic, that calculates mask of vertices for compressed_tree instead of all tree logic

TEST(SkipLayerTreeTest, SingleVertexTree)
{
    auto tree = skip_layer_tree<0>(get_tree_degrees({ 0 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
}

TEST(SkipLayerTreeTest, SingleEdgeTree)
{
    auto tree = skip_layer_tree<1>(get_tree_degrees({ 0, 0 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_FALSE(is_parent_exists(tree, 1));
}

TEST(SkipLayerTreeTest, VLikeTree)
{
    auto tree = skip_layer_tree<1>(get_tree_degrees({ 0, 0, 0 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_FALSE(is_parent_exists(tree, 1));
    EXPECT_FALSE(is_parent_exists(tree, 2));
}

TEST(SkipLayerTreeTest, YLikeTree)
{
    auto tree = skip_layer_tree<1>(get_tree_degrees({ 0, 0, 1, 1 }));
    EXPECT_EQ(get_parent(tree, 0), 0);
    EXPECT_EQ(get_parent(tree, 1), 0);
    EXPECT_FALSE(is_parent_exists(tree, 2));
    EXPECT_FALSE(is_parent_exists(tree, 3));
}

class SkipLayerTreeTest : public TestWithParam<size_t> {
};

TEST_P(SkipLayerTreeTest, StressTest)
{
    const size_t skip_layers = 3;

    size_t vertices = GetParam();
    Tree random_tree = RandomTreeGenerator().generate(vertices);
    auto tree = skip_layer_tree<skip_layers>(random_tree.degrees());
    size_t known_parents = 0;
    for (size_t v = 0; v < vertices; v++) {
        if (is_parent_exists(tree, v)) {
            EXPECT_EQ(get_parent(tree, v), random_tree.parent(v));
            known_parents++;
        }
    }
    EXPECT_LE(known_parents, vertices / skip_layers + 1); // +1 for root vertex
}

INSTANTIATE_TEST_CASE_P(SkipLayerTreeStressTest, SkipLayerTreeTest, Range<size_t>(1, 100, 10));
INSTANTIATE_TEST_CASE_P(SkipLayerTreeBigDataStressTest, SkipLayerTreeTest, Range<size_t>((size_t)1e4, (size_t)1e5, (size_t)1e4));
