#include "raw_trie.hpp"
#include "commons/test_utils.hpp"
#include "transitions.h"
#include "trie_path_array.hpp"
#include "gmock/gmock.h"

using namespace ::testing;

template <typename transitions_type>
size_t get_transition(const transitions_type& trans, size_t from, size_t symbol)
{
    size_t nxt;
    EXPECT_TRUE(trans.try_next(from, symbol, nxt)) << " from " << from << " by symbol " << symbol;
    return nxt;
}

TEST(SuccinctTransitionsTest, EmptyTransitions)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0 }, { 0 }));
    size_t _;
    EXPECT_FALSE(trans.try_next(0, 'a', _));
}

TEST(SuccinctTransitionsTest, SingleTransition)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0 }, { 0, 1 }));

    size_t _;
    EXPECT_FALSE(trans.try_next(0, 0, _));
    EXPECT_EQ(get_transition(trans, 0, 1), 1);
}

TEST(SuccinctTransitionsTest, TwoTransitions)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0, 0 }, { 0, 0, 1 }));

    EXPECT_EQ(get_transition(trans, 0, 0), 1);
    EXPECT_EQ(get_transition(trans, 0, 1), 2);
}

TEST(SuccinctTransitionsTest, ManyTransitions)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0, 4, 1, 0, 1 }, { 0, 0, 0, 1, 2, 2 }));

    EXPECT_EQ(get_transition(trans, 0, 0), 1);
    EXPECT_EQ(get_transition(trans, 0, 2), 4);
    EXPECT_EQ(get_transition(trans, 1, 1), 3);
    EXPECT_EQ(get_transition(trans, 1, 2), 5);
    EXPECT_EQ(get_transition(trans, 4, 0), 2);
}

TEST(SuccinctTransitionsTest, LetterTransitions)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 2, 0 }, { 0, 'a', 'b' }));

    EXPECT_EQ(get_transition(trans, 0, 'b'), 2);
    EXPECT_EQ(get_transition(trans, 2, 'a'), 1);
}

TEST(SuccinctTransitionsTest, SingleTransitionParent)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0 }, { 0, 1 }));

    EXPECT_EQ(trans.parent(1, 1), 0);
}

TEST(SuccinctTransitionsTest, TwoTransitionsParent)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0, 0 }, { 0, 0, 1 }));

    EXPECT_EQ(trans.parent(1, 0), 0);
    EXPECT_EQ(trans.parent(2, 1), 0);
}

TEST(SuccinctTransitionsTest, ManyTransitionsParent)
{
    auto trans = transitions_succinct_map<>(raw_trie({ 0, 0, 4, 1, 0, 1 }, { 0, 0, 0, 1, 2, 2 }));

    EXPECT_EQ(trans.parent(1, 0), 0);
    EXPECT_EQ(trans.parent(2, 0), 4);
    EXPECT_EQ(trans.parent(3, 1), 1);
    EXPECT_EQ(trans.parent(4, 2), 0);
    EXPECT_EQ(trans.parent(5, 2), 1);
}

TEST(SuccinctTransitionsTest, StressTest)
{
    for (size_t it = 0; it < 10000; it++) {
        size_t vertices_count = random_int(100) + 1, sigma = random_int(100) + 1, seed = random_int(1 << 20);
        auto random_trie = Trie(vertices_count, sigma, seed);
        auto trie = raw_trie(raw_trie(random_trie.parents(), random_trie.characters()));
        auto trie_array = trie_path_array(trie);
        trie = trie.permute(trie_array.vertices_order, trie_array.i_vertices_order);

        auto trans = transitions_succinct_map<>(trie);
        std::map<std::pair<size_t, size_t>, size_t> trans_map;
        //GTEST_LOG_(INFO) << "Test #" << it << " with " << vertices_count << " vertices for trie [" << random_trie << "]";
        for (size_t v = 1; v < vertices_count; v++)
            trans_map[std::make_pair(trie.parent[v], trie.parent_char[v])] = v;
        for (size_t v = 0; v < vertices_count; v++) {
            for (size_t letter = 0; letter < sigma; letter++) {
                size_t nxt;
                bool has_next = trans.try_next(v, letter, nxt);
                if (has_next && trans_map.count(std::make_pair(v, letter)) > 0)
                    EXPECT_EQ(nxt, trans_map[std::make_pair(v, letter)]) << "with v=" << v << " and  letter=" << letter;
                else if (has_next && trans_map.count(std::make_pair(v, letter)) == 0)
                    FAIL() << "extra transition in transitions_succinct_map<> from " << v << " by " << letter;
                else if (!has_next && trans_map.count(std::make_pair(v, letter)) > 0)
                    FAIL() << "missed transition in transitions_succinct_map<> from " << v << " by " << letter;
            }
        }
    }
}
