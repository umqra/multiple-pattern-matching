#include "raw_trie.hpp"
#include "commons/test_utils.hpp"

#include "trie_path_array.hpp"
#include "gmock/gmock.h"

using namespace ::testing;

std::vector<size_t> sort_trie_paths(std::vector<size_t> parent, std::vector<size_t> classes)
{
    std::vector<size_t> v_order = std::vector<size_t>(parent.size());
    std::iota(v_order.begin(), v_order.end(), 0);
    sort(v_order.begin(), v_order.end(), [&parent, &classes](int a, int b) {
        while (classes[a] == classes[b]) {
            a = parent[a];
            b = parent[b];
        }
        return classes[a] < classes[b];
    });
    return v_order;
}

TEST(TestTriePathArray, SingleVertex)
{
    auto trie_array = trie_path_array(raw_trie({ 0 }, { 0 }));
    EXPECT_THAT(trie_array.vertices_order, ElementsAre(0));
}

TEST(TestTriePathArray, SingleEdge)
{
    auto trie_array = trie_path_array(raw_trie({ 0, 0 }, { 0, 'a' }));
    EXPECT_THAT(trie_array.vertices_order, ElementsAre(0, 1));
}

TEST(TestTriePathArray, VLikeTree)
{
    auto trie_array = trie_path_array(raw_trie({ 0, 0, 0 }, { 0, 'a', 'b' }));
    EXPECT_THAT(trie_array.vertices_order, ElementsAre(0, 1, 2));
}

TEST(TestTriePathArray, YLikeTree)
{
    auto trie_array = trie_path_array(raw_trie({ 0, 0, 1, 1 }, { 0, 'b', 'a', 'c' }));
    EXPECT_THAT(trie_array.vertices_order, ElementsAre(0, 2, 1, 3));
}

TEST(TestTriePathArray, ComplexTree)
{
    auto trie_array = trie_path_array(raw_trie({ 0, 0, 1, 1, 3, 0, 5, 5 }, { 0, 'a', 'b', 'c', 'a', 'c', 'b', 'c' }));
    EXPECT_THAT(trie_array.vertices_order, ElementsAre(0, 1, 4, 2, 6, 5, 3, 7));
}

TEST(TestTriePathArray, StressTest)
{
    for (size_t it = 0; it < 10000; it++) {
        Trie random_trie = Trie(random_int(20) + 1, random_int(10) + 1, it);
        //GTEST_LOG_(INFO) << "Test #" << it << " with random_trie = " << random_trie << std::endl;
        auto trie_array = trie_path_array(raw_trie(random_trie.parents(), random_trie.characters()));
        EXPECT_EQ(trie_array.vertices_order, sort_trie_paths(random_trie.parents(), random_trie.characters()));
    }
}

class TrieStressTest : public TestWithParam<size_t> {
};

TEST_P(TrieStressTest, WorksRight)
{
    size_t vertices = GetParam();
    Trie random_trie = Trie(vertices, random_int(10) + 2);
    auto trie_array = trie_path_array(raw_trie(random_trie.parents(), random_trie.characters()));
    EXPECT_EQ(trie_array.vertices_order, sort_trie_paths(random_trie.parents(), random_trie.characters()));
}

INSTANTIATE_TEST_CASE_P(StressTest, TrieStressTest, Range<size_t>(1, 1000, 100));
