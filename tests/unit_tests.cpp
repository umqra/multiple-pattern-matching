#include "test_aho.hpp"
#include "test_bp.hpp"
#include "test_compressed_tree.hpp"
#include "test_dfuds_tree.hpp"
#include "test_dictionary.hpp"
#include "test_skip_layer_tree.hpp"
#include "test_transitions.hpp"
#include "test_trie_path_array.hpp"
#include "test_near_match_finder_close.hpp"
#include "test_near_match_finder_open.hpp"

#include "gmock/gmock.h"

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
